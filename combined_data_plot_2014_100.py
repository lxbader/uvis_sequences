# Large plots with all necessary information
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime, timedelta
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import numpy.ma as ma
import matplotlib.patches as ptch
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import time_conversions as tc
import uvisdb

samplepath = '{}hst_2014_100'.format(__file__.strip('combined_data_plot_2014_100.py'))
if not os.path.exists(samplepath):
    os.makedirs(samplepath)

myblue='royalblue'
myred='crimson'
def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_UV_short = truncate_colormap(cmap_UV, 0.1, 0.9, 100)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
cmap_SPEC_short = truncate_colormap(cmap_SPEC, 0.25, 0.75, 100)
cmap_SPEC_r = cubehelix.cmap(reverse=True, start=1.8, rot=1, gamma=1.5)
cmap_SPEC_r_short = truncate_colormap(cmap_SPEC_r, 0.25, 0.75, 100)

# path to list of UVIS images
udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF

uvistimes = imglist[['ET_START', 'ET_STOP']]

# Set up Cassinipy
cd = cassinipy.CassiniData(datapath)
cd.set_refframe('KRTP')
magdata = cd.cmag
rpwsdata = cd.crpws
incadata = cd.cinca
lemmsdata = cd.clemms
elsdata = cd.cels

# General settings
fpcolordark = 'k'
fpcolorbright = 'white'
mydpi = 300

# Constants
SATURN_R = 60268 # km

# Magnetopause model
# Input: Dp (dynamic pressure in nPa)
def mploc(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    theta = np.arange(0,170,0.1)/180*np.pi
    radius = r0*(2/(1+np.cos(theta)))**K
    [x,r] = radius*[np.cos(theta), np.sin(theta)]
    return [x,r]

    
def plot_combined_data(etstart, etstop, savefile, title):
    # time windows
    # for data plots
    tlimet = [etstart, etstop]
    times = np.linspace(tlimet[0],tlimet[1],num=1000,endpoint=True)
    # for trajectories
    tlimet_ext = [etstart-10*24*3600,etstop+10*24*3600]
    times_ext = np.linspace(tlimet_ext[0],tlimet_ext[1],num=1000,endpoint=True)
    # markers
    markers_pytimes = [tc.et2datetime(iii) for iii in tlimet]
    markers_labels = [datetime.strftime(iii, '%Y-%j') for iii in markers_pytimes]
    
    # update Cassini data
    cd.set_timeframe(tlimet)
    cd.update()
            
    print('Plotting...')
    # Define subplot arrangement
    fig = plt.figure()
    fig.set_size_inches(21,16)
    fig.suptitle(title, fontsize=20, y=0.9)

    gs1 = gridspec.GridSpec(3, 1)
    gs1.update(left=0, right=0.15)
    ax1 = plt.subplot(gs1[0, 0])
    ax2 = plt.subplot(gs1[1, 0])
    ax3 = plt.subplot(gs1[2, 0])

    gs2 = gridspec.GridSpec(6, 2, height_ratios=[3,3,3,3,3,3], width_ratios=[80,1])
    gs2.update(left=0.2, right=1, hspace=0, wspace=0.03)
    bx1 = plt.subplot(gs2[0, 0])
    bx2 = plt.subplot(gs2[1, 0], sharex=bx1)
    bx3 = plt.subplot(gs2[2, 0], sharex=bx1)
    bx4 = plt.subplot(gs2[3, 0], sharex=bx1)
    bx5 = plt.subplot(gs2[4, 0], sharex=bx1)
    bx6 = plt.subplot(gs2[5, 0], sharex=bx1)
#    bx7 = plt.subplot(gs2[6, 0], sharex=bx1)
        
    #============================
    # plot x-y
    # trajectory with data coverage
    tmp = cd.get_locations(times, 'KSM')
    ax1.plot(tmp[1], tmp[2], ls='-', color=myred, lw=5, zorder=5)
    # extended trajectory
    tmp = cd.get_locations(times_ext, 'KSM')
    ax1.plot(tmp[1], tmp[2], 'k-', lw=0.5, zorder=5)
    tmp = cd.get_locations(tlimet, 'KSM')
    for jjj in range(len(tlimet_ext)):
        ax1.text(tmp[1][jjj], tmp[2][jjj]+3, markers_labels[jjj], color=myred,
                 ha='right', 
                 rotation=-45,
                 va='bottom')
    # do some setup and plot MP model
    ax1.set_xlim([-40,40])
    ax1.set_ylim([-40,40])
    ax1.set_xticks(np.arange(-40,41,20))
    ax1.set_yticks(np.arange(-40,41,20))
    [xmp, rmp] = mploc(0.01)
    ax1.plot(xmp,rmp,'k-')
    ax1.plot(xmp,-rmp,'k-')
    [xmp, rmp] = mploc(0.1)
    ax1.plot(xmp,rmp,'k-')
    ax1.plot(xmp,-rmp,'k-')
    ax1.fill_between(xmp,0,rmp, facecolor=myblue, alpha=0.1)
    ax1.fill_between(xmp,-rmp,0,facecolor=myblue, alpha=0.1)
    ax1.set_aspect('equal')
    ax1.set_xlabel('$X_{KSM}$')
    ax1.set_ylabel('$Y_{KSM}$')
    ax1.grid()
    ax1.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7', zorder=5))
    ax1.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0', zorder=5))
    ax1.text(0.1, 0.9, '(a)', transform=ax1.transAxes,
             fontsize=20, color='k', fontweight='bold',
             ha='center', va='center')
    
    #============================
    # plot x-z
    # trajectory with data coverage
    tmp = cd.get_locations(times, 'KSM')
    ax2.plot(tmp[1], tmp[3], ls='-', c=myred, lw=5, zorder=5)
    # extended trajectory
    tmp = cd.get_locations(times_ext, 'KSM')
    ax2.plot(tmp[1], tmp[3], 'k-', lw=0.5, zorder=5)
    # Plot dot markers
    tmp = cd.get_locations(tlimet, 'KSM')
    for jjj in range(len(tlimet_ext)):
        ax2.text(tmp[1][jjj], tmp[3][jjj]-3, markers_labels[jjj], color=myred,
                 ha='right', 
                 rotation=45,
                 va='top')
    # do some setup and plot MP model
    ax2.set_xlim([-40,40])
    ax2.set_ylim([-40,40])
    ax2.set_xticks(np.arange(-40,41,20))
    ax2.set_yticks(np.arange(-40,41,20))
    [xmp, rmp] = mploc(0.01)
    ax2.plot(xmp,rmp,'k-')
    ax2.plot(xmp,-rmp,'k-')
    [xmp, rmp] = mploc(0.1)
    ax2.plot(xmp,rmp,'k-')
    ax2.plot(xmp,-rmp,'k-') 
    ax2.set_aspect('equal')
    ax2.set_xlabel('$X_{KSM}$')
    ax2.set_ylabel('$Z_{KSM}$')
    ax2.grid()
    ax2.plot([0,0],[-5,5], c='k', lw=0.5, zorder=4)
    ax2.plot([-60,xmp[np.where(rmp>1)[0][0]-5]],[0,0], c=myblue, alpha=0.1, lw=5)
    ax2.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7', zorder=5))
    ax2.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0', zorder=5))
    ax2.text(0.1, 0.9, '(b)', transform=ax2.transAxes,
             fontsize=20, color='k', fontweight='bold',
             ha='center', va='center')
    
    #============================
    # plot y-z
    # trajectory with data coverage
    tmp = cd.get_locations(times, 'KSM')
    ax3.plot(tmp[2], tmp[3], ls='-', c=myred, lw=5, zorder=5)
    # extended trajectory
    tmp = cd.get_locations(times_ext, 'KSM')
    ax3.plot(tmp[2], tmp[3], 'k-', lw=0.5, zorder=5)
    tmp = cd.get_locations(tlimet, 'KSM')
    # do some setup and plot MP model
    ax3.set_xlim([-40,40])
    ax3.set_ylim([-40,40])
    ax3.set_xticks(np.arange(-40,40,20))
    ax3.set_yticks(np.arange(-40,41,20))
    ax3.set_aspect('equal')
    ax3.set_xlabel('$Y_{KSM}$')
    ax3.set_ylabel('$Z_{KSM}$')
    ax3.grid()
    ax3.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7', zorder=5))
    ax3.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0.7', zorder=5))
    ax3.text(0.1, 0.9, '(c)', transform=ax3.transAxes,
             fontsize=20, color='k', fontweight='bold',
             ha='center', va='center')
    
    def nodata(ax):
        ax.text(0.5,0.5, 'no data', ha='center', va='center', fontsize=20,
                transform = ax.transAxes)
    
    # Plot B field in KRTP
    if np.any(magdata.ettimes):
        bx1.plot(magdata.ettimes, magdata.data[:,0], c='k', lw=1)
    else:
        nodata(bx1)
    bx1.set_ylabel(r'$B_{R}$ (nT)')
    
    if np.any(magdata.ettimes):
        bx2.plot(magdata.ettimes, magdata.data[:,1], c='k', lw=1)
    else:
        nodata(bx2)
    bx2.set_ylabel(r'$B_{T}$ (nT)')
    
    if np.any(magdata.ettimes):
        bx3.plot(magdata.ettimes, magdata.data[:,2], c='k', lw=1)
    else:
        nodata(bx3)
    bx3.set_ylabel(r'$B_{P}$ (nT)')
    
    if np.any(magdata.ettimes):
        bx4.plot(magdata.ettimes, np.linalg.norm(magdata.data, axis=1), c='k', lw=1)
    else:
        nodata(bx4)
    bx4.set_ylabel(r'$B_{tot}$ (nT)')
    
    # Plot RPWS spectrum
    dataplotted = False
    if np.any(rpwsdata.data):
        dataplotted = True
        data = np.transpose(rpwsdata.data)
        data = ma.masked_invalid(data)
        ppp = bx5.pcolormesh(rpwsdata.ettimes, rpwsdata.freq, data, cmap=cmap_UV, vmin=0, vmax=35)
        ppp.cmap.set_bad('gray')
        cx6 = plt.subplot(gs2[4, 1])
        cbar = plt.colorbar(ppp, cax=cx6)
        cbar.set_label(r'dB over BG', rotation=270, labelpad=20)
    bx5.set_yscale('log')
    bx5.set_ylim([1.1,1.5e7])
    bx5.set_ylabel('f (Hz)')  
    
#    # Plot RPWS densities
#    bx5_twin = bx5.twinx()
#    if np.any(rpwsdata.dens_e_val):
#        dataplotted = True
#        bx5_twin.plot(rpwsdata.dens_e_et, rpwsdata.dens_e_val, c=myred, lw=2)
#        bx5_twin.scatter(rpwsdata.dens_e_et, rpwsdata.dens_e_val,
#                         marker='o', color=myred, s=10)
#    bx5_twin.set_ylabel('e$^-$ density (cm$^{-3}$)', rotation=270, labelpad=15)
#    bx5_twin.set_yscale('log')
#    tmp = bx5_twin.get_ylim()
#    bx5_twin.set_ylim([np.min([tmp[0],1e-3]),
#                       np.max([tmp[1],1e0])])
    
    if not dataplotted:
        nodata(bx5)
        
    # Plot LEMMS spectrum
    if np.any(lemmsdata.data):
        for iii in range(len(lemmsdata.energy_lo)):
            l = len(lemmsdata.energy_lo)
            v = iii*1/l
            c = cmap_SPEC_short(v)
            ys = lemmsdata.data[:,iii]
            bx6.plot(lemmsdata.ettimes, ys,
                        color=c, lw=0.5)
            bx6.scatter(lemmsdata.ettimes, ys,
                        color=c, s=1)
            tmp = bx6.get_position().bounds
            bx6.text(tmp[0]+tmp[2]+0.005, tmp[1]+tmp[3]-(iii+2)/8*(tmp[3]),
                     '{}-{} keV'.format(int(lemmsdata.energy_lo[iii]),int(lemmsdata.energy_hi[iii])),
                     color=c, transform=fig.transFigure)
    else:
        nodata(bx6)
    bx6.set_yscale('log')
    bx6.set_ylim([1.1e-4,0.9e2])
    bx6.set_ylabel('Diff. e$^-$ flux\n(1/s/sr/cm$^2$/keV)')  
    
#    # Plot INCA neutral counts
#    if np.any(incadata.incaaccvalues):
#        val = np.where(incadata.incaacc_dt > datetime(2014,4,10,0))[0]
#        bx7.scatter(tc.datetime2et(incadata.incaacc_dt[val]), incadata.incaaccvalues[val],
#                    s=5, color='k', marker='o', label='data')
#        
#        #ins = np.where(np.diff(inca_acc_df.index)>pd.Timedelta(minutes=dt))[0]+1
#        #incaacc_dt = np.insert(incaacc_dt, ins, incaacc_dt[ins]-timedelta(seconds=3))
#        #incaaccvalues = np.insert(incaaccvalues, ins, np.nan)
#        val = np.where(tc.datetime2et(incadata.inca_acc_df.index.tolist())
#                > tc.datetime2et(datetime(2014,4,10,0)))[0]
#        bx7.plot(tc.datetime2et(np.array(incadata.inca_acc_df.index.tolist())[val]), 
#                 np.array(incadata.inca_acc_df['VALUES'].tolist())[val],
#                 color=myred, ls='-', label='10 min average')
#        bx7.legend(loc=1)
#    else:
#        nodata(bx7)
#    bx7.set_ylim([0,35])
#    bx7.set_ylabel('Diff. ENA flux\n (1/cm$^2$/s/sr/keV)')

    # Put proper x-axis labels
    pytlim = tc.et2datetime(tlimet)
    numticks = 7
    delta = (pytlim[1] - pytlim[0])/(numticks-1)
    ticklist = np.array([pytlim[0]+(iii+0.5)*delta for iii in range(numticks-1)])
    
    # all hours in the field of view
    start = datetime(pytlim[0].year, pytlim[0].month, pytlim[0].day, pytlim[0].hour)
    while (pytlim[0]-start).total_seconds() > 0:
        start = start + timedelta(hours=1)
    ticklist_full = [start]
    while ticklist_full[-1] <= pytlim[-1]-timedelta(hours=1):
        ticklist_full.append(ticklist_full[-1]+timedelta(hours=1))
    ticklistet_full = tc.datetime2et(ticklist_full)
    if pytlim[1]-pytlim[0]>=timedelta(hours=18):
        ticklist = ticklist_full[::3]
        ticklistet = ticklistet_full[::3]
    elif pytlim[1]-pytlim[0]>=timedelta(hours=12):
        ticklist = ticklist_full[::2]
        ticklistet = ticklistet_full[::2]
    else:
        ticklist = ticklist_full
        ticklistet = ticklistet_full
        
        
    thisLOC = cd.get_locations(ticklistet, 'KRTP')
    ticlab = ['%s\n%.1f\n%.1f\n%.1f' % (datetime.strftime(ticklist[iii],'%j/%H:%M'),
                                        thisLOC[1][iii],
                                        thisLOC[2][iii],
                                        thisLOC[3][iii]) for iii in range(len(ticklistet))]
    for bx in [bx1,bx2,bx3,bx4,bx5,bx6]:
        bx.set_xticks(ticklistet)
        bx.set_xticklabels(ticlab)
        bx.set_xticks(ticklistet_full, minor=True)
        bx.tick_params(which='both', direction='in', bottom=True, top=True)
        bx.tick_params(axis='y', which='both', direction='out', left=True, right=True)
        if bx != bx6:
            plt.setp(bx.get_xticklabels(), visible=False)
            
    for bx in [bx1,bx2,bx3,bx4,bx5,bx6]:
        bx.grid(which='major', axis='both',
                color='k', linestyle='-', linewidth=0.5, alpha=0.5) 
        bx.grid(which='minor', axis='x' if bx in [bx5, bx6] else 'both',
                color='k', linestyle='--', linewidth=0.5, alpha=0.6)

    # Label description at the right
    ticklab = bx6.xaxis.get_ticklabels()[0]
    trans = ticklab.get_transform()
    bx6.xaxis.set_label_coords(tlimet[0]-np.diff(tlimet)/16, 0, transform=trans)
    bx6.set_xlabel('DOY/HH:MM\n r (RS) \n el (deg)\n LT (h)')
    bx6.set_xlim(tlimet)

    # Vertical greyed areas indicating UVIS images
    tmp = uvistimes.loc[(uvistimes['ET_STOP']>tlimet[0]) | (uvistimes['ET_START']<tlimet[1]), 'ET_START'].index
    for argctr in tmp:
        thismin = np.max([tlimet[0], uvistimes.loc[argctr,'ET_START']])
        thismax = np.min([tlimet[1], uvistimes.loc[argctr,'ET_STOP']])
        for axctr in [bx1,bx2,bx3,bx4,bx5,bx6]:
            axctr.axvspan(thismin, thismax, alpha=0.3, color='grey')
            
    # Vertical colored areas indicating HST images
    fmt = '%Y-%j %H:%M:%S'
    lt = 4510 #light time correction
    hststart = np.array([
            '2014-100 00:39:49',
#            '2014-100 00:57:27',
            '2014-100 01:11:42',
            '2014-100 02:08:42',
#            '2014-100 02:28:40',
            '2014-100 02:42:55',
            '2014-100 03:44:20',
#            '2014-100 04:04:18',
            '2014-100 04:18:33',
            ])
    hststart = np.array([tc.datetime2et(datetime.strptime(iii, fmt)-timedelta(seconds=lt))
                            for iii in hststart])
    exp = np.array([
            700,
#            540 ,
            700,
            840,
#            540,
            840,
            840,
#            540,
            840,
            ])
    numbers = '123456'
    for ctr in range(len(hststart)):
        for axctr in [bx1,bx2,bx3,bx4,bx5,bx6]:
            axctr.axvspan(hststart[ctr], hststart[ctr]+exp[ctr], alpha=0.3, color='grey')
        bx1.text(np.mean([hststart[ctr], hststart[ctr]+exp[ctr]]),
                 bx1.get_ylim()[1]-0.05*np.diff(bx1.get_ylim()),
                 numbers[ctr], va='top', ha='center',
                 fontsize=12, fontweight='bold',
                 transform = bx1.transData)
            
    panels = 'defghijk'
    bxs = [bx1,bx2,bx3,bx4,bx5,bx6]
    for axctr in range(len(bxs)):
        ax = bxs[axctr]
        ax.text(0.017, 0.5 if axctr<4 else 0.9, '({})'.format(panels[axctr]), ha='center', va='center',
                transform=ax.transAxes, color='k' if ax != bx5 else 'w',
                fontsize=20, fontweight='bold')
            
    plt.savefig('{}.png'.format(savefile), bbox_inches='tight', dpi=mydpi)
#    plt.savefig('{}.pdf'.format(savefile), bbox_inches='tight')
    plt.close()


etstart = tc.datetime2et(datetime.strptime('2014-99 18:00:00', '%Y-%j %H:%M:%S'))
etstop = tc.datetime2et(datetime.strptime('2014-100 18:00:00', '%Y-%j %H:%M:%S'))

plot_combined_data(etstart,etstop,'{}/2014_100_data'.format(samplepath),'2014-04-09 (DOY 99) to 2014-04-10 (DOY 100)')


#etstart = tc.datetime2et(datetime.strptime('2014-100 06:00:00', '%Y-%j %H:%M:%S'))
#etstop = tc.datetime2et(datetime.strptime('2014-100 12:00:00', '%Y-%j %H:%M:%S'))
#plot_combined_data(etstart,etstop,'{}/2014_100_data_short01'.format(samplepath),'2014-04-10 (DOY 100)')
#
#etstart = tc.datetime2et(datetime.strptime('2014-99 23:00:00', '%Y-%j %H:%M:%S'))
#etstop = tc.datetime2et(datetime.strptime('2014-100 05:00:00', '%Y-%j %H:%M:%S'))
#plot_combined_data(etstart,etstop,'{}/2014_100_data_short02'.format(samplepath),'2014-04-09 (DOY 99) to 2014-04-10 (DOY 100)')



