# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import ppo_mag_phases
import scipy.optimize as sopt

myblue='royalblue'
myred='crimson'

pmp = ppo_mag_phases.PPOMagPhases(datapath)

flashfile = '{}/imglists/UVIS_flashes_tmp.txt'.format(boxpath)
flashes = pd.read_csv(flashfile, header=0, sep='\t')
freqfile = '{}/imglists/UVIS_freq_tmp.txt'.format(boxpath)
freq = pd.read_csv(freqfile, header=0, sep='\t')

#sys.exit()

savepath = '{}/UV_seq_new/stats'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

def make_hist(data, bins):
    tmp = np.digitize(data, bins)-1
    return np.array([np.size(np.where(tmp==iii)[0]) for iii in range(len(bins)-1)])

def linear(x,t):
    return x+t

flashes['LONRAD'] = flashes['LTLOCATION']/12*np.pi
flashes['PPO_PSI_N'] = (pmp.getMagPhase(flashes['ETTIME'], 'N')-flashes['LONRAD']) % (2*np.pi)
flashes['PPO_PSI_S'] = (pmp.getMagPhase(flashes['ETTIME'], 'S')-flashes['LONRAD']) % (2*np.pi)
flashes['PPO_PHI_N'] = (pmp.getMagPhase(flashes['ETTIME'], 'N')-np.pi) % (2*np.pi)
flashes['PPO_PHI_S'] = (pmp.getMagPhase(flashes['ETTIME'], 'S')-np.pi) % (2*np.pi)

flashes['GW_clean'] = flashes['GW']
flashes.at[np.isnan(flashes['LTLOCATION']),'GW_clean'] = np.nan
ipp_power = pd.DataFrame(np.diff(flashes['ETTIME']), columns=['IPP'])
ipp_power['PRE'] = flashes['GW_clean'][:-1]
ipp_power['POST'] = flashes[1:].reset_index()['GW_clean']
ipp_power.at[ipp_power['IPP']>5400, ['PRE','POST']] = np.nan
ipp_power['DIFF'] = ipp_power['POST'] - ipp_power['PRE']
ipp_power['RATIO'] = ipp_power['POST']/ipp_power['PRE']
ipp_power['RATIO_lin1'] = np.nan
ipp_power.at[ipp_power['RATIO']>=1, 'RATIO_lin1'] = ipp_power['RATIO']
ipp_power.at[ipp_power['RATIO']<1, 'RATIO_lin1'] = 1-(1/ipp_power['RATIO']-1)

subpan = 'abcdefghijklmnopqrstuvwxyz'
text_top = 0.92
text_left = 0.04
text_fs = 18

width_mult = 0.7

# =============================================================================
# figure for paper
# =============================================================================

fig, axx = plt.subplots(4,2)
fig.set_size_inches(16,16)

plotit = iter(range(np.size(axx)))

ctr = next(plotit)
ax = axx[ctr//2, ctr%2]
bins = np.linspace(0,30,num=21)
hist = make_hist(flashes.loc[np.isfinite(flashes['LTLOCATION']), 'GW'], bins)
ax.bar(bins[:-1]+np.diff(bins)/2, hist, width=width_mult*np.diff(bins), edgecolor='k', 
        color='grey', alpha=0.7, zorder=3)
ax.bar(bins[-1]-np.mean(np.diff(bins)/2), len(np.where(flashes['GW']>=bins[-1])[0]),
       width=width_mult*np.diff(bins), bottom=hist[-1], color='w', linewidth=1.5,
       edgecolor='k', alpha=0.6, zorder=3, hatch='///')
ax.set_xlim([bins[0], bins[-1]])
ax.set_xlabel('Pulse power (GW)')
ax.set_xticks(np.arange(0,bins[-1]+0.001,5))
ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
ax.set_ylabel('Number of pulses')
ax.grid(zorder=0)
ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
        color='k', fontsize=text_fs, fontweight='bold',
        ha='center', va='center')

ctr = next(plotit)
ax = axx[ctr//2, ctr%2]
bins = np.linspace(28*60,80*60,num=14)
data = np.diff(flashes['ETTIME'])
#data = ipp_power.loc[np.isfinite(ipp_power['DIFF']), 'IPP']
hist = make_hist(data, bins)
ax.bar(bins[:-1]+np.diff(bins)/2, hist, width=width_mult*np.diff(bins), edgecolor='k', 
        color='grey', alpha=0.6, zorder=3)
ax.set_xlim([bins[0], bins[-1]])
ax.set_xlabel('Interpulse period (min)')
tmp = np.arange(bins[0],bins[-1]+1,240)
ax.set_xticks(tmp)
ax.set_xticklabels((tmp/60).astype(int))
ax.set_ylabel('Number of interpulses')
ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
ax.grid(zorder=0)
ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
        color='k', fontsize=text_fs, fontweight='bold',
        ha='center', va='center')

ctr = next(plotit)
ax = axx[ctr//2, ctr%2]
bins = np.linspace(0,24,num=13)
hist = make_hist(flashes['LTLOCATION'], bins)
ax.bar(bins[:-1]+np.diff(bins)/2, hist, width=width_mult*np.diff(bins), edgecolor='k',
        color='grey', alpha=0.6, zorder=3)
ax.set_xlim([0,24])
ax.set_xlabel('LT (hrs)')
ax.set_xticks(np.arange(0,25,3))
ax.set_xticks(np.arange(0,25,3))
ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
ax.set_ylabel('Number of pulses')
ax.grid(zorder=0)
ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
        color='k', fontsize=text_fs, fontweight='bold',
        ha='center', va='center')

ctr = next(plotit)
ax = axx[ctr//2, ctr%2]
bins = np.linspace(0,24,num=13)
tmp = np.digitize(flashes['LTLOCATION'], bins)-1
ltpower_mean = np.array([np.nanmean(flashes.loc[tmp==iii, 'GW'])
                            for iii in range(len(bins)-1)])
ax.bar(bins[:-1]+np.diff(bins)/2, ltpower_mean, width=width_mult*np.mean(np.diff(bins)),
        edgecolor='k', color='grey', alpha=0.6, zorder=3)
yerrors = np.array([np.nanstd(flashes.loc[tmp==iii, 'GW'])
                            for iii in range(len(bins)-1)])
ax.errorbar(bins[:-1]+np.diff(bins)/2, ltpower_mean,
             yerr=[np.zeros_like(yerrors),yerrors], color='k',
             linewidth=0, elinewidth=1, capsize=3)
ax.set_xlim([0, 24])
ax.set_xlabel('LT (hrs)')
ax.set_xticks(np.arange(0,25,3))
ax.set_ylabel('Mean pulse power (GW)')
ax.grid(zorder=0)
ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
        color='k', fontsize=text_fs, fontweight='bold',
        ha='center', va='center')

for ppoctr in ['N', 'S']:
    ctr = next(plotit)
    ax = axx[ctr//2, ctr%2]
    ax2 = plt.twinx(ax)
    bins = np.linspace(0,360,num=9)
    tmp = np.digitize(flashes.loc[flashes['HEMISPHERE']=='North', 'PPO_PHI_{}'.format(ppoctr)]*180/np.pi, bins)-1
    ppo_num = np.array([np.sum(np.isfinite(flashes.loc[tmp==iii, 'GW']))
                                for iii in range(len(bins)-1)])
    ppopower_mean = np.array([np.nanmean(flashes.loc[np.where((tmp==iii) & (flashes.loc[flashes['HEMISPHERE']=='North','LTLOCATION']>12))[0], 'GW'])
                                for iii in range(len(bins)-1)])
    ax.bar(bins[:-1]+np.diff(bins)/2-width_mult*np.mean(np.diff(bins))/3.5, ppo_num, width=0.4*width_mult*np.mean(np.diff(bins)),
            edgecolor='k', color=myred if ppoctr=='N' else myblue, alpha=0.6, zorder=3)
    ax2.bar(bins[:-1]+np.diff(bins)/2+width_mult*np.mean(np.diff(bins))/3.5, ppopower_mean, width=0.4*width_mult*np.mean(np.diff(bins)),
            linewidth=1.5, edgecolor='k', color='lightyellow', alpha=0.6, zorder=3, hatch='///', label='Power')
    yerrors = np.array([np.nanstd(flashes.loc[np.where((tmp==iii) & (flashes.loc[flashes['HEMISPHERE']=='North','LTLOCATION']>12))[0], 'GW'])
                                for iii in range(len(bins)-1)])
    ax2.errorbar(bins[:-1]+np.diff(bins)/2+width_mult*np.mean(np.diff(bins))/3.5, ppopower_mean,
                 yerr=[np.zeros_like(yerrors),yerrors], color='k',
                 linewidth=0, elinewidth=1, capsize=3)
    ax2.bar(20, 0, width=0,
            linewidth=1, edgecolor='k', color=myred if ppoctr=='N' else myblue, alpha=0.6, zorder=3, label='Number')
    ax.set_xlim([0, 360])
    ax.set_xlabel('$\Phi_\mathrm{{{}}}$ (deg)'.format(ppoctr))
    ax.set_xticks(np.arange(0,361,45))
    ax.set_ylabel('Number of pulses, North')
    ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
    ax.grid(zorder=0)
    ax2.legend(loc=3, framealpha=1, ncol=2)
    ax2.set_ylabel('Mean power, North (GW)', rotation=270, labelpad=12)
    ax2.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax2.transAxes,
            color='k', fontsize=text_fs, fontweight='bold',
            ha='center', va='center')
        
    ctr = next(plotit)
    ax = axx[ctr//2, ctr%2]
    ax2 = plt.twinx(ax)
    bins = np.linspace(0,360,num=9)
    tmp = np.digitize(flashes.loc[flashes['HEMISPHERE']=='North', 'PPO_PSI_{}'.format(ppoctr)]*180/np.pi, bins)-1
    ppo_num = np.array([np.sum(np.isfinite(flashes.loc[tmp==iii, 'GW']))
                                for iii in range(len(bins)-1)])
    ppopower_mean = np.array([np.nanmean(flashes.loc[np.where((tmp==iii) & (flashes.loc[flashes['HEMISPHERE']=='North','LTLOCATION']>12))[0], 'GW'])
                                for iii in range(len(bins)-1)])
    ax.bar(bins[:-1]+np.diff(bins)/2-width_mult*np.mean(np.diff(bins))/3.5, ppo_num, width=0.4*width_mult*np.mean(np.diff(bins)),
            edgecolor='k', color=myred if ppoctr=='N' else myblue, alpha=0.6, zorder=3)
    ax2.bar(bins[:-1]+np.diff(bins)/2+width_mult*np.mean(np.diff(bins))/3.5, ppopower_mean, width=0.4*width_mult*np.mean(np.diff(bins)),
            linewidth=1.5, edgecolor='k', color='lightyellow', alpha=0.6, zorder=3, hatch='///', label='Power')
    yerrors = np.array([np.nanstd(flashes.loc[np.where((tmp==iii) & (flashes.loc[flashes['HEMISPHERE']=='North','LTLOCATION']>12))[0], 'GW'])
                                for iii in range(len(bins)-1)])
    ax2.errorbar(bins[:-1]+np.diff(bins)/2+width_mult*np.mean(np.diff(bins))/3.5, ppopower_mean,
                 yerr=[np.zeros_like(yerrors),yerrors], color='k',
                 linewidth=0, elinewidth=1, capsize=3)
    ax2.bar(20, 0, width=0,
            linewidth=1, edgecolor='k', color=myred if ppoctr=='N' else myblue, alpha=0.6, zorder=3, label='Number')
    ax.set_xlim([0, 360])
    ax.set_xlabel('$\Psi_\mathrm{{{}}}$ (deg)'.format(ppoctr))
    ax.set_xticks(np.arange(0,361,45))
    ax.set_ylabel('Number of pulses, North')
    ax.set_yticks(np.arange(0,ax.get_ylim()[1],3))
    ax.grid(zorder=0)
    ax2.legend(loc=3, framealpha=1, ncol=2)
    ax2.set_ylabel('Mean power, North (GW)', rotation=270, labelpad=12)
    ax2.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax2.transAxes,
            color='k', fontsize=text_fs, fontweight='bold',
            ha='center', va='center') 
        
plt.savefig('{}/basic_stats.png'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.savefig('{}/basic_stats.pdf'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.show()
plt.close()

sys.exit()

# =============================================================================
# dusk only
# =============================================================================

flashes['IS_DUSK'] = 0
flashes.at[(flashes['LTLOCATION']>13) & (flashes['LTLOCATION']<20), 'IS_DUSK'] =1

fig, axx = plt.subplots(4,2)
fig.set_size_inches(16,16)

plotit = iter(range(np.size(axx)))

ctr = next(plotit)
ax = axx[ctr//2, ctr%2]
bins = np.linspace(0,30,num=21)
hist = make_hist(flashes.loc[(np.isfinite(flashes['LTLOCATION'])) & 
                              (flashes['IS_DUSK']==1), 'GW'], bins)
ax.bar(bins[:-1]+np.diff(bins)/2, hist, width=width_mult*np.diff(bins), edgecolor='k', 
        color='grey', alpha=0.7, zorder=3)
ax.bar(bins[-1]-np.mean(np.diff(bins)/2), len(np.where(flashes['GW']>=bins[-1])[0]),
       width=width_mult*np.diff(bins), bottom=hist[-1], color='w', linewidth=1.5,
       edgecolor='k', alpha=0.6, zorder=3, hatch='///')
ax.set_xlim([bins[0], bins[-1]])
ax.set_xlabel('Pulse power (GW)')
ax.set_xticks(np.arange(0,bins[-1]+0.001,5))
ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
ax.set_ylabel('Number of pulses')
ax.grid(zorder=0)
ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
        color='k', fontsize=text_fs, fontweight='bold',
        ha='center', va='center')

ctr = next(plotit)
ax = axx[ctr//2, ctr%2]
bins = np.linspace(24*60,80*60,num=15)
data = np.diff(flashes['ETTIME'])
#data = ipp_power.loc[np.isfinite(ipp_power['DIFF']), 'IPP']
hist = make_hist(data, bins)
ax.bar(bins[:-1]+np.diff(bins)/2, hist, width=width_mult*np.diff(bins), edgecolor='k', 
        color='grey', alpha=0.6, zorder=3)
ax.set_xlim([bins[0], bins[-1]])
ax.set_xlabel('Interpulse period (min)')
tmp = np.arange(bins[0],bins[-1]+1,240)
ax.set_xticks(tmp)
ax.set_xticklabels((tmp/60).astype(int))
ax.set_ylabel('Number of interpulses')
ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
ax.grid(zorder=0)
ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
        color='k', fontsize=text_fs, fontweight='bold',
        ha='center', va='center')

ctr = next(plotit)
ax = axx[ctr//2, ctr%2]
bins = np.linspace(0,24,num=19)
hist = make_hist(flashes.loc[(flashes['IS_DUSK']==1), 'LTLOCATION'], bins)
ax.bar(bins[:-1]+np.diff(bins)/2, hist, width=width_mult*np.diff(bins), edgecolor='k',
        color='grey', alpha=0.6, zorder=3)
ax.set_xlim([0,24])
ax.set_xlabel('LT (hrs)')
ax.set_xticks(np.arange(0,25,3))
ax.set_xticks(np.arange(0,25,3))
ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
ax.set_ylabel('Number of pulses')
ax.grid(zorder=0)
ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
        color='k', fontsize=text_fs, fontweight='bold',
        ha='center', va='center')

ctr = next(plotit)
ax = axx[ctr//2, ctr%2]
bins = np.linspace(0,24,num=13)
tmp = np.digitize(flashes.loc[(flashes['IS_DUSK']==1), 'LTLOCATION'], bins)-1
ltpower_mean = np.array([np.nanmean(flashes.loc[tmp==iii, 'GW'])
                            for iii in range(len(bins)-1)])
ax.bar(bins[:-1]+np.diff(bins)/2, ltpower_mean, width=width_mult*np.mean(np.diff(bins)),
        edgecolor='k', color='grey', alpha=0.6, zorder=3)
yerrors = np.array([np.nanstd(flashes.loc[tmp==iii, 'GW'])
                            for iii in range(len(bins)-1)])
ax.errorbar(bins[:-1]+np.diff(bins)/2, ltpower_mean,
             yerr=[np.zeros_like(yerrors),yerrors], color='k',
             linewidth=0, elinewidth=1, capsize=3)
ax.set_xlim([0, 24])
ax.set_xlabel('LT (hrs)')
ax.set_xticks(np.arange(0,25,3))
ax.set_ylabel('Mean pulse power (GW)')
ax.grid(zorder=0)
ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
        color='k', fontsize=text_fs, fontweight='bold',
        ha='center', va='center')

for ppoctr in ['N', 'S']:
    ctr = next(plotit)
    ax = axx[ctr//2, ctr%2]
    ax2 = plt.twinx(ax)
    bins = np.linspace(0,360,num=9)
    tmp = np.digitize(flashes.loc[(flashes['HEMISPHERE']=='North') & (flashes['IS_DUSK']==1), 'PPO_PHI_{}'.format(ppoctr)]*180/np.pi, bins)-1
    ppo_num = np.array([np.sum(np.isfinite(flashes.loc[tmp==iii, 'GW']))
                                for iii in range(len(bins)-1)])
    ppopower_mean = np.array([np.nanmean(flashes.loc[np.where((tmp==iii) & (flashes.loc[(flashes['HEMISPHERE']=='North')&(flashes['IS_DUSK']==1),'LTLOCATION']>12))[0], 'GW'])
                                for iii in range(len(bins)-1)])
    ax.bar(bins[:-1]+np.diff(bins)/2-width_mult*np.mean(np.diff(bins))/3.5, ppo_num, width=0.4*width_mult*np.mean(np.diff(bins)),
            edgecolor='k', color=myred if ppoctr=='N' else myblue, alpha=0.6, zorder=3)
    ax2.bar(bins[:-1]+np.diff(bins)/2+width_mult*np.mean(np.diff(bins))/3.5, ppopower_mean, width=0.4*width_mult*np.mean(np.diff(bins)),
            linewidth=1.5, edgecolor='k', color='lightyellow', alpha=0.6, zorder=3, hatch='///', label='Power')
    yerrors = np.array([np.nanstd(flashes.loc[np.where((tmp==iii) & (flashes.loc[(flashes['HEMISPHERE']=='North')
                                                            &(flashes['IS_DUSK']==1),'LTLOCATION']>12))[0], 'GW'])
                                for iii in range(len(bins)-1)])
    ax2.errorbar(bins[:-1]+np.diff(bins)/2+width_mult*np.mean(np.diff(bins))/3.5, ppopower_mean,
                 yerr=[np.zeros_like(yerrors),yerrors], color='k',
                 linewidth=0, elinewidth=1, capsize=3)
    ax2.bar(20, 0, width=0,
            linewidth=1, edgecolor='k', color=myred if ppoctr=='N' else myblue, alpha=0.6, zorder=3, label='Number')
    ax.set_xlim([0, 360])
    ax.set_xlabel('$\Phi_\mathrm{{{}}}$ (deg)'.format(ppoctr))
    ax.set_xticks(np.arange(0,361,45))
    ax.set_ylabel('Number of pulses, North')
    ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
    ax.grid(zorder=0)
    ax2.legend(loc=1, framealpha=1)
    ax2.set_ylabel('Mean power, North (GW)', rotation=270, labelpad=12)
    ax2.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax2.transAxes,
            color='k', fontsize=text_fs, fontweight='bold',
            ha='center', va='center')
        
    ctr = next(plotit)
    ax = axx[ctr//2, ctr%2]
    ax2 = plt.twinx(ax)
    bins = np.linspace(0,360,num=9)
    tmp = np.digitize(flashes.loc[(flashes['HEMISPHERE']=='North') &(flashes['IS_DUSK']==1), 'PPO_PSI_{}'.format(ppoctr)]*180/np.pi, bins)-1
    ppo_num = np.array([np.sum(np.isfinite(flashes.loc[tmp==iii, 'GW']))
                                for iii in range(len(bins)-1)])
    ppopower_mean = np.array([np.nanmean(flashes.loc[np.where((tmp==iii) & (flashes.loc[(flashes['HEMISPHERE']=='North') & (flashes['IS_DUSK']==1),'LTLOCATION']>12))[0], 'GW'])
                                for iii in range(len(bins)-1)])
    ax.bar(bins[:-1]+np.diff(bins)/2-width_mult*np.mean(np.diff(bins))/3.5, ppo_num, width=0.4*width_mult*np.mean(np.diff(bins)),
            edgecolor='k', color=myred if ppoctr=='N' else myblue, alpha=0.6, zorder=3)
    ax2.bar(bins[:-1]+np.diff(bins)/2+width_mult*np.mean(np.diff(bins))/3.5, ppopower_mean, width=0.4*width_mult*np.mean(np.diff(bins)),
            linewidth=1.5, edgecolor='k', color='lightyellow', alpha=0.6, zorder=3, hatch='///', label='Power')
    yerrors = np.array([np.nanstd(flashes.loc[np.where((tmp==iii) & (flashes.loc[(flashes['HEMISPHERE']=='North')&(flashes['IS_DUSK']==1),'LTLOCATION']>12))[0], 'GW'])
                                for iii in range(len(bins)-1)])
    ax2.errorbar(bins[:-1]+np.diff(bins)/2+width_mult*np.mean(np.diff(bins))/3.5, ppopower_mean,
                 yerr=[np.zeros_like(yerrors),yerrors], color='k',
                 linewidth=0, elinewidth=1, capsize=3)
    ax2.bar(20, 0, width=0,
            linewidth=1, edgecolor='k', color=myred if ppoctr=='N' else myblue, alpha=0.6, zorder=3, label='Number')
    ax.set_xlim([0, 360])
    ax.set_xlabel('$\Psi_\mathrm{{{}}}$ (deg)'.format(ppoctr))
    ax.set_xticks(np.arange(0,361,45))
    ax.set_ylabel('Number of pulses, North')
    ax.set_yticks(np.arange(0,ax.get_ylim()[1],3))
    ax.grid(zorder=0)
    ax2.legend(loc=1, framealpha=1)
    ax2.set_ylabel('Mean power, North (GW)', rotation=270, labelpad=12)
    ax2.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax2.transAxes,
            color='k', fontsize=text_fs, fontweight='bold',
            ha='center', va='center') 
    
plt.savefig('{}/basic_stats_dusk.png'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.savefig('{}/basic_stats_dusk.pdf'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.show()
plt.close()

# =============================================================================
# old stuff
# =============================================================================

for ppoctr in ['N', 'S']:
    fig, axx = plt.subplots(4,1)
    fig.set_size_inches(8,15)
    
    plotit = iter(range(len(axx)))
    
    ctr = next(plotit)
    ax = axx[ctr]
    bins = np.linspace(0,360,num=19)
    tmp = np.digitize(flashes.loc[flashes['HEMISPHERE']=='North', 'PPO_PHI_{}'.format(ppoctr)]*180/np.pi, bins)-1
    ppo_num = np.array([np.sum(np.isfinite(flashes.loc[tmp==iii, 'GW']))
                                for iii in range(len(bins)-1)])
    ax.bar(bins[:-1]+np.diff(bins)/2, ppo_num, width=width_mult*np.mean(np.diff(bins)),
            edgecolor='k', color='steelblue', alpha=0.7, zorder=3)
    ax.set_xlim([0, 360])
    ax.set_xlabel('$\Phi_\mathrm{{{}}}$ (deg)'.format(ppoctr))
    ax.set_xticks(np.arange(0,361,45))
    ax.set_ylabel('Number of pulses, North')
    ax.set_yticks(np.arange(0,ax.get_ylim()[1],5))
    ax.grid(zorder=0)
    ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
            color='k', fontsize=text_fs, fontweight='bold',
            ha='center', va='center')
    
    ctr = next(plotit)
    ax = axx[ctr]
    bins = np.linspace(0,360,num=19)
    tmp = np.digitize(flashes.loc[flashes['HEMISPHERE']=='North', 'PPO_PHI_{}'.format(ppoctr)]*180/np.pi, bins)-1
    ppopower_mean = np.array([np.nanmean(flashes.loc[tmp==iii, 'GW'])
                                for iii in range(len(bins)-1)])
    ax.bar(bins[:-1]+np.diff(bins)/2, ppopower_mean, width=width_mult*np.mean(np.diff(bins)),
            edgecolor='k', color='steelblue', alpha=0.7, zorder=3)
    ax.set_xlim([0, 360])
    ax.set_xlabel('$\Phi_\mathrm{{{}}}$ (deg)'.format(ppoctr))
    ax.set_xticks(np.arange(0,361,45))
    ax.set_ylabel('Mean pulse power, North (GW)')
    ax.grid(zorder=0)
    ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
            color='k', fontsize=text_fs, fontweight='bold',
            ha='center', va='center')
    
    ctr = next(plotit)
    ax = axx[ctr]
    bins = np.linspace(0,360,num=19)
    tmp = np.digitize(flashes.loc[flashes['HEMISPHERE']=='North', 'PPO_PSI_{}'.format(ppoctr)]*180/np.pi, bins)-1
    ppo_num = np.array([np.sum(np.isfinite(flashes.loc[tmp==iii, 'GW']))
                                for iii in range(len(bins)-1)])
    ax.bar(bins[:-1]+np.diff(bins)/2, ppo_num, width=width_mult*np.mean(np.diff(bins)),
            edgecolor='k', color='steelblue', alpha=0.7, zorder=3)
    ax.set_xlim([0, 360])
    ax.set_xlabel('$\Psi_\mathrm{{{}}}$ (deg)'.format(ppoctr))
    ax.set_xticks(np.arange(0,361,45))
    ax.set_ylabel('Number of pulses, North')
    ax.set_yticks(np.arange(0,ax.get_ylim()[1],3))
    ax.grid(zorder=0)
    ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
            color='k', fontsize=text_fs, fontweight='bold',
            ha='center', va='center') 
    
    ctr = next(plotit)
    ax = axx[ctr]
    bins = np.linspace(0,360,num=19)
    tmp = np.digitize(flashes.loc[flashes['HEMISPHERE']=='North', 'PPO_PSI_{}'.format(ppoctr)]*180/np.pi, bins)-1
    ppopower_mean = np.array([np.nanmean(flashes.loc[tmp==iii, 'GW'])
                                for iii in range(len(bins)-1)])
    ax.bar(bins[:-1]+np.diff(bins)/2, ppopower_mean, width=width_mult*np.mean(np.diff(bins)),
            edgecolor='k', color='steelblue', alpha=0.7, zorder=3)
    ax.set_xlim([0, 360])
    ax.set_xlabel('$\Psi_\mathrm{{{}}}$ (deg)'.format(ppoctr))
    ax.set_xticks(np.arange(0,361,45))
    ax.set_ylabel('Mean pulse power, North (GW)')
    ax.grid(zorder=0)
    ax.text(text_left, text_top, '({})'.format(subpan[ctr]), transform=ax.transAxes,
            color='k', fontsize=text_fs, fontweight='bold',
            ha='center', va='center')
    
    plt.savefig('{}/basic_stats_ppo_{}.png'.format(savepath, str.lower(ppoctr)),
                    bbox_inches='tight', dpi=300)
    plt.savefig('{}/basic_stats_ppo_{}.pdf'.format(savepath, str.lower(ppoctr)),
                    bbox_inches='tight', dpi=300)
    plt.show()
    plt.close()

#=====================================================
#=====================================================
# interpulse period vs peak size

fig, axx = plt.subplots(3,1, sharex=True)
fig.set_size_inches(10,14)
for iii in range(3):
    ax = axx[iii]
    lbl = ['PRE', 'POST', 'BOTH'][iii]
    xbins = np.arange(28*60,76*60+1,240)
    if not lbl=='BOTH':
        ax.scatter(ipp_power['IPP'], ipp_power[lbl], marker='.', color='k')
    else:
        ax.scatter(ipp_power['IPP'], ipp_power['PRE'], marker='.', color='k')
        ax.scatter(ipp_power['IPP'], ipp_power['POST'], marker='.', color='k')
    xvalues = xbins[:-1]+np.diff(xbins)/2
    tmp = np.digitize(ipp_power['IPP'], xbins)-1
    selec = lbl if lbl!='BOTH' else ['PRE','POST']
    yvalues = np.array([np.nanmean(ipp_power.loc[np.where(tmp==iii)[0], selec])
                        for iii in range(len(xbins)-1)])
    yerrors = np.array([np.nanstd(ipp_power.loc[np.where(tmp==iii)[0], selec])
                        for iii in range(len(xbins)-1)])
    ax.bar(xvalues, yvalues, width=0.7*np.mean(np.diff(xvalues)),
           color='steelblue', alpha=0.5, edgecolor='k')
#    x_bar = np.concatenate([[0],xvalues,[1e4]])
#    y_bar = np.concatenate([[yvalues[0]], yvalues, [yvalues[-1]]])
#    ax.step(x_bar, y_bar, where='mid', color='steelblue')
#    ax.bar(xvalues, yerrors, width=1/4*np.mean(np.diff(xvalues)), bottom=yvalues,
#           edgecolor='k', color='grey', alpha=0.5)
    ax.errorbar(xvalues, yvalues, yerr=[np.zeros_like(yerrors),yerrors], color='blue',
                linewidth=0, elinewidth=2, capsize=3)
    ticks = xbins
    ax.set_xlim([ticks[0],ticks[-1]])
    ax.set_xticks(ticks)
    ax.set_xticklabels((ticks/60).astype(int))
    if not iii:
        ax.set_title('Interpulse period vs. pulse power')
    if iii==2:
        ax.set_xlabel('Interpulse period (min)')
    ax.set_ylim([0.1,10])
    ax.set_yscale('log')
    ax.set_ylabel('{} pulse power (GW/sr)'.format(lbl))
    ax.grid(which='major', color='k', linestyle='-', linewidth=0.5, alpha=0.5) 
    ax.grid(which='minor', color='k', linestyle='--', linewidth=0.5, alpha=0.6)
plt.subplots_adjust(hspace=0.07)
plt.savefig('{}/interpulse_powers.png'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.savefig('{}/interpulse_powers.pdf'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.show()
plt.close()


#=====================================================
#=====================================================

fig, ax = plt.subplots(1,1)
fig.set_size_inches(5,5.15)
ax.set_title('Relative sizes of consecutive peaks')
ax.axis('equal')
ax.scatter(ipp_power['PRE'], ipp_power['POST'], marker='.', color='steelblue')
cond = np.isfinite(ipp_power['PRE']) & np.isfinite(ipp_power['POST'])
opt, cov = sopt.curve_fit(linear, np.log10(ipp_power[cond]['PRE']),
                          np.log10(ipp_power[cond]['POST']), p0=(0))
xs = np.logspace(-1,1,num=100)
ys = 10**(linear(np.log10(xs), *opt))
#ax.plot(xs,ys, c='steelblue')
ax.plot(xs,xs, 'k--')
ax.set_xlim([0.1,10])
ax.set_ylim([0.1,10])
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel('PRE pulse power (GW/sr)')
ax.set_ylabel('POST pulse power (GW/sr)')
ax.grid(which='major', color='k', linestyle='-', linewidth=0.5, alpha=0.5) 
ax.grid(which='minor', color='k', linestyle='--', linewidth=0.5, alpha=0.6)
plt.savefig('{}/pre_vs_post.png'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.savefig('{}/pre_vs_post.pdf'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.show()
plt.close()

#=====================================================
#=====================================================

from matplotlib.ticker import Locator
class MinorSymLogLocator(Locator):
    def __init__(self, linthresh):
        self.linthresh = linthresh
    def __call__(self):
        majorlocs = self.axis.get_majorticklocs()
        minorlocs = []
        for i in range(1, len(majorlocs)):
            majorstep = majorlocs[i] - majorlocs[i-1]
            if abs(majorlocs[i-1] + majorstep/2) < self.linthresh:
                ndivs = 10
            else:
                ndivs = 9
            minorstep = majorstep / ndivs
            locs = np.arange(majorlocs[i-1], majorlocs[i], minorstep)[1:]
            minorlocs.extend(locs)
        return self.raise_if_exceeds(np.array(minorlocs))
    def tick_values(self, vmin, vmax):
        raise NotImplementedError('Cannot get tick locations for a '
                                  '%s type.' % type(self))

fig, ax = plt.subplots(1,1)
fig.set_size_inches(10,6)
xbins = np.arange(28*60,76*60+1,360)
ax.scatter(ipp_power['IPP'], ipp_power['DIFF'], marker='.', color='k', label='pulse differences')
ticks = xbins
ax.set_xlim([ticks[0],ticks[-1]])
ax.set_xticks(ticks)
ax.set_xticklabels((ticks/60).astype(int))
ax.set_title('Interpulse period vs. pulse power difference')
ax.set_xlabel('Interpulse period (min)')
ax.set_yscale('symlog', linthreshy=0.3)
ax.set_ylim([-10,10])
ax.yaxis.set_minor_locator(MinorSymLogLocator(1e-1))
ax.set_ylabel('Pulse power difference (POST-PRE) (GW/sr)')
ax.grid(which='major', color='k', linestyle='-', linewidth=0.5, alpha=0.5) 
ax.grid(which='minor', color='k', linestyle='--', linewidth=0.5, alpha=0.6)

ax2 = ax.twinx()
xvalues = xbins[:-1]+np.diff(xbins)/2
tmp = np.digitize(ipp_power['IPP'], xbins)-1
ymedian = np.array([ipp_power.loc[np.where(tmp==iii)[0], 'DIFF'].median()
                    for iii in range(len(xbins)-1)])
ymean = np.array([ipp_power.loc[np.where(tmp==iii)[0], 'DIFF'].mean()
                    for iii in range(len(xbins)-1)])
    
ycount_pos = np.array([ipp_power.loc[np.where((tmp==iii)&(ipp_power['DIFF']>0))[0], 'DIFF'].count()
                    for iii in range(len(xbins)-1)])
ycount_neg = np.array([ipp_power.loc[np.where((tmp==iii)&(ipp_power['DIFF']<0))[0], 'DIFF'].count()
                    for iii in range(len(xbins)-1)])
ax.bar(xvalues, ymedian, width=3/6*np.mean(np.diff(xvalues)), edgecolor='k',
       color='indianred', alpha=0.7, label='median difference')
ax2.bar(xvalues, ycount_pos, width=5/6*np.mean(np.diff(xvalues)), edgecolor='k',
       color='steelblue', alpha=0.3)
ax2.bar(xvalues, -ycount_neg, width=5/6*np.mean(np.diff(xvalues)), edgecolor='k',
       color='steelblue', alpha=0.3)
ax.bar(xvalues, 0, width=0, edgecolor='k', color='steelblue', alpha=0.3, label='pos/neg count')
ax.legend(fancybox=True, framealpha=0.7)
MAX = np.max(np.append(ycount_neg, ycount_pos))+1
ax2.set_ylim([-MAX,MAX])
ax2.set_ylabel('Number of samples', rotation=270, labelpad=10)
ticks = np.arange(-15,16,5)
ax2.set_yticks(ticks)
ax2.set_yticklabels(np.abs(ticks).astype(int))
ax2.tick_params(axis='y', colors='steelblue')
ax2.yaxis.label.set_color('steelblue')
plt.savefig('{}/pulse_height_change.png'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.savefig('{}/pulse_height_change.pdf'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.show()
plt.close()


#----------------------------------
fig, ax = plt.subplots(1,1)
fig.set_size_inches(10,6)
xbins = np.arange(28*60,76*60+1,360)
ax.scatter(ipp_power['IPP'], ipp_power['RATIO_lin1'], marker='.', color='k', label='pulse ratios')
ticks = xbins
ax.set_xlim([ticks[0],ticks[-1]])
ax.set_xticks(ticks)
ax.set_xticklabels((ticks/60).astype(int))
ax.set_title('Interpulse period vs. pulse power ratio')
ax.set_xlabel('Interpulse period (min)')

ratiomax = 5
ax.set_ylim([2-ratiomax,ratiomax])
ticklist = np.arange(2-ratiomax,ratiomax+1)
tmp = np.arange(2, ratiomax+1)
a = ['$1/{}$'.format(iii) for iii in np.flipud(tmp)]
b = ['${}/1$'.format(iii) for iii in tmp]
ticknames = np.concatenate([a,['$1/1$'],b])
ax.set_yticks(ticklist)
ax.set_yticklabels(ticknames)
ax.set_ylabel('Pulse power ratio (POST/PRE)')
ax.grid(which='major', color='k', linestyle='-', linewidth=0.5, alpha=0.5) 
ax.grid(which='minor', color='k', linestyle='--', linewidth=0.5, alpha=0.6)

ax2 = ax.twinx()
xvalues = xbins[:-1]+np.diff(xbins)/2
tmp = np.digitize(ipp_power['IPP'], xbins)-1
ymedian = np.array([ipp_power.loc[np.where(tmp==iii)[0], 'RATIO_lin1'].median()
                    for iii in range(len(xbins)-1)])
ymedian = ymedian-1
ycount_pos = np.array([ipp_power.loc[np.where((tmp==iii)&(ipp_power['RATIO_lin1']>1))[0], 'RATIO_lin1'].count()
                    for iii in range(len(xbins)-1)])
ycount_neg = np.array([ipp_power.loc[np.where((tmp==iii)&(ipp_power['RATIO_lin1']<1))[0], 'RATIO_lin1'].count()
                    for iii in range(len(xbins)-1)])
ax.bar(xvalues, ymedian, width=3/6*np.mean(np.diff(xvalues)), bottom=1, edgecolor='k',
       color='indianred', alpha=0.7, label='median ratio')
ax2.bar(xvalues, ycount_pos, width=5/6*np.mean(np.diff(xvalues)), edgecolor='k',
       color='steelblue', alpha=0.3)
ax2.bar(xvalues, -ycount_neg, width=5/6*np.mean(np.diff(xvalues)), edgecolor='k',
       color='steelblue', alpha=0.3)
ax.bar(xvalues, 0, width=0, edgecolor='k', color='steelblue', alpha=0.3, label='inc/dec count')
ax.legend(fancybox=True, framealpha=0.7)
MAX = np.max(np.append(ycount_neg, ycount_pos))+1
ax2.set_ylim([-MAX,MAX])
ax2.set_ylabel('Number of samples', rotation=270, labelpad=10)
ticks = np.arange(-15,16,5)
ax2.set_yticks(ticks)
ax2.set_yticklabels(np.abs(ticks).astype(int))
ax2.tick_params(axis='y', colors='steelblue')
ax2.yaxis.label.set_color('steelblue')
plt.savefig('{}/pulse_height_change_ratio.png'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.savefig('{}/pulse_height_change_ratio.pdf'.format(savepath),
                bbox_inches='tight', dpi=300)
plt.show()
plt.close()

