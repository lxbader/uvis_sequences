import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from brokenaxes import brokenaxes
import cubehelix
from datetime import datetime, timedelta
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import ppo_mag_phases
import remove_solar_reflection
import time_conversions as tc
import uvisdb

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)

mydpi = 400

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF

pmp = ppo_mag_phases.PPOMagPhases(datapath)
rsr = remove_solar_reflection.RemoveSolarReflection()

tmin = datetime(2008,1,1)
tmax = datetime(2018,1,1)

present = False

savepath = '{}/UVIS_seq_lat'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

# =============================================================================
# get UVIS keogram
# =============================================================================
SECTIONS_TO_USE = 'UV_POWER_SECTIONS_NOSUN_8_22'
SECTIONS_TO_USE = 'UV_POWER_SECTIONS_NOSUN_0_30'
selec = imglist[(imglist['ET_STOP']>tc.datetime2et(tmin)) & 
                (imglist['ET_START']<tc.datetime2et(tmax)) &
                (imglist['HEMISPHERE'] == 'North')]
selec = selec.drop(2674)
keogram_uvis = np.concatenate([selec[SECTIONS_TO_USE].tolist()])/1e9
keogram_uvis[keogram_uvis<=0] = 0.1
times_uvis = np.insert(selec['ET_STOP'].tolist(), np.arange(len(selec)), selec['ET_START'].tolist())
keo_uvis = np.insert(keogram_uvis, np.arange(1,len(selec)), np.full((len(selec)-1, 36), np.nan), axis=0)
thishem = selec['HEMISPHERE'].iloc[0]
ltbins_uvis = np.linspace(0,24,num=37)
ltcenters_uvis = ltbins_uvis[:-1]+np.diff(ltbins_uvis)/2

# =============================================================================
# get PPO phases
# =============================================================================
etrange = np.arange(tc.datetime2et(tmin), tc.datetime2et(tmax), 300)
ppo_n = np.degrees((pmp.getMagPhase(etrange, 'N')+np.pi) % (2*np.pi))
ppo_s = np.degrees((pmp.getMagPhase(etrange, 'S')+np.pi) % (2*np.pi))

# PPO upward current LTs
for ctr in range(2):
    ppo = [ppo_n, ppo_s][ctr]
    # transform phases into upward FAC maximum angles
    max_fac = (ppo-90-180) % 360 if thishem=='North' else (ppo+90-180) % 360
    # put nan's at 360-0 flips
    tmp = np.where(np.diff(max_fac)<-180)[0]+1
    ins_times = etrange[tmp-1]+(etrange[1]-etrange[0])/2
    xdata = np.insert(etrange,tmp,ins_times)
    ydata = np.insert(max_fac,tmp,[np.nan])
    # put 360 before
    tmp = np.where(np.isnan(ydata))[0]
    xdata = np.insert(xdata, tmp, xdata[tmp]-1)
    ydata = np.insert(ydata, tmp, [360])
    # put 0 after
    tmp = np.where(np.isnan(ydata))[0]
    xdata = np.insert(xdata, tmp+1, xdata[tmp]+1)
    ydata = np.insert(ydata, tmp+1, [0])
    # change into LTs
    ydata = ydata/360*24
    if ctr==0:
        dts_ppo_n = tc.et2datetime(xdata)
        ppo_n_up = ydata
#        ppo_n_thin = (ppo_n_up+ (6 if thishem=='North' else -6)) % 24
#        ppo_n_thin[(ppo_n_thin>=6) & (ppo_n_thin<=18)] = np.nan
    else:
        dts_ppo_s = tc.et2datetime(xdata)
        ppo_s_up = ydata
#        ppo_s_thin = (ppo_s_up+ (6 if thishem=='North' else -6)) % 24
#        ppo_s_thin[(ppo_s_thin>=6) & (ppo_s_thin<=18)] = np.nan
        
# PPO thin current sheet LTs
n_thin = (ppo_n-180)%360 #referencing Phi_N=0 from 0 LT
s_thin = ppo_s%360 #referencing Phi_S=180 from 0 LT

# sort smaller, bigger angle
sortang = np.sort([n_thin,s_thin], axis=0)
diff = sortang[1]-sortang[0]
# where difference larger than 180 deg, shift larger angle by -360 degrees and resort
tmp = np.where(diff>180)[0]
sortang[1,tmp] = sortang[1,tmp]-360
sortang = np.sort(sortang, axis=0)
diff = sortang[1]-sortang[0]

# calculate mean where N and S thinning are not further than 90deg separated
mean_thin = np.mean(sortang, axis=0)%360
maxdiff = 90
mean_thin[diff>maxdiff] = np.nan

# convert to LT and plot only LTs in the midnight to dawn sector
mean_thin = mean_thin/360*24
#mean_thin[(mean_thin>6) & (mean_thin<18)] = np.nan

val = np.where(np.isfinite(mean_thin))[0]
mean_thin = mean_thin[val]
et_thin = etrange[val]

# put nan's at 360-0 flips
tmp = np.where(np.diff(mean_thin)<-12)[0]+1
ins_times = et_thin[tmp-1]+(etrange[1]-etrange[0])/2
xdata = np.insert(et_thin,tmp,ins_times)
ydata = np.insert(mean_thin,tmp,[np.nan])
# put 24 before
tmp = np.where(np.isnan(ydata))[0]
xdata = np.insert(xdata, tmp, xdata[tmp]-1)
ydata = np.insert(ydata, tmp, [24])
# put 0 after
tmp = np.where(np.isnan(ydata))[0]
xdata = np.insert(xdata, tmp+1, xdata[tmp]+1)
ydata = np.insert(ydata, tmp+1, [0])
# put nan's at all other big skips
tmp = np.where(np.diff(ydata)>2)[0]+1
ins_times = xdata[tmp-1]+(et_thin[1]-et_thin[0])/2
xdata = np.insert(xdata,tmp,ins_times)
ydata = np.insert(ydata,tmp,[np.nan])

mean_thin = np.copy(ydata)
dt_thin = tc.et2datetime(xdata)

#%%

# =============================================================================
# plot
# =============================================================================
day_selec = np.array([
        [2008,2,6,12,12,    0], # OCB # too many gaps?
        [2008,4,18,12,8,    0], # OCB
        [2008,5,8,12,8,     0], # OCB
        [2008,7,13,7,8,     0], # OCB
        [2008,7,19,8,12,    0],
        [2008,11,30,4,8,    0], # OCB
        
#        [2013,8,23,14,12,   0], #T=30min
#        [2013,8,25,16,9,    0], # southern hemisphere
#        [2013,8,27,16,8,    0], # southern hemisphere
#        [2013,11,26,14,8,   0], #T=25min
        
        [2014,3,20,2,6,     0],
        [2014,3,28,14,12,   0],
        
        [2014,5,10,10,12,   0],
#        [2014,5,20,22,8,    0], #T=23min
#        [2014,5,21,22,8,    0], #<3hrs
        [2014,5,25,15,11,   0],
        [2014,5,27,12,12,   0],
        [2014,5,29,12,12,   0],
        
        [2014,5,30,20,6,    0],
        [2014,5,31,18,8,    0],
        [2014,6,1,18,8,     0],
        [2014,6,2,21,6,     0],
        [2014,6,3,21,6,     0],
        [2014,6,5,18,12,    0],
        [2014,6,7,20,9,     0],
        [2014,6,9,20,12,    0],
        [2014,6,11,0,12,    0],

        [2014,9,5,16,8,     0],
        [2014,9,13,10,8,    0],
        
        [2014,10,17,0,12,   0],
        [2014,11,5,9,12,    0], #alternating flashes, double frequency 
        [2014,11,7,6,8,     0],
        
        [2014,11,23,15,6,   0],
        [2014,11,28,3,8,    0],
        [2014,12,1,6,7,    0],
        
        [2016,6,25,6,8,     0],
#        [2016,8,16,18,8,    0], #T=17min, N<20
#        [2016,8,17,21,12,   0], #T=21min
        [2016,9,7,0,12,     0], # OCB
        [2016,9,29,18,8,    0],
        [2016,9,30,12,12,   0],
        [2016,10,1,18,12,   0], # OCB
        [2016,10,29,6,12,   0],
        
        [2017,1,14,20,8,    0],
#        [2017,3,13,8,8,     0], #T=18min, N<20
        [2017,3,20,16,16,   0],
#        [2017,4,2,19,8,     0], #incomplete imagery
        [2017,4,18,8,8,     0],
#        [2017,7,25,0,12,    0], # incomplete / alternating scans
#        [2017,9,14,6,16,    0], #T=47min

        [2014,5,27,4,63,   1],
        [2014,6,8,1,84,     1],
        [1001,1,1,1,1,     1], # all quiet sequences
        [1002,1,1,1,1,     1], # all quiet sequences except 2017
        ])
    
#for dctr in [np.shape(day_selec)[0]-3, np.shape(day_selec)[0]-4]:
for dctr in [np.shape(day_selec)[0]-4]:
#for dctr in range(np.shape(day_selec)[0]-6, np.shape(day_selec)[0]):
#for dctr in range(np.shape(day_selec)[0]):
    try:
    
        if day_selec[dctr,0] > 2000:
            # get rough time limits
            mid = datetime(int(day_selec[dctr,0]), int(day_selec[dctr,1]),
                           int(day_selec[dctr,2]), int(day_selec[dctr,3]))
            lower = mid-timedelta(hours=float(day_selec[dctr,4]))
            upper = mid+timedelta(hours=float(day_selec[dctr,4]))
            
            lower_et = tc.datetime2et(lower)
            upper_et = tc.datetime2et(upper)
            # exact timings from data
            val = np.where((times_uvis>lower_et) & 
                           (times_uvis<upper_et))[0]
            lower_et = times_uvis[val[0]]-1
            upper_et = times_uvis[val[-1]]+1
            lower = tc.et2datetime(lower_et)
            upper = tc.et2datetime(upper_et)
        
        # get example times
        if day_selec[dctr,5]>0:
            quiet = np.array([
                    [2014,5,10,10,12],
                    [2014,5,27,12,12],
                    [2014,6,7,20,9],
                    [2014,11,7,6,8],
                    [2017,3,20,16,16],
                    ])
            if day_selec[dctr,0] < 2000:
                if day_selec[dctr,0] == 1001:
                    seq = quiet[:-1]
                elif day_selec[dctr,0] == 1002:
                    seq = quiet
                times_tmp = np.array([])
                for iii in range(np.shape(seq)[0]):
                    mid_tmp = datetime(int(seq[iii,0]), int(seq[iii,1]),
                                       int(seq[iii,2]), int(seq[iii,3]))
                    lower_tmp = tc.datetime2et(mid_tmp-timedelta(hours=float(seq[iii,4])))
                    upper_tmp = tc.datetime2et(mid_tmp+timedelta(hours=float(seq[iii,4])))
                    val = np.where((times_uvis>lower_tmp) & 
                                   (times_uvis<upper_tmp))[0]
                    times_tmp = np.append(times_tmp, times_uvis[val])
                lower_et = times_tmp[0]
                upper_et = times_tmp[-1]
                lower, upper = tc.et2datetime([lower_et, upper_et])
                mid = lower+(upper-lower)/2
            else:   
                val = np.where((times_uvis>lower_et) & 
                               (times_uvis<upper_et))[0]
                times_tmp = times_uvis[val]
                
            tmp = np.where(np.diff(times_tmp)>3.6e3)[0]
            start = np.concatenate([[times_tmp[0]],times_tmp[tmp+1]])-500
            stop = np.concatenate([times_tmp[tmp], [times_tmp[-1]]])+500
            xlims = ()
            for iii in range(len(start)):
                xlims = (xlims) + ((tc.et2datetime(start[iii]), tc.et2datetime(stop[iii])),)
                
            fractimes = np.mean([start, stop], axis=0)
        else:
            frac = np.array([1/6, 3/6, 5/6])
            # ET times at which images are requested
            fractimes = lower_et + frac*(upper_et-lower_et)
            
        if not np.any(day_selec[dctr,:] - [2017,3,20,16,16,0]):
            tmp = np.array([datetime(2017,3,20,iii,jjj)
                            for iii,jjj in zip([9,15,20],[50,0,20])])
            fractimes = tc.datetime2et(tmp)
    
    # =============================================================================
    # Calculate latitude stuff
    # =============================================================================
        inc_img = imglist[(imglist['ET_STOP']>lower_et) & 
                          (imglist['ET_START']<upper_et) &
                          (imglist['HEMISPHERE'] == 'North')]
        inc_img['ET'] = inc_img.loc[:,['ET_START','ET_STOP']].mean(axis=1)
    
        diff_lat_lonbins = np.linspace(0, 360, num=36+1)
        peaklat = np.full((len(inc_img), len(diff_lat_lonbins)-1), np.nan)
        peakint = np.full((len(inc_img), len(diff_lat_lonbins)-1), np.nan)
        
        for iii in range(len(inc_img.index)):
            if not iii % 20:
                print(iii)
            thisind = inc_img.index[iii]
            thisfile = '{}{}'.format(uvispath, inc_img.loc[thisind, 'FILEPATH'])
            rsr.calculate(thisfile, minangle=10)
            
            thisimg = rsr.redImage_aur
            imglon = np.linspace(0, 360, num=thisimg.shape[0]+1)
            imglat = np.linspace(0, 30, num=thisimg.shape[1]+1)
            imglon_cent = imglon[:-1] + np.diff(imglon)/2
            imglat_cent = imglat[:-1] + np.diff(imglat)/2
            
            for jjj in range(len(diff_lat_lonbins)-1):
                val = np.where((imglon_cent>diff_lat_lonbins[jjj]) &
                               (imglon_cent<diff_lat_lonbins[jjj+1]))[0]
                thissect = thisimg[val,:]
                sectlat = np.repeat(np.expand_dims(imglat_cent, axis=1),
                                    thissect.shape[0], axis=1).T
                x = np.ravel(sectlat)
                y = np.ravel(thissect)
                
                if np.sum(np.isnan(y)) > len(y)/3:
                    continue
                
                new_x = np.linspace(0, 30, num=240)
                new_y = np.full_like(new_x, np.nan)
                # filter
                binsize_deg = 3
                for kkk in range(len(new_x)):
                    val = np.where((x>new_x[kkk]-binsize_deg) &
                                   (x<new_x[kkk]+binsize_deg))[0]
                    new_y[kkk] = np.nanmean(y[val])
                
                peaklat[iii,jjj] = new_x[np.nanargmax(new_y)]
                peakint[iii,jjj] = np.nanmax(new_y)
                    
        #        fig, ax = plt.subplots()
        #        ax.scatter(x, y, s=1, c='k')
        #        ax.plot(new_x, new_y, 'r-')
        #        plt.show()
        #        plt.close()
        
        # get average auroral oval from 2019 modulations paper
        avgfile = '{}/uvis_sequences/LT_table_comb_long.txt'.format(gitpath)
        stat_cent_N = np.genfromtxt(avgfile, skip_header=1, usecols=(0,3), delimiter=' & ')
        bin_cent = np.zeros((len(diff_lat_lonbins)-1))
        for iii in range(len(diff_lat_lonbins)-1):
            val = np.where((stat_cent_N[:,0]>diff_lat_lonbins[iii]) &
                           (stat_cent_N[:,0]<diff_lat_lonbins[iii+1]))[0]
            bin_cent[iii] = np.mean(stat_cent_N[val,1])
        
        ppoavgfile = '{}/uvis_sequences/PPO_table_comb.txt'.format(gitpath)
        stat_offs_N = np.genfromtxt(ppoavgfile, skip_header=1, usecols=(0,1), delimiter=' & ')
    
        keo_avg = np.repeat(np.expand_dims(bin_cent, axis=0), peaklat.shape[0], axis=0)
        ppo_phase_N = inc_img['PPO_PHASE_N'].as_matrix()
        for iii in range(len(ppo_phase_N)):
            this_offs = np.copy(stat_offs_N)
            this_offs[:,0] = 180+ppo_phase_N[iii]/np.pi*180-this_offs[:,0]
            this_offs[:,0] = this_offs[:,0] % 360
            offs_binned = np.zeros((len(diff_lat_lonbins)-1))
            for jjj in range(len(diff_lat_lonbins)-1):
                val = np.where((stat_cent_N[:,0]>diff_lat_lonbins[jjj]) &
                               (stat_cent_N[:,0]<diff_lat_lonbins[jjj+1]))[0]
                offs_binned[jjj] = np.mean(this_offs[val,1])
            keo_avg[iii,:] += offs_binned
        
        diff_lat = peaklat-keo_avg
        diff_lat[peakint<2] = np.nan
        
        diff_lat_times = np.repeat(inc_img['ET_START'], 2).tolist()
        diff_lat_times[1::2] = inc_img['ET_STOP']
        diff_lat = np.repeat(diff_lat, 2, axis=0)
        diff_lat = diff_lat[:-1,:]
        for iii in range(1, diff_lat.shape[0], 2):
            diff_lat[iii, :] = np.nan
    
    ##%%
    # =============================================================================
    # now actually plot    
    # =============================================================================
            
        # iterator for numbering panels
        panels = 'abcdefghijk'
        pan_iter = iter(range(len(panels)))
            
    #    make figure
        fig = plt.figure()
        fig.set_size_inches(11 if len(fractimes)<4 else 12,
                            12 if len(fractimes)<3 else 11)
        wr = np.append(np.ones((len(fractimes))), [0.05 if len(fractimes)<4 else 0.1])
        gs = gridspec.GridSpec(3, len(fractimes)+1, wspace=0.05, hspace=0.18, width_ratios=tuple(wr),
                               height_ratios=(1 if len(fractimes)<4 else (0.8 if len(fractimes)<5 else 0.6),1,1))
        if day_selec[dctr,5]>0:        
            ax1 = brokenaxes(xlims=xlims, wspace=0.15, subplot_spec=gs[1,:len(fractimes)], d=0.008, tilt=60, despine=False)
            ax2 = brokenaxes(xlims=xlims, wspace=0.15, subplot_spec=gs[2,:len(fractimes)], d=0.008, tilt=60, despine=False)
        else:
            ax1 = plt.subplot(gs[1,:len(fractimes)])
            ax2 = plt.subplot(gs[2,:len(fractimes)], sharex=ax1)
        
        inc_img = imglist[(imglist['ET_STOP']>lower_et) & 
                          (imglist['ET_START']<upper_et) &
                          (imglist['HEMISPHERE'] == 'North')]
        inc_img['ET'] = inc_img.loc[:,['ET_START','ET_STOP']].mean(axis=1)
        for ftctr in range(len(fractimes)):
            # returns actual index of the dataframe!
            tmp = np.argmin(np.abs(inc_img['ET']-fractimes[ftctr]))
            thisfile = '{}{}'.format(uvispath, inc_img.loc[tmp,'FILEPATH'])
            name = inc_img.loc[tmp,'FILEPATH'].split('/')[-1].split('.')[0]
            
            ax = plt.subplot(gs[0,ftctr], projection='polar')
            # get image with reflected sunlight removed
            rsr.calculate(thisfile)
            
            KR_MIN=0.5
            KR_MAX=30
            data = rsr.redImage_aur
            data[data<0.1] = 0.1
            lonbins = np.linspace(0, 2*np.pi, num=np.shape(data)[0]+1)
            colatbins = np.linspace(0, 30, num=np.shape(data)[1]+1)
            quad = ax.pcolormesh(lonbins, colatbins, data.T, cmap=cmap_UV)
            quad.set_clim(0, KR_MAX)
            quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
            ax.set_facecolor('gray')
            
            timestr = datetime.strftime(tc.et2datetime(inc_img.loc[tmp,'ET_START']),
                                        '%Y-%j %H:%M:%S')
            ax.set_title('{}\n{}, {:.1f} min'.format(timestr, 'North', inc_img.loc[tmp,'EXP']/60), fontsize=11, y=1)
            ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
            ticklabels = ['00','06','12','18']
            for iii in range(len(ticklabels)):
                txt = ax.text(ticks[iii], 26, ticklabels[iii], color='w', fontsize=11, fontweight='bold',
                              ha='center',va='center')
                txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
            ax.set_xticks(ticks)
            ax.set_xticklabels([])
            ax.set_yticks([10,20,30])
            ax.set_yticklabels([])
            ax.grid('on', color='0.8', linewidth=1)
            ax.set_theta_zero_location("N")
            ax.set_rmax(30)
            ax.set_rmin(0)
            
            if '22' in SECTIONS_TO_USE:
                fillmin = [0,22]
                fillmax = [8,30]
                for iii in range(len(fillmin)):
                    ax.fill_between(lonbins,
                                    np.full_like(lonbins, fillmin[iii]),
                                    np.full_like(lonbins, fillmax[iii]),
                                    facecolor='0.9',
                                    edgecolor='k',
                                    alpha=0.4,
                                    hatch='xx')
                
            if not np.any(day_selec[dctr,:] - [2017,3,20,16,16,0]):
                ax.plot([pmp.getMagPhase(inc_img.loc[tmp,'ET_START'], 'N')-np.pi/2, 
                         pmp.getMagPhase(inc_img.loc[tmp,'ET_START'], 'N')-np.pi/2],
                         [0, 30],
                         ls='-' if thishem=='North' else '--',
                         c='gold',
                         lw=1.5)
                ax.plot([pmp.getMagPhase(inc_img.loc[tmp,'ET_START'], 'S')-np.pi/2, 
                         pmp.getMagPhase(inc_img.loc[tmp,'ET_START'], 'S')-np.pi/2],
                         [0, 30],
                         ls='--' if thishem=='North' else '-',
                         c='gold',
                         lw=1.5)
            
            if not ftctr:
                # plot colorbar
                cbar = plt.colorbar(quad, cax=plt.subplot(gs[0,len(fractimes)]))
                cbar.set_label('Intensity (kR)', labelpad=10, fontsize=11, rotation=270)
                cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
                                                   np.arange(1,10,1)),
                                         np.arange(10,101,10)))
                cbar.ax.tick_params(labelsize=10)
            if not present:
                txt = ax.text(0, 1, '({})'.format(panels[next(pan_iter)]),
                              transform=ax.transAxes, ha='center', va='center',
                              color='k', fontweight='bold', fontsize=15)
                txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
            
            tmpax = ax1 if day_selec[dctr,5]==0 else ax1.axs[ftctr]
            tmpax.annotate('', xy=((tc.et2datetime(inc_img.loc[tmp,'ET'])),24),
                           xycoords='data', xytext=((tc.et2datetime(inc_img.loc[tmp,'ET'])),27),
                           arrowprops=dict(facecolor='black', shrink=0.01))
    
        # plot UV
        val = np.where((times_uvis>lower_et) & 
                       (times_uvis<upper_et))[0]
        quad = ax1.pcolormesh(tc.et2datetime(times_uvis[val]), ltbins_uvis,
                            keo_uvis[val[:-1],:].T, cmap=cmap_UV,
                            norm=mcolors.LogNorm(vmin=1e-1, vmax=1e1))
        cbar = plt.colorbar(quad[0] if day_selec[dctr,5]>0 else quad, cax=plt.subplot(gs[1,len(fractimes)]))
        cbar.set_label('UV power (GW)', rotation=270,
                        labelpad=15)
        
        # plot PPO FACs
        val = np.where((dts_ppo_n>lower) & 
                       (dts_ppo_n<upper))[0]
        ax1.plot(dts_ppo_n[val], ppo_n_up[val], ls='-' if thishem=='North' else '--',
                 c='gold',
                 lw=1.5,
                 label='PPO N max upward FAC')
        val = np.where((dts_ppo_s>lower) & 
                       (dts_ppo_s<upper))[0]
        ax1.plot(dts_ppo_s[val], ppo_s_up[val], ls='-' if thishem=='South' else '--',
                 c='gold',
                 lw=1.5,
                 label='PPO S max upward FAC')
        if day_selec[dctr,5] and day_selec[dctr,0]==2014:
            val = np.where((dt_thin>lower) & 
                       (dt_thin<upper))[0]
            ax1.plot(dt_thin[val], mean_thin[val], color='orange', ls=':',
                     lw=2,
                     label='PPO thin current sheet', zorder=10)
        
        # plot latitude
        quad = ax2.pcolormesh(tc.et2datetime(diff_lat_times), diff_lat_lonbins/180*12,
                            diff_lat.T, cmap='coolwarm',
                            vmin=-5, vmax=5)
        cbar = plt.colorbar(quad[0] if day_selec[dctr,5]>0 else quad, cax=plt.subplot(gs[2,len(fractimes)]))
        cbar.set_label('Colatitude offset (deg)', rotation=270,
                        labelpad=15)
            
        # set up axes
        for ax in [ax1,ax2]:
            ax.set_ylabel('LT (h)')
            if day_selec[dctr,5]>0:
                axs = ax.axs
            else:
                axs = [ax]
                ax.set_xlim([tmin, tmax])
            for ax in axs:
                # setting grid
                ax.grid(which='major', color='k', linestyle='-', linewidth=0.5, alpha=0.5) 
                ax.grid(which='minor', color='k', linestyle='--', linewidth=0.5, alpha=0.6)
                # keogram limits and labels
                ax.set_facecolor('grey')
                ax.set_ylim([0,24])
                ax.set_yticks(np.arange(0,25,6))
        
        leg = ax1.legend(bbox_to_anchor=(0.05, -0.07, 0.9, .05),
                         ncol=3, mode="expand", borderaxespad=0.,
                         shadow=True,
                         facecolor='0.4', edgecolor='k', fancybox=True,
                         prop=dict(weight='bold'))
        for text in leg.get_texts():
            text.set_color('w')
              
        # panel labeling
        for ax in [ax1, ax2]:
            if not present:
                tmpax = ax if day_selec[dctr,5]==0 else ax.big_ax
                txt = tmpax.text(0.03, 0.92,
                                 '({})'.format(panels[next(pan_iter)]),
                                  transform=tmpax.transAxes, ha='center', va='center',
                                  color='k', fontweight='bold', fontsize=15)
                txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
        if day_selec[dctr,5]>0:
    #        fig.autofmt_xdate()
            for ax in [ax1,ax2]:
                for sax in ax.axs:
                    sax.xaxis.set_major_locator(mdates.HourLocator(byhour=[0,12]))
                    sax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%j\n%H:%M'))
                    sax.xaxis.set_minor_locator(mdates.HourLocator(byhour=np.arange(0,24,3)))
    #                sax.tick_params(axis='x', which='both', labelbottom=False if ax==ax1 else True)
                for iii in range(len(fractimes)):
                    ax.axs[iii].tick_params(axis='both', which='both',
                          left=True if iii==0 else False,
                          labelleft=True if iii==0 else False,
                          right=True if iii==len(fractimes)-1 else False,
                          labelright=False,
                          top=True,
                          bottom=True,
                          labelbottom=False if ax==ax1 else True)
        else:
            fig.autofmt_xdate()
            ax1.xaxis.set_major_locator(mdates.HourLocator(interval=3))
            ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%j\n%H:%M'))
            ax1.xaxis.set_minor_locator(mdates.HourLocator(interval=1))
            ax1.set_xlim([lower, upper])
            
        # set title
        fmt = '%Y-%m-%d (DOY %j)'
    #    ax1.set_title('{0} ({1})'.format(datetime.strftime(mid, fmt), thishem))
        print('Saving {0} ({1})'.format(datetime.strftime(mid, fmt), thishem))
        
        # save
        fname = '{}/UV_dawn_dusk_{}{}comp_{}_{}_{}'.format(savepath,
                 'long' if day_selec[dctr,5] else '',
                 'full' if '30' in SECTIONS_TO_USE else '',
                 int(day_selec[dctr,0]),
                 int(day_selec[dctr,1]),
                 int(day_selec[dctr,2]))
        plt.savefig(fname+'.png', bbox_inches='tight', dpi=mydpi)
    #    plt.savefig(fname+'.pdf', bbox_inches='tight',)
        plt.close()
    except:
        pass
