# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import cassinipy
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

fmt = '%Y-%m-%d %H:%M'
tstart = tc.datetime2et(datetime.strptime('2008-05-08 00:00', fmt))
tstop = tstart+3600*24

cd.set_timeframe([tstart, tstop])
cd.get_RPWS()

data = cd.crpws.data
x = cd.crpws.ettimes
y = cd.crpws.freq

plt.figure(figsize=[12,3])
plt.pcolormesh(tc.et2datetime(x),np.log10(y),data.T, cmap = 'inferno', vmin=5, vmax=30)
plt.ylim([1,2])
plt.colorbar()
plt.show()