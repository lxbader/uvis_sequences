# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
from datetime import datetime, timedelta
import get_image
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.ticker as mticker
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import os
import pandas as pd
import ppo_mag_phases
import remove_solar_reflection
import scipy.interpolate as sint
import scipy.signal as sgn
import time_conversions as tc
import uvisdb

myblue='royalblue'
myred='crimson'
def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_UV_short = truncate_colormap(cmap_UV, 0.1, 0.9, 100)

def MAD(x):
    a = np.nanmedian(x)
    return np.nanmedian(np.abs(x-a))

flashfile = '{}/imglists/UVIS_flashes_tmp.txt'.format(boxpath)
with open(flashfile, 'w') as f:
    f.write('UTCTIME\tETTIME\tLTLOCATION\tGW\tHEMISPHERE\n')
    
freqfile = '{}/imglists/UVIS_freq_tmp.txt'.format(boxpath)
with open(freqfile, 'w') as f:
    f.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(
            'UTCSTART',
            'UTCSTOP',
            'ETSTART',
            'ETSTOP',
            'NUM_IMAGES',
            'NUM_PEAKS',
            'INTERPEAK_T',
            'MED_SAMPL_T',
            'PULSATION_PERC_AVG',
            'PULSATION_PERC_MAX',
            ))
    
ftexfile = '{}/imglists/UVIS_ftex_tmp.txt'.format(boxpath)
with open(ftexfile, 'w') as f:
    f.write('{} & {} & {} & {} & {} & {} & {} & {}\\\\\n'.format(
            'UTCSTART',
            'UTCSTOP',
            'NUM_IMAGES',
            'MED_SAMPL_MIN',
            'NUM_PEAKS',
            'INTERPEAK_MIN',
            'PULSATION_PERC_AVG',
            'PULSATION_PERC_MAX',
            ))

rsr = remove_solar_reflection.RemoveSolarReflection()
picklefile = '{}/imglists/UVIS_current_pickle'.format(boxpath)
imglist = pd.read_pickle(picklefile)

udb = uvisdb.UVISDB(update=False)
#imglist = udb.currentDF
imglist['ET'] = np.mean(imglist[['ET_START', 'ET_STOP']], axis=1)
imglist['UV_POW_BG'] = np.nan
imglist['UV_POW_FEAT'] = np.nan

pmp = ppo_mag_phases.PPOMagPhases(datapath)

day_selec = np.array([
#        [2008,2,6,12,12,    0], # OCB # too many gaps?
        [2008,4,18,12,8,    0], # OCB
        [2008,5,8,12,8,     0], # OCB
        [2008,7,13,7,8,     0], # OCB
        [2008,7,19,8,12,    0],
#        [2008,11,30,4,8,    0], # OCB
        
#        [2013,8,23,14,12,   0], #T=30min
#        [2013,8,25,16,9,    0], # southern hemisphere
#        [2013,8,27,16,8,    0], # southern hemisphere
#        [2013,11,26,14,8,   0], #T=25min
        
        [2014,3,20,2,6,     0],
        [2014,3,28,14,12,   0],
        
        [2014,5,10,10,12,   0],
#        [2014,5,20,22,8,    0], #T=23min
#        [2014,5,21,22,8,    0], #<3hrs
        [2014,5,25,15,11,   0],
        [2014,5,27,12,12,   0],
        [2014,5,29,12,12,   0],
        
        [2014,5,30,20,6,    0],
        [2014,5,31,18,8,    0],
        [2014,6,1,18,8,     0],
        [2014,6,2,21,6,     0],
        [2014,6,3,21,6,     0],
        [2014,6,5,18,12,    0],
        [2014,6,7,20,9,     0],
        [2014,6,9,20,12,    0],
        [2014,6,11,0,12,    0],

        [2014,9,5,16,8,     0],
        [2014,9,13,10,8,    0],
        
        [2014,10,17,0,12,   0],
#        [2014,11,5,9,12,    0], #alternating flashes, double frequency 
        [2014,11,7,6,8,     0],
        
        [2014,11,23,15,6,   0],
        [2014,11,28,3,8,    0],
        [2014,12,1,6,7,    0],
        
        [2016,6,25,6,8,     0],
#        [2016,8,16,18,8,    0], #T=17min, N<20
#        [2016,8,17,21,12,   0], #T=21min
        [2016,9,7,0,12,     0], # OCB
        [2016,9,29,18,8,    0],
        [2016,9,30,12,12,   0],
        [2016,10,1,18,12,   0], # OCB
        [2016,10,29,6,12,   0],
        
        [2017,1,14,20,8,    0],
#        [2017,3,13,8,8,     0], #T=18min, N<20
        [2017,3,20,16,16,   0],
        [2017,4,2,19,8,     0], #incomplete
        [2017,4,18,8,8,     0],
#        [2017,7,25,0,12,    0], # incomplete / alternating scans
#        [2017,9,14,6,16,    0], #T=47min
        ])

savedir = '{}/UV_seq_new'.format(plotpath)
if not os.path.exists(savedir):
    os.makedirs(savedir)
pdf = PdfPages('{}/all_seq.pdf'.format(savedir))
    
for selecctr in range(np.shape(day_selec)[0]):
#    if not np.all(day_selec[selecctr,0:3] == [2014,5,25]):
#        continue
#    if not (np.all(day_selec[selecctr,0:3] == [2013,8,25]) |
#            np.all(day_selec[selecctr,0:3] == [2014,3,28]) |
#            np.all(day_selec[selecctr,0:3] == [2014,5,25])):
#        continue
    centertime = datetime(day_selec[selecctr,0], day_selec[selecctr,1],
                          day_selec[selecctr,2], day_selec[selecctr,3])
    dtstart = centertime - timedelta(hours=int(day_selec[selecctr,4]))
    dtstop = centertime + timedelta(hours=int(day_selec[selecctr,4]))
    etstart = tc.datetime2et(dtstart)
    etstop = tc.datetime2et(dtstop)
    fmt = '%Y-%m-%d %H:%M:%S'
    print('New plot: {}'.format(datetime.strftime(dtstart, fmt)))
    
    print('Selecting time window...')
    selec_UVIS = imglist[(imglist['ET'] >= etstart) & 
                    (imglist['ET'] <= etstop)]
#    if np.all(day_selec[selecctr,0:3] == [2017,3,20]):
#        selec_UVIS = selec_UVIS[selec_UVIS['ET_START']<tc.datetime2et(datetime(2017,3,20,20,0,0))]
    if np.all(day_selec[selecctr,0:3] == [2014,3,28]):
        selec_UVIS = selec_UVIS.drop(890)
    if np.all(day_selec[selecctr,0:3] == [2014,11,28]):
        selec_UVIS = selec_UVIS.drop(2674)

    # combine images
    print('Combining images...')
    join = np.array([
            ['2008_201T04_38_43', '2008_201T04_45_39'],
            ['2008_201T09_14_35', '2008_201T09_21_39'],
            ['2014_150T20_19_17', '2014_150T20_22_21'],
            ['2014_151T20_45_16', '2014_151T20_48_52'],
            ['2014_154T19_14_56', '2014_154T19_19_04'],
            ['2014_158T16_15_59', '2014_158T16_19_11'],
                     ])
    selec_UVIS['COMBINE_FILEPATH'] = 'NONE'
    for iii in range(np.shape(join)[0]):
        et1 = tc.datetime2et(datetime.strptime(join[iii,0], '%Y_%jT%H_%M_%S'))
        et2 = tc.datetime2et(datetime.strptime(join[iii,1], '%Y_%jT%H_%M_%S'))
        if not (np.all(np.array([et1,et2])>=etstart) and np.all(np.array([et1,et2])<=etstop)):
            continue
        # reorganize dataframe a bit
        ind1 = selec_UVIS[(selec_UVIS['FILEPATH'].str.endswith('{}.fits'.format(join[iii,0])))].index[0]
        ind2 = selec_UVIS[(selec_UVIS['FILEPATH'].str.endswith('{}.fits'.format(join[iii,1])))].index[0]
        selec_UVIS.at[ind1,'ET_STOP'] = selec_UVIS.loc[ind2,'ET_STOP']
        selec_UVIS.at[ind1,'ET'] = selec_UVIS.loc[ind1,['ET_START','ET_STOP']].mean()
        selec_UVIS.at[ind1,'EXP'] += selec_UVIS.loc[ind2,'EXP']
        selec_UVIS.at[ind1,'COMBINE_FILEPATH'] = selec_UVIS.loc[ind2,'FILEPATH']
        selec_UVIS.drop(ind2, inplace=True)
        # load and combine images to get true UV power
        image1,angles1,_ = get_image.getRedUVIS('{}/{}'.format(uvispath,selec_UVIS.at[ind1,'FILEPATH']),
                                          minangle=0)
        rsr.calculate('{}/{}'.format(uvispath,selec_UVIS.at[ind1,'FILEPATH']))
        image1 = rsr.redImage_aur
        image2,angles2,_ = get_image.getRedUVIS('{}/{}'.format(uvispath,selec_UVIS.at[ind1,'COMBINE_FILEPATH']),
                                          minangle=0)
        rsr.calculate('{}/{}'.format(uvispath,selec_UVIS.at[ind1,'COMBINE_FILEPATH']))
        image2 = rsr.redImage_aur
        image = np.nanmean([image1,image2], axis=0)
        angles = np.nanmean([angles1,angles2], axis=0)
        selec_UVIS.at[ind1,'UV_POWER_TOTAL_NOSUN_0_30'] = udb.integrateUV(data=image, angles=angles, sections=False)
        selec_UVIS.at[ind1,'UV_POWER_SECTIONS_NOSUN_0_30'] = udb.integrateUV(data=image, angles=angles, sections=True)
            
    # get PPO phases
    print('Importing PPO...')
    etrange = np.linspace(etstart, etstop, num=1000)
    dtrange = tc.et2datetime(etrange)
    hem = selec_UVIS['HEMISPHERE'].iloc[0]
    pol = 'RH' if hem == 'North' else 'LH'
    ppo_n = np.degrees((pmp.getMagPhase(etrange, 'N')+np.pi) % (2*np.pi))
    ppo_s = np.degrees((pmp.getMagPhase(etrange, 'S')+np.pi) % (2*np.pi))
        
    def set_grid(ax, color='k'):
        ax.grid(which='major', color=color, linestyle='-', linewidth=0.5, alpha=0.5) 
        ax.grid(which='minor', color=color, linestyle='--', linewidth=0.5, alpha=0.6)
        
    def expticks(x, pos):
        if np.isnan(x):
            return 'NaN'
        elif x==0:
            return '0'
        elif np.isinf(x):
            return 'inf'
        tmp = int(np.floor(np.log10(np.abs(x))))
        return r'$%1.2f\times 10^{%d}$' % (x/10**tmp, tmp)

    formatter = mticker.FuncFormatter(expticks)
    
    #==============
    #==============
    # set up figure
    fig = plt.figure()
    fig.set_size_inches(15,12)
    NUM_subplots = 3
    sp_iter = iter(range(NUM_subplots))
    gs = gridspec.GridSpec(NUM_subplots, 2,
                           width_ratios=(50,1), wspace=0.01)
    
    # for naming panels
    panels = 'abcdefghijk'
    pan_iter = iter(range(len(panels)))
    pan_fs = 20
    pan_left = 0.03
    
    #=================
    # UV power keogram
    thissp = next(sp_iter)
    ax_keo = plt.subplot(gs[thissp, 0])
    ax_keo.set_facecolor('0.3')
    ybins = np.linspace(0,24,num=37)
    data = np.zeros((0,36))
    xvalues = np.array([], dtype=np.float)
    for ind in selec_UVIS.index:
        xvalues = np.append(xvalues, selec_UVIS.loc[ind,'ET_START'])
        if np.all(np.isnan(selec_UVIS.loc[ind,'UV_POWER_SECTIONS_NOSUN_0_30'])):
            selec_UVIS.at[ind,'UV_POWER_SECTIONS_NOSUN_0_30'] = np.full((36), np.nan)
        data = np.append(data, [selec_UVIS.loc[ind,'UV_POWER_SECTIONS_NOSUN_0_30']], axis=0)
        xvalues = np.append(xvalues, selec_UVIS.loc[ind,'ET_STOP'])
        if ind != selec_UVIS.index[-1]:
            data = np.append(data, [np.full((36), np.nan)], axis=0)
    tmp = np.copy(data)
    tmp[tmp<=0] = 0.1
    quad = ax_keo.pcolormesh(tc.et2datetime(xvalues),ybins,tmp.T,
                             norm=mcolors.LogNorm(vmin=3e8,
                                                  vmax=1e10),
                             cmap=cmap_UV)
    
    max_n = (ppo_n-90-180) % 360 if hem=='North' else (ppo_n+90-180) % 360
    tmp = np.where(np.diff(max_n)<-180)[0]+1
    ins_times = dtrange[tmp-1]+(dtrange[1]-dtrange[0])/2
    xdata = np.insert(dtrange,tmp,ins_times)
    ydata = np.insert(max_n,tmp,[np.nan])
    tmp = np.where(np.isnan(ydata))[0]
    xdata = np.insert(xdata, tmp, xdata[tmp]-timedelta(seconds=1))
    ydata = np.insert(ydata, tmp, [360])
    tmp = np.where(np.isnan(ydata))[0]
    xdata = np.insert(xdata, tmp+1, xdata[tmp]+timedelta(seconds=1))
    ydata = np.insert(ydata, tmp+1, [0])
    ydata = ydata/360*24
    ax_keo.plot(xdata, ydata, 'w-' if pol=='RH' else 'w--',
                lw=2 if pol=='RH' else 0.8,
                label='PPO N max upward FAC')
    
    if not np.all(np.isnan(ppo_s)):
        max_s = (ppo_s-90-180) % 360 if hem=='North' else (ppo_s+90-180) % 360
        tmp = np.where(np.diff(max_s)<-180)[0]+1
        ins_times = dtrange[tmp-1]+(dtrange[1]-dtrange[0])/2
        xdata = np.insert(dtrange,tmp,ins_times)
        ydata = np.insert(max_s,tmp,[np.nan])
        tmp = np.where(np.isnan(ydata))[0]
        xdata = np.insert(xdata, tmp, xdata[tmp]-timedelta(seconds=1))
        ydata = np.insert(ydata, tmp, [360])
        tmp = np.where(np.isnan(ydata))[0]
        xdata = np.insert(xdata, tmp+1, xdata[tmp]+timedelta(seconds=1))
        ydata = np.insert(ydata, tmp+1, [0])
        ydata = ydata/360*24
        ax_keo.plot(xdata, ydata, 'w-' if pol=='LH' else 'w--',
                    lw=2 if pol=='LH' else 0.8,
                    label='PPO S max upward FAC')
            
    leg = ax_keo.legend(loc=4)
    leg.get_frame().set_facecolor('0.6')
    ax_keo.set_ylabel('LT (hrs)')
    ax_keo.set_ylim([0,24])
    ax_keo.set_yticks([6,12,18,24])
    set_grid(ax_keo)
    ax_keo.text(pan_left, 0.9, '({})'.format(panels[next(pan_iter)]),
                transform=ax_keo.transAxes, ha='center', va='center',
                color='w', fontweight='bold', fontsize=pan_fs)
            
    subax = plt.subplot(gs[thissp, 1])
    cbar = plt.colorbar(quad, cax=subax)
    cbar.set_label('UV power (W)', rotation=270, labelpad=10)
    
    short_x = (xvalues[0::2]+xvalues[1::2])/2
    short_y = ybins[:-1]+np.diff(ybins)/2
    short_data = data[::2,:]
    
    fmt = '%Y-%m-%d (DOY %j)'
    ax_keo.set_title('{0} ({1}, $T_\mathrm{{median}} = {2:.1f}\,\mathrm{{min}}$)'.format(
                        datetime.strftime(centertime, fmt),
                        hem,
                        np.median(np.diff(short_x[np.isfinite(np.sum(short_data,
                                                    axis=1))])/60)))
    
    # separate oscillations from background
    feat = np.zeros_like(short_data)
    bg = np.zeros_like(short_data)
    for iii in range(np.shape(short_data)[0]):
        for jjj in range(np.shape(short_data)[1]):
            # get range of xs
            cond1 = short_x-short_x[iii]<1500
            cond2 = short_x-short_x[iii]>-1500
            xs = np.where(cond1&cond2)[0]
            # get corresponding ranges of ys
            corot_frac = 0.7
            period = 10.66*3600/corot_frac
            LTdiff = 1
            region = np.array([])
            for kkk in xs:
                tdiff = short_x[kkk]-short_x[iii]
                LToffs = 24*(tdiff/period)
                ys = np.where((np.abs(short_y-short_y[jjj]-LToffs)<LTdiff) |
                        (np.abs(short_y-short_y[jjj]-LToffs)>(24-LTdiff)))[0]
                region = np.append(region, short_data[kkk,ys])
            bg[iii,jjj] = np.nanpercentile(region, 50)
            feat[iii,jjj] = short_data[iii,jjj]-bg[iii,jjj]
    bg[np.isnan(feat)] = np.nan
            
    feat_median = np.zeros_like(feat)
    feat_median_mean = np.zeros_like(feat)
    for iii in range(np.shape(feat)[0]):
        for jjj in range(np.shape(feat)[1]):
            LTdiff = 3
            feat_median[iii,jjj] = np.nanmedian(feat[iii,np.where((np.abs(short_y[jjj]-short_y)<=LTdiff)|
                                                            (np.abs(short_y[jjj]-short_y)>=(24-LTdiff)))[0]])
    # now smooth the median-smoothed feat array with a mean filter
    for iii in range(np.shape(feat)[0]):
        for jjj in range(np.shape(feat)[1]):
            LTdiff = 3
            a = np.where(np.abs(short_x-short_x[iii])<600)[0]
            cond1 = np.abs(short_y[jjj]-short_y)<=LTdiff
            cond2 = np.abs(short_y[jjj]-short_y)>=(24-LTdiff)
            b = np.where(cond1 | cond2)[0]
            feat_median_mean[iii,jjj] = np.nanmean(feat_median[np.meshgrid(a,b)])
#    feat_max = np.argmax(feat_smooth/bg**2, axis=1)
    feat_max = np.argmax(feat_median_mean, axis=1)
    feat_mad = MAD(feat_median_mean)
    feat_max_stat = np.nanmax(feat_median_mean, axis=1)/feat_mad
            
    power_feat = np.sum(feat, axis=1)
    power_bg = np.sum(bg, axis=1)
    imglist.at[selec_UVIS.index, 'UV_POW_BG'] = power_bg
    imglist.at[selec_UVIS.index, 'UV_POW_FEAT'] = power_feat
        
    #===============
    # total UV power
    # background
    thissp = next(sp_iter)
    ax_uvtot = plt.subplot(gs[thissp, 0], sharex=ax_keo)
    xdata = tc.et2datetime(selec_UVIS['ET'])
    diffsecs = np.array([iii.total_seconds() for iii in np.diff(xdata)])
    tmp = np.where(diffsecs>4000)[0]+1
    ins = np.array([xdata[iii] - timedelta(seconds=1800) for iii in tmp])
    ydata = np.array(selec_UVIS['UV_POWER_TOTAL_NOSUN_0_30'])*1e-9
    xdata = np.insert(xdata, tmp, ins)
    ydata = np.insert(ydata, tmp, [np.nan])
    ax_uvtot.plot(xdata, ydata, 'k-x', lw=1, label='$P_\mathrm{tot}$')

    xdata = tc.et2datetime(selec_UVIS['ET'])
    diffsecs = np.array([iii.total_seconds() for iii in np.diff(xdata)])
    tmp = np.where(diffsecs>4000)[0]+1
    ins = np.array([xdata[iii] - timedelta(seconds=1800) for iii in tmp])
    ydata = power_bg*1e-9
    xdata = np.insert(xdata, tmp, ins)
    ydata = np.insert(ydata, tmp, [np.nan])
    ax_uvtot.plot(xdata, ydata, color=myred, linestyle='-', lw=1, label='$P_\mathrm{bg}$')
    bg_x = tc.datetime2et(xdata)[np.where(np.isfinite(ydata))]
    bg_y = ydata[np.where(np.isfinite(ydata))]
    
    ax_uvtot.set_ylabel('UV power (GW)',
                        labelpad=15 if not thissp%2 else 5,
                        rotation=270 if not thissp%2 else 90)
    ax_uvtot.legend(loc=1)
    set_grid(ax_uvtot)
    if not thissp%2:
        ax_uvtot.yaxis.set_label_position('right')
        ax_uvtot.yaxis.tick_right()
    ax_uvtot.text(pan_left, 0.9, '({})'.format(panels[next(pan_iter)]),
                  transform=ax_uvtot.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=pan_fs)
        
    #=================
    # UV power over BG
    thissp = next(sp_iter)
    ax_uvsel = plt.subplot(gs[thissp, 0], sharex=ax_keo)
    
    xdata = tc.et2datetime(selec_UVIS['ET'])
    diffsecs = np.array([iii.total_seconds() for iii in np.diff(xdata)])
    instmp = np.where(diffsecs>4000)[0]+1
    ins = np.array([xdata[iii] - timedelta(seconds=1800) for iii in instmp])
    xdata = np.insert(xdata, instmp, ins)
    
    ydata = power_feat*1e-9
    ydata = np.insert(ydata, instmp, [np.nan])
    
    ax_uvsel.plot(xdata, ydata,
                  c='k', ls='-', marker='x', lw=1, label='$P_\mathrm{pulses}$')
    ax_uvsel.set_ylabel('UV pulsing power (GW)',
                        labelpad=15 if not thissp%2 else 5,
                        rotation=270 if not thissp%2 else 90)
    set_grid(ax_uvsel)
    if not thissp%2:
        ax_uvsel.yaxis.set_label_position('right')
        ax_uvsel.yaxis.tick_right()
    ax_uvsel.text(pan_left, 0.9, '({})'.format(panels[next(pan_iter)]),
                  transform=ax_uvsel.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=pan_fs)
        
    uv_x = tc.datetime2et(xdata)[np.where(np.isfinite(ydata))]
    uv_y = ydata[np.where(np.isfinite(ydata))]  

    all_x = xvalues
    all_y = np.sum(data, axis=1)
    inv = np.where(np.isnan(all_y))[0]
    
    # mean filter
    f_feat = sint.interp1d(uv_x, uv_y, fill_value='extrapolate')
    f_bg = sint.interp1d(bg_x, bg_y, fill_value='extrapolate')
    xspace = np.linspace(np.min(uv_x), np.max(uv_x), num=10000)
    feat_mean = np.zeros_like(xspace)
    bg_mean = np.zeros_like(xspace)
    for xxx in range(len(xspace)):
        winsize = 1200
        tmp = np.where(np.abs(xspace-xspace[xxx])<winsize/2)[0]
        feat_mean[xxx] = np.trapz(f_feat(xspace[tmp]), xspace[tmp])/winsize
        bg_mean[xxx] = np.trapz(f_bg(xspace[tmp]), xspace[tmp])/winsize
    for iii in inv:
        if all_x[iii+1]-all_x[iii] > 600:
            feat_mean[np.where((xspace>all_x[iii]) & (xspace<all_x[iii+1]))] = np.nan
            bg_mean[np.where((xspace>all_x[iii]) & (xspace<all_x[iii+1]))] = np.nan
    ax_uvsel.plot(tc.et2datetime(xspace), feat_mean, color=myred, linestyle='-', lw=1, label='$20\,\mathrm{{min}}$ mean')
    ax_uvsel.legend(loc=1)
    
    # find peaks
    sample = np.copy(feat_mean)
    sample[np.isnan(sample)] = 0
    peaks, props = sgn.find_peaks(sample,
                                  prominence=3,
                                  distance=int(1200/np.mean(np.diff(xspace))))
    # get max power
    tmp = xspace[peaks][:-1]+np.diff(xspace[peaks])/2
    middles = np.concatenate([[xspace[0]],
                               tmp,
                               [xspace[-1]]])
    peakamps = np.zeros(np.shape(peaks))
    for iii in range(len(peaks)):
        ppp = peaks[iii]
        # plot vertical lines
        ax_uvsel.axvline(tc.et2datetime(xspace[ppp]),
                         color='k', linestyle='--', linewidth=1, alpha=0.7)
        try:
            peakamps[iii] = np.nanmax(uv_y[np.where((uv_x>middles[iii]) & (uv_x<middles[iii+1]))])
        except:
            peakamps[iii] = uv_y[np.argmin(np.abs(xspace[ppp]-uv_x))]
    ax_uvsel.bar(tc.et2datetime(xspace[peaks]),
                 peakamps,
                 width=1/24/3,
                 color='k',
                 alpha=0.3,
                 )
    
    # add LT flash location marker to keogram
    argpeaks = np.array([np.argmin(np.abs(xspace[iii]-short_x)) for iii in peaks])
    if len(argpeaks):
        loc_valid = (feat_max_stat[argpeaks]>5).astype(np.float)
        loc_valid[loc_valid==0] = np.nan
        ax_keo.scatter(tc.et2datetime(xspace[peaks]),
                       short_y[feat_max[argpeaks]]*loc_valid,
                       marker='o', facecolor='w', edgecolor='k',
                       linewidth=1.2, s=30)
        # write flash data to file
        with open(flashfile, 'a') as f:
            for iii in range(len(peaks)):
                f.write('{}\t{:.2f}\t{:.2f}\t{:.2f}\t{}\n'.format(
                            datetime.strftime(
                                    tc.et2datetime(xspace[peaks][iii]),
                                    '%Y-%jT%H:%M'),
                            xspace[peaks][iii],
                            short_y[feat_max[argpeaks]][iii]*loc_valid[iii],
                            peakamps[iii],
                            hem))

    #========
    # GENERAL
    ax = plt.gca()
    if etstop-etstart>3600*24*2:
        ax.xaxis.set_major_locator(mdates.DayLocator(interval=1))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
        ax.xaxis.set_minor_locator(mdates.HourLocator(interval=3))
    else:
        ax.xaxis.set_major_locator(mdates.HourLocator(interval=3))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d\n%H:%M'))
        ax.xaxis.set_minor_locator(mdates.HourLocator(interval=1))
#    ax.set_xlim(dtstart, dtstop)
    ax.set_xlim([np.min(tc.et2datetime(xvalues)), np.max(tc.et2datetime(xvalues))])
    plt.setp(ax.get_xticklabels(), visible=True)
    fig.autofmt_xdate()
    gs.update(hspace=0)    
    
    
    #====================                
    with open(freqfile, 'a') as f:
        f.write('{}\t{}\t{:.2f}\t{:.2f}\t{}\t{}\t{}\t{:.1f}\t{:.1f}\n'.format(
                datetime.strftime(
                                tc.et2datetime(xspace[0]),
                                '%Y-%jT%H:%M'),
                datetime.strftime(
                                tc.et2datetime(xspace[-1]),
                                '%Y-%jT%H:%M'),
                xspace[0],
                xspace[-1],
                len(selec_UVIS),
                len(peaks),
                int(np.median(np.diff(short_x[np.isfinite(np.sum(short_data,
                                                                  axis=1))]))),
                np.nanmean(np.sum(feat, axis=1)/np.sum(bg+feat, axis=1))*100,
                np.nanmax(np.sum(feat, axis=1)/np.sum(bg+feat, axis=1))*100
                ))
    with open(ftexfile, 'a') as f:
        f.write('{} & {} & {} & {:.1f} & {} & {:.1f} & {:.1f}\\\\\n'.format(
                datetime.strftime(
                                tc.et2datetime(xspace[0]),
                                '%Y-%m-%d %H:%M'),
                datetime.strftime(
                                tc.et2datetime(xspace[-1]),
                                '%Y-%m-%d %H:%M'),
                len(selec_UVIS),
                int(np.median(np.diff(short_x[np.isfinite(np.sum(short_data,
                                                                  axis=1))])))/60,
                len(peaks),
                np.nanmean(np.sum(feat, axis=1)/np.sum(bg+feat, axis=1))*100,
                np.nanmax(np.sum(feat, axis=1)/np.sum(bg+feat, axis=1))*100
                ))
        
    #=======
    # Saving
    savedir = '{}/UV_seq_new'.format(plotpath)
    if not os.path.exists(savedir):
        os.makedirs(savedir)
    fmt = '%Y_%j_%H'
    plt.savefig('{}/{}.png'.format(savedir,datetime.strftime(centertime, fmt)),
                bbox_inches='tight', dpi=300)
    plt.savefig('{}/{}.pdf'.format(savedir,datetime.strftime(centertime, fmt)),
                bbox_inches='tight', dpi=300)
    pdf.savefig()
    plt.close()
    
    
    #======================================
    # Demonstration plot
    
    fig = plt.figure()
    fig.set_size_inches(16,13)
    
    outer = gridspec.GridSpec(2, 2, height_ratios=(3,3),
                              wspace=0.1, hspace=0.07)
    gs1 = gridspec.GridSpecFromSubplotSpec(2, 2, hspace=0, wspace=0.02,
                                           height_ratios=[2, 1],
                                           width_ratios=[30, 1],
                                           subplot_spec=outer[0,0])
    gs2 = gridspec.GridSpecFromSubplotSpec(2, 2, hspace=0, wspace=0.02,
                                           height_ratios=[2, 1],
                                           width_ratios=[30, 1],
                                           subplot_spec=outer[0,1])
    gs3 = gridspec.GridSpecFromSubplotSpec(2, 2, hspace=0, wspace=0.02,
                                           height_ratios=[2, 1],
                                           width_ratios=[30, 1],
                                           subplot_spec=outer[1,0])
    gs4 = gridspec.GridSpecFromSubplotSpec(2, 2, hspace=0, wspace=0.02,
                                           height_ratios=[2, 1],
                                           width_ratios=[30, 1],
                                           subplot_spec=outer[1,1])
    
    arrays = [short_data, bg, feat, feat_median_mean]
    names = ['log10 data', 'log10 background', 'features', 'filtered features']
    gridspecs = [gs1, gs2, gs3, gs4]
    panels = ['a','b','c','d']
    
    for ggg in range(len(gridspecs)):
        arr = arrays[ggg]
        gs = gridspecs[ggg]
        full_data = np.full((len(xvalues)-1, len(ybins)-1), np.nan)
        full_data[::2, :] = arr
        full_data[full_data<=0] = 0.1
        
        ax1 = plt.subplot(gs[0,0])
        ax1.set_facecolor('0.3')
        if ggg<2:
            quad = ax1.pcolormesh(tc.et2datetime(xvalues), ybins,
                           full_data.T, cmap=cmap_UV,
                           norm=mcolors.LogNorm(vmin=3e8,
                                                vmax=1e10))
        else:
            quad = ax1.pcolormesh(tc.et2datetime(xvalues), ybins,
                           full_data.T, cmap=cmap_UV)
        ax1.set_ylim([0,24])
        if not ggg%2:
            ax1.set_ylabel('LT (hrs)')
            ax1.set_yticks(np.arange(0,25,6))
        else:
            ax1.set_yticks([])
        
        
        cax = plt.subplot(gs[0,1])
        cbar = plt.colorbar(quad, cax=cax)
        cbar.set_label('UV power (W)', rotation=270, labelpad=5 if ggg<2 else 12)
        if ggg==3:
            cbar.set_label('relative Power (arbitrary units)', rotation=270, labelpad=14)
            cbar.set_ticks([0])
        
        ax1.text(0.015, 0.91, '({})'.format(panels[ggg]), fontweight='bold',
                 color='w', transform=ax1.transAxes, fontsize=19)
        if ggg>1:
            if len(argpeaks):
                ax1.scatter(tc.et2datetime(xspace[peaks]),
                           short_y[feat_max[argpeaks]]*loc_valid,
                           marker='o', facecolor='w', edgecolor='k',
                           linewidth=1.2, s=30)
        if ggg==1:
            max_n = (ppo_n-90-180) % 360 if hem=='North' else (ppo_n+90-180) % 360
            tmp = np.where(np.diff(max_n)<-180)[0]+1
            ins_times = dtrange[tmp-1]+(dtrange[1]-dtrange[0])/2
            xdata = np.insert(dtrange,tmp,ins_times)
            ydata = np.insert(max_n,tmp,[np.nan])
            tmp = np.where(np.isnan(ydata))[0]
            xdata = np.insert(xdata, tmp, xdata[tmp]-timedelta(seconds=1))
            ydata = np.insert(ydata, tmp, [360])
            tmp = np.where(np.isnan(ydata))[0]
            xdata = np.insert(xdata, tmp+1, xdata[tmp]+timedelta(seconds=1))
            ydata = np.insert(ydata, tmp+1, [0])
            ydata = ydata/360*24
            ax1.plot(xdata, ydata, 'w-' if pol=='RH' else 'w--',
                        lw=2 if pol=='RH' else 0.8,
                        label='PPO N max upward FAC')
            
            if not np.all(np.isnan(ppo_s)):
                max_s = (ppo_s-90-180) % 360 if hem=='North' else (ppo_s+90-180) % 360
                tmp = np.where(np.diff(max_s)<-180)[0]+1
                ins_times = dtrange[tmp-1]+(dtrange[1]-dtrange[0])/2
                xdata = np.insert(dtrange,tmp,ins_times)
                ydata = np.insert(max_s,tmp,[np.nan])
                tmp = np.where(np.isnan(ydata))[0]
                xdata = np.insert(xdata, tmp, xdata[tmp]-timedelta(seconds=1))
                ydata = np.insert(ydata, tmp, [360])
                tmp = np.where(np.isnan(ydata))[0]
                xdata = np.insert(xdata, tmp+1, xdata[tmp]+timedelta(seconds=1))
                ydata = np.insert(ydata, tmp+1, [0])
                ydata = ydata/360*24
                ax1.plot(xdata, ydata, 'w-' if pol=='LH' else 'w--',
                            lw=2 if pol=='LH' else 0.8,
                            label='PPO S max upward FAC')
            ax1.legend(loc=1)
        
        # plot graphs
        ax2 = plt.subplot(gs[1,0], sharex=ax1)
        if ggg==0:
            ax2.plot(tc.et2datetime(short_x), np.sum(arr, axis=1)*1e-9,
                 'k-x', label='$P_\mathrm{tot}$')
            ax2.plot(tc.et2datetime(short_x), np.sum(bg, axis=1)*1e-9,
                 color=myred, linestyle='-', label='$P_\mathrm{bg}$')
            ax2.legend(loc=1)
        elif ggg==1:
            ax2.plot(tc.et2datetime(short_x), np.sum(arr, axis=1)*1e-9,
                 'k-x', label='$P_\mathrm{bg}$')
            ax2.legend(loc=1)
        else:
            ax2.plot(tc.et2datetime(short_x), np.sum(feat, axis=1)*1e-9,
                 'k-x', label='$P_\mathrm{pulses}$')
            ax2.plot(tc.et2datetime(xspace), feat_mean, color=myred, linestyle='-', lw=1,
                     label='$20\,\mathrm{{min}}$ mean')
            ax2.legend(loc=1)
            # plot vertical lines
            for iii in range(len(peaks)):
                ppp = peaks[iii]
                ax2.axvline(tc.et2datetime(xspace[ppp]),
                                 color='k', linestyle='--', linewidth=1, alpha=0.7)
            ax2.bar(tc.et2datetime(xspace[peaks]),
                     peakamps,
                     width=1/24/3,
                     color='k',
                     alpha=0.3,
                     )
                
        set_grid(ax2)
        ax2.yaxis.set_label_position('right')
        ax2.yaxis.tick_right()
        ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
        ax2.set_ylabel('UV power (GW)', rotation=270, labelpad=15)
        
        ax2.xaxis.set_major_locator(mdates.HourLocator(interval=3))
        ax2.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d\n%H:%M'))
        ax2.xaxis.set_minor_locator(mdates.HourLocator(interval=1))
        ax2.set_xlim([np.min(tc.et2datetime(xvalues)), np.max(tc.et2datetime(xvalues))])
        plt.setp(ax2.get_xticklabels(), visible=True)
        fig.autofmt_xdate()
        if ggg<2:
            ax2.set_xticklabels([])
            
    #=======
    # Saving
    savedir = '{}/UV_seq_new/proc'.format(plotpath)
    if not os.path.exists(savedir):
        os.makedirs(savedir)
    fmt = '%Y_%j_%H'
    plt.savefig('{}/{}_proc.png'.format(savedir,datetime.strftime(centertime, fmt)),
                bbox_inches='tight', dpi=300)
    plt.savefig('{}/{}_proc.pdf'.format(savedir,datetime.strftime(centertime, fmt)),
                bbox_inches='tight', dpi=300)
    plt.close()
    
pdf.close()

# Lomb-Scargle
fig = plt.figure()
fig.set_size_inches(7,5)
ax = plt.subplot(111)
selec = imglist[np.isfinite(imglist['UV_POW_FEAT'])]
x_val = selec['ET']
y_val = selec['UV_POW_FEAT']

tmax, tmin = [4800,1800]
fs = 1/np.linspace(tmax, tmin, num=10000)*2*np.pi
interpulse_ts = 1/(fs/(2*np.pi))

pgram_raw = sgn.lombscargle(x_val,
                            y_val,
                            fs, precenter=True, normalize=True)
pgram_smooth = sgn.medfilt(pgram_raw, kernel_size=201)

ax.plot(interpulse_ts/60, pgram_smooth, 'k-', label='raw data')
ax.set_xlim([35, 75])
ax.set_xlabel('Interpulse period (min)')
ax.set_ylim([0,ax.get_ylim()[1]])
ax.set_ylabel('Normalized wave power')
ax.grid()
savedir = '{}/UV_seq_new/lomb_scargle'.format(plotpath)
if not os.path.exists(savedir):
    os.makedirs(savedir)
fmt = '%Y_%j_%H'
plt.savefig('{}/lomb_scargle.png'.format(savedir,datetime.strftime(centertime, fmt)),
            bbox_inches='tight', dpi=300)
plt.savefig('{}/lomb_scargle.pdf'.format(savedir,datetime.strftime(centertime, fmt)),
            bbox_inches='tight')
plt.close()
