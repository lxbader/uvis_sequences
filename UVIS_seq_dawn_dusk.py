# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from brokenaxes import brokenaxes
import cubehelix
from datetime import datetime, timedelta
import get_image
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import ppo_mag_phases
import remove_solar_reflection
#import skr_phases
import sys
import time_conversions as tc
import uvisdb

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)

mydpi = 400

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF

pmp = ppo_mag_phases.PPOMagPhases(datapath)
rsr = remove_solar_reflection.RemoveSolarReflection()
    
tmin = datetime(2007,1,1)
tmax = datetime(2018,1,1)

present = False

savepath = '{}/UV_seq_dawn_dusk{}'.format(plotpath, '_present' if present else '')
if not os.path.exists(savepath):
    os.makedirs(savepath)

# =============================================================================
# get UVIS keogram
# =============================================================================
SECTIONS_TO_USE = 'UV_POWER_SECTIONS_NOSUN_8_22'
selec = imglist[(imglist['ET_STOP']>tc.datetime2et(tmin)) & 
                (imglist['ET_START']<tc.datetime2et(tmax)) &
                (imglist['HEMISPHERE'] == 'North')]
selec = selec.drop(2674)
keogram_uvis = np.concatenate([selec[SECTIONS_TO_USE].tolist()])/1e9
keogram_uvis[keogram_uvis<=0] = 0.1
times_uvis = np.insert(selec['ET_STOP'].tolist(), np.arange(len(selec)), selec['ET_START'].tolist())
keo_uvis = np.insert(keogram_uvis, np.arange(1,len(selec)), np.full((len(selec)-1, 36), np.nan), axis=0)
thishem = selec['HEMISPHERE'].iloc[0]
ltbins_uvis = np.linspace(0,24,num=37)
ltcenters_uvis = ltbins_uvis[:-1]+np.diff(ltbins_uvis)/2

# =============================================================================
# get PPO phases
# =============================================================================
etrange = np.arange(tc.datetime2et(tmin), tc.datetime2et(tmax), 300)
ppo_n = np.degrees((pmp.getMagPhase(etrange, 'N')+np.pi) % (2*np.pi))
ppo_s = np.degrees((pmp.getMagPhase(etrange, 'S')+np.pi) % (2*np.pi))

# PPO upward current LTs
for ctr in range(2):
    ppo = [ppo_n, ppo_s][ctr]
    # transform phases into upward FAC maximum angles
    max_fac = (ppo-90-180) % 360 if thishem=='North' else (ppo+90-180) % 360
    # put nan's at 360-0 flips
    tmp = np.where(np.diff(max_fac)<-180)[0]+1
    ins_times = etrange[tmp-1]+(etrange[1]-etrange[0])/2
    xdata = np.insert(etrange,tmp,ins_times)
    ydata = np.insert(max_fac,tmp,[np.nan])
    # put 360 before
    tmp = np.where(np.isnan(ydata))[0]
    xdata = np.insert(xdata, tmp, xdata[tmp]-1)
    ydata = np.insert(ydata, tmp, [360])
    # put 0 after
    tmp = np.where(np.isnan(ydata))[0]
    xdata = np.insert(xdata, tmp+1, xdata[tmp]+1)
    ydata = np.insert(ydata, tmp+1, [0])
    # change into LTs
    ydata = ydata/360*24
    if ctr==0:
        dts_ppo_n = tc.et2datetime(xdata)
        ppo_n_up = ydata
#        ppo_n_thin = (ppo_n_up+ (6 if thishem=='North' else -6)) % 24
#        ppo_n_thin[(ppo_n_thin>=6) & (ppo_n_thin<=18)] = np.nan
    else:
        dts_ppo_s = tc.et2datetime(xdata)
        ppo_s_up = ydata
#        ppo_s_thin = (ppo_s_up+ (6 if thishem=='North' else -6)) % 24
#        ppo_s_thin[(ppo_s_thin>=6) & (ppo_s_thin<=18)] = np.nan
        
# PPO thin current sheet LTs
n_thin = (ppo_n-180)%360 #referencing Phi_N=0 from 0 LT
s_thin = ppo_s%360 #referencing Phi_S=180 from 0 LT

# sort smaller, bigger angle
sortang = np.sort([n_thin,s_thin], axis=0)
diff = sortang[1]-sortang[0]
# where difference larger than 180 deg, shift larger angle by -360 degrees and resort
tmp = np.where(diff>180)[0]
sortang[1,tmp] = sortang[1,tmp]-360
sortang = np.sort(sortang, axis=0)
diff = sortang[1]-sortang[0]

# calculate mean where N and S thinning are not further than 90deg separated
mean_thin = np.mean(sortang, axis=0)%360
maxdiff = 90
mean_thin[diff>maxdiff] = np.nan

# convert to LT and plot only LTs in the midnight to dawn sector
mean_thin = mean_thin/360*24
#mean_thin[(mean_thin>6) & (mean_thin<18)] = np.nan

val = np.where(np.isfinite(mean_thin))[0]
mean_thin = mean_thin[val]
et_thin = etrange[val]

# put nan's at 360-0 flips
tmp = np.where(np.diff(mean_thin)<-12)[0]+1
ins_times = et_thin[tmp-1]+(etrange[1]-etrange[0])/2
xdata = np.insert(et_thin,tmp,ins_times)
ydata = np.insert(mean_thin,tmp,[np.nan])
# put 24 before
tmp = np.where(np.isnan(ydata))[0]
xdata = np.insert(xdata, tmp, xdata[tmp]-1)
ydata = np.insert(ydata, tmp, [24])
# put 0 after
tmp = np.where(np.isnan(ydata))[0]
xdata = np.insert(xdata, tmp+1, xdata[tmp]+1)
ydata = np.insert(ydata, tmp+1, [0])
# put nan's at all other big skips
tmp = np.where(np.diff(ydata)>2)[0]+1
ins_times = xdata[tmp-1]+(et_thin[1]-et_thin[0])/2
xdata = np.insert(xdata,tmp,ins_times)
ydata = np.insert(ydata,tmp,[np.nan])

mean_thin = np.copy(ydata)
dt_thin = tc.et2datetime(xdata)

        
## =============================================================================
## get SKR phases
## =============================================================================
#skrp = skr_phases.SKRPhases(datapath)
#skr_n = np.degrees(skrp.getSKRPhase(etrange, 'N'))
#
#plt.plot(etrange, (ppo_n-skr_n)%360)
#plt.axvline(tc.datetime2et(datetime(2013,6,1)))
#plt.axvline(tc.datetime2et(datetime(2014,6,1)))
#
#sys.exit()
#        

#%%
# =============================================================================
# plot
# =============================================================================
day_selec = np.array([
        [2008,2,6,12,12,    0], # OCB # too many gaps?
        [2008,4,18,12,8,    0], # OCB
        [2008,5,8,12,8,     0], # OCB
        [2008,7,13,7,8,     0], # OCB
        [2008,7,19,8,12,    0],
        [2008,11,30,4,8,    0], # OCB
        
#        [2013,8,23,14,12,   0], #T=30min
#        [2013,8,25,16,9,    0], # southern hemisphere
#        [2013,8,27,16,8,    0], # southern hemisphere
#        [2013,11,26,14,8,   0], #T=25min
        
        [2014,3,20,2,6,     0],
        [2014,3,28,14,12,   0],
        
        [2014,5,10,10,12,   0],
#        [2014,5,20,22,8,    0], #T=23min
#        [2014,5,21,22,8,    0], #<3hrs
        [2014,5,25,15,11,   0],
        [2014,5,27,12,12,   0],
        [2014,5,29,12,12,   0],
        
        [2014,5,30,20,6,    0],
        [2014,5,31,18,8,    0],
        [2014,6,1,18,8,     0],
        [2014,6,2,21,6,     0],
        [2014,6,3,21,6,     0],
        [2014,6,5,18,12,    0],
        [2014,6,7,20,9,     0],
        [2014,6,9,20,12,    0],
        [2014,6,11,0,12,    0],

        [2014,9,5,16,8,     0],
        [2014,9,13,10,8,    0],
        
        [2014,10,17,0,12,   0],
        [2014,11,5,9,12,    0], #alternating flashes, double frequency 
        [2014,11,7,6,8,     0],
        
        [2014,11,23,15,6,   0],
        [2014,11,28,3,8,    0],
        [2014,12,1,6,7,    0],
        
        [2016,6,25,6,8,     0],
#        [2016,8,16,18,8,    0], #T=17min, N<20
#        [2016,8,17,21,12,   0], #T=21min
        [2016,9,7,0,12,     0], # OCB
        [2016,9,29,18,8,    0],
        [2016,9,30,12,12,   0],
        [2016,10,1,18,12,   0], # OCB
        [2016,10,29,6,12,   0],
        
        [2017,1,14,20,8,    0],
#        [2017,3,13,8,8,     0], #T=18min, N<20
        [2017,3,20,16,16,   0],
#        [2017,4,2,19,8,     0], #incomplete imagery
        [2017,4,18,8,8,     0],
#        [2017,7,25,0,12,    0], # incomplete / alternating scans
#        [2017,9,14,6,16,    0], #T=47min

        [2014,5,27,4,63,   1],
        [2014,6,8,1,84,     1],
        [1001,1,1,1,1,     1], # all quiet sequences
        [1002,1,1,1,1,     1], # all quiet sequences except 2017
        ])
    
#for dctr in [np.shape(day_selec)[0]-3]:
for dctr in range(np.shape(day_selec)[0]-6, np.shape(day_selec)[0]):
#for dctr in range(np.shape(day_selec)[0]):
#for dctr in range(1):
    # iterator for numbering panels
    panels = 'abcdefghijk'
    pan_iter = iter(range(len(panels)))
    
    if day_selec[dctr,0] > 2000:
        # get rough time limits
        mid = datetime(int(day_selec[dctr,0]), int(day_selec[dctr,1]),
                       int(day_selec[dctr,2]), int(day_selec[dctr,3]))
        lower = mid-timedelta(hours=float(day_selec[dctr,4]))
        upper = mid+timedelta(hours=float(day_selec[dctr,4]))
        
        lower_et = tc.datetime2et(lower)
        upper_et = tc.datetime2et(upper)
        # exact timings from data
        val = np.where((times_uvis>lower_et) & 
                       (times_uvis<upper_et))[0]
        lower_et = times_uvis[val[0]]-1
        upper_et = times_uvis[val[-1]]+1
        lower = tc.et2datetime(lower_et)
        upper = tc.et2datetime(upper_et)
    
    # get example times
    if day_selec[dctr,5]>0:
        quiet = np.array([
                [2014,5,10,10,12],
                [2014,5,27,12,12],
                [2014,6,7,20,9],
                [2014,11,7,6,8],
                [2017,3,20,16,16],
                ])
        if day_selec[dctr,0] < 2000:
            if day_selec[dctr,0] == 1001:
                seq = quiet[:-1]
            elif day_selec[dctr,0] == 1002:
                seq = quiet
            times_tmp = np.array([])
            for iii in range(np.shape(seq)[0]):
                mid_tmp = datetime(int(seq[iii,0]), int(seq[iii,1]),
                                   int(seq[iii,2]), int(seq[iii,3]))
                lower_tmp = tc.datetime2et(mid_tmp-timedelta(hours=float(seq[iii,4])))
                upper_tmp = tc.datetime2et(mid_tmp+timedelta(hours=float(seq[iii,4])))
                val = np.where((times_uvis>lower_tmp) & 
                               (times_uvis<upper_tmp))[0]
                times_tmp = np.append(times_tmp, times_uvis[val])
            lower_et = times_tmp[0]
            upper_et = times_tmp[-1]
            lower, upper = tc.et2datetime([lower_et, upper_et])
            mid = lower+(upper-lower)/2
        else:   
            val = np.where((times_uvis>lower_et) & 
                           (times_uvis<upper_et))[0]
            times_tmp = times_uvis[val]
            
        tmp = np.where(np.diff(times_tmp)>3.6e3)[0]
        start = np.concatenate([[times_tmp[0]],times_tmp[tmp+1]])-500
        stop = np.concatenate([times_tmp[tmp], [times_tmp[-1]]])+500
        xlims = ()
        for iii in range(len(start)):
            xlims = (xlims) + ((tc.et2datetime(start[iii]), tc.et2datetime(stop[iii])),)
            
        fractimes = np.mean([start, stop], axis=0)
    else:
        frac = np.array([1/6, 3/6, 5/6])
        # ET times at which images are requested
        fractimes = lower_et + frac*(upper_et-lower_et)
        
    if not np.any(day_selec[dctr,:] - [2017,3,20,16,16,0]):
        tmp = np.array([datetime(2017,3,20,iii,jjj)
                        for iii,jjj in zip([9,15,20],[50,0,20])])
        fractimes = tc.datetime2et(tmp)        
        
    # make figure
    fig = plt.figure()
    fig.set_size_inches(11 if len(fractimes)<4 else 12,
                        12 if len(fractimes)<3 else 11)
    wr = np.append(np.ones((len(fractimes))), [0.05 if len(fractimes)<4 else 0.1])
    gs = gridspec.GridSpec(3, len(fractimes)+1, wspace=0.05, hspace=0.18, width_ratios=tuple(wr),
                           height_ratios=(1 if len(fractimes)<4 else (0.8 if len(fractimes)<5 else 0.6),1,1))
    if day_selec[dctr,5]>0:        
        ax1 = brokenaxes(xlims=xlims, wspace=0.15, subplot_spec=gs[1,:len(fractimes)], d=0.008, tilt=60, despine=False)
        ax2 = brokenaxes(xlims=xlims, wspace=0.15, subplot_spec=gs[2,:len(fractimes)], d=0.008, tilt=60, despine=False)
    else:
        ax1 = plt.subplot(gs[1,:len(fractimes)])
        ax2 = plt.subplot(gs[2,:len(fractimes)], sharex=ax1)
    
    inc_img = imglist[(imglist['ET_STOP']>lower_et) & 
                      (imglist['ET_START']<upper_et) &
                      (imglist['HEMISPHERE'] == 'North')]
    inc_img['ET'] = inc_img.loc[:,['ET_START','ET_STOP']].mean(axis=1)
    for ftctr in range(len(fractimes)):
        # returns actual index of the dataframe!
        tmp = np.argmin(np.abs(inc_img['ET']-fractimes[ftctr]))
        thisfile = '{}{}'.format(uvispath, inc_img.loc[tmp,'FILEPATH'])
        name = inc_img.loc[tmp,'FILEPATH'].split('/')[-1].split('.')[0]
        
        ax = plt.subplot(gs[0,ftctr], projection='polar')
        # get image with reflected sunlight removed
        rsr.calculate(thisfile)
        
        KR_MIN=0.5
        KR_MAX=30
        data = rsr.redImage_aur
        data[data<0.1] = 0.1
        lonbins = np.linspace(0, 2*np.pi, num=np.shape(data)[0]+1)
        colatbins = np.linspace(0, 30, num=np.shape(data)[1]+1)
        quad = ax.pcolormesh(lonbins, colatbins, data.T, cmap=cmap_UV)
        quad.set_clim(0, KR_MAX)
        quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
        ax.set_facecolor('gray')
        
        timestr = datetime.strftime(tc.et2datetime(inc_img.loc[tmp,'ET_START']),
                                    '%Y-%j %H:%M:%S')
        ax.set_title('{}\n{}, {:.1f} min'.format(timestr, 'North', inc_img.loc[tmp,'EXP']/60), fontsize=11, y=1)
        ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
        ticklabels = ['00','06','12','18']
        for iii in range(len(ticklabels)):
            txt = ax.text(ticks[iii], 26, ticklabels[iii], color='w', fontsize=11, fontweight='bold',
                          ha='center',va='center')
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
        ax.set_xticks(ticks)
        ax.set_xticklabels([])
        ax.set_yticks([10,20,30])
        ax.set_yticklabels([])
        ax.grid('on', color='0.8', linewidth=1)
        ax.set_theta_zero_location("N")
        ax.set_rmax(30)
        ax.set_rmin(0)
        
        fillmin = [0,22]
        fillmax = [8,30]
        for iii in range(len(fillmin)):
#            ax.fill_between(lonbins,
#                            np.full_like(lonbins, fillmin[iii]),
#                            np.full_like(lonbins, fillmax[iii]),
#                            color='grey',
#                            alpha=0.7)
            ax.fill_between(lonbins,
                            np.full_like(lonbins, fillmin[iii]),
                            np.full_like(lonbins, fillmax[iii]),
                            facecolor='0.9',
                            edgecolor='k',
                            alpha=0.4,
                            hatch='xx')
            
        if not np.any(day_selec[dctr,:] - [2017,3,20,16,16,0]):
            # plot PPO upward current markers
            ax.plot([pmp.getMagPhase(inc_img.loc[tmp,'ET_START'], 'N')-np.pi/2, 
                     pmp.getMagPhase(inc_img.loc[tmp,'ET_START'], 'N')-np.pi/2],
                     [0, 30],
                     ls='-' if thishem=='North' else '--',
                     c='gold',
                     lw=1.5)
            ax.plot([pmp.getMagPhase(inc_img.loc[tmp,'ET_START'], 'S')-np.pi/2, 
                     pmp.getMagPhase(inc_img.loc[tmp,'ET_START'], 'S')-np.pi/2],
                     [0, 30],
                     ls='--' if thishem=='North' else '-',
                     c='gold',
                     lw=1.5)
        
        if not ftctr:
            # plot colorbar
            cbar = plt.colorbar(quad, cax=plt.subplot(gs[0,len(fractimes)]))
            cbar.set_label('Intensity (kR)', labelpad=10, fontsize=11, rotation=270)
            cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
                                               np.arange(1,10,1)),
                                     np.arange(10,101,10)))
            cbar.ax.tick_params(labelsize=10)
        if not present:
            txt = ax.text(0, 1, '({})'.format(panels[next(pan_iter)]),
                          transform=ax.transAxes, ha='center', va='center',
                          color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
        tmpax = ax1 if day_selec[dctr,5]==0 else ax1.axs[ftctr]
        tmpax.annotate('', xy=((tc.et2datetime(inc_img.loc[tmp,'ET'])),24),
                       xycoords='data', xytext=((tc.et2datetime(inc_img.loc[tmp,'ET'])),27),
                       arrowprops=dict(facecolor='black', shrink=0.01))

    # plot UV
    val = np.where((times_uvis>lower_et) & 
                   (times_uvis<upper_et))[0]
    quad = ax1.pcolormesh(tc.et2datetime(times_uvis[val]), ltbins_uvis,
                        keo_uvis[val[:-1],:].T, cmap=cmap_UV,
                        norm=mcolors.LogNorm(vmin=1e-1, vmax=1e1))
    cbar = plt.colorbar(quad[0] if day_selec[dctr,5]>0 else quad, cax=plt.subplot(gs[1,len(fractimes)]))
    cbar.set_label('UV power (GW)', rotation=270,
                    labelpad=15)
    
    # plot PPO FACs
    val = np.where((dts_ppo_n>lower) & 
                   (dts_ppo_n<upper))[0]
    ax1.plot(dts_ppo_n[val], ppo_n_up[val], ls='-' if thishem=='North' else '--',
             c='gold',
             lw=1.5,
             label='PPO N max upward FAC')
    val = np.where((dts_ppo_s>lower) & 
                   (dts_ppo_s<upper))[0]
    ax1.plot(dts_ppo_s[val], ppo_s_up[val], ls='-' if thishem=='South' else '--',
             c='gold',
             lw=1.5,
             label='PPO S max upward FAC')
    if day_selec[dctr,5] and day_selec[dctr,0]==2014:
        val = np.where((dt_thin>lower) & 
                   (dt_thin<upper))[0]
        ax1.plot(dt_thin[val], mean_thin[val], color='orange', ls=':',
                 lw=2,
                 label='PPO thin current sheet', zorder=10)
#    ax1.scatter(dts_ppo_s, ppo_s_thin, color='gold', s=8 if thishem=='South' else 1,
#             label='PPO S thin current sheet')
    
    # plot sector sums
    LTMIN = [0,3,15]
    LTMAX = [24,9,21]
    name = ['total','{}-{} LT'.format(LTMIN[1], LTMAX[1]), '{}-{} LT'.format(LTMIN[2], LTMAX[2])]
    colors = ['k', myblue, myred]
    
    for iii in range(len(LTMIN)):
        val = np.where((times_uvis>lower_et) & 
                   (times_uvis<upper_et))[0]
        uv = keo_uvis[val[:-1],:]
        val_lt = np.where((ltcenters_uvis>LTMIN[iii]) & (ltcenters_uvis<LTMAX[iii]))[0]
        uv = np.sum(uv[:,val_lt], axis=1)
        xs = times_uvis[val[:-1]] + np.diff(times_uvis[val])/2
        uvis_dt = tc.et2datetime(xs[np.isfinite(uv)])
        uv = uv[np.isfinite(uv)]
        ins = np.where(np.diff(uvis_dt)>timedelta(hours=1.5))[0]+1
        uvis_dt = np.insert(uvis_dt, ins, uvis_dt[ins]-timedelta(minutes=10))
        uv = np.insert(uv, ins, np.nan)
        ax2.scatter(uvis_dt, uv, s=10 if day_selec[dctr,5]==0 else 5, color=colors[iii], marker='o')
        ax2.plot(uvis_dt, uv, color=colors[iii], ls='-', label=name[iii], lw=1.5 if day_selec[dctr,5]==0 else 1)
        
    # horizontal lines
    for iii in range(1,3):
        val_lt = np.where((ltcenters_uvis>LTMIN[iii]) & (ltcenters_uvis<LTMAX[iii]))[0]
        minlt = ltbins_uvis[val_lt[0]]
        maxlt = ltbins_uvis[val_lt[-1]+1]
        for lim in [minlt, maxlt]:
#            ax1.axhline(lim, color='w', linestyle='-', linewidth=2)
            ax1.axhline(lim, color=colors[iii], linestyle='--', linewidth=1.5)
        
    # setting grid
    for ax in [ax1,ax2]:
        if day_selec[dctr,5]>0:
            axs = ax.axs
        else:
            axs = [ax]
        for ax in axs:
            ax.grid(which='major', color='k', linestyle='-', linewidth=0.5, alpha=0.5) 
            ax.grid(which='minor', color='k', linestyle='--', linewidth=0.5, alpha=0.6)
    
    # keogram limits and labels
    ax1.set_ylabel('LT (h)')    
    if day_selec[dctr,5]>0:
        for tax in ax1.axs:
            tax.set_facecolor('grey')
            tax.set_ylim([0,24])
            tax.set_yticks(np.arange(0,25,6))            
    else:
        ax1.set_facecolor('grey')
        ax1.set_xlim([tmin, tmax])
        ax1.set_ylim([0,24])
        ax1.set_yticks(np.arange(0,25,6))

    leg = ax1.legend(bbox_to_anchor=(0.05, -0.07, 0.9, .05),
                     ncol=3, mode="expand", borderaxespad=0.,
                     shadow=True,
                     facecolor='0.4', edgecolor='k', fancybox=True,
                     prop=dict(weight='bold'))
    for text in leg.get_texts():
        text.set_color('w')
          
    # sector sums      
    ax2.legend(loc=1, shadow=True, ncol=3)
    if day_selec[dctr,5]>0:
        ax2.set_ylabel('UV power (GW)', rotation=270, labelpad=40)
        ax2.big_ax.yaxis.set_label_position('right')
#        ax2.axs[len(ax2.axs)-1].yaxis.tick_right()
        for tax in ax2.axs:
            tax.set_yscale('log')
            tax.set_ylim([5e-1,1e2])
    else:
        ax2.set_ylabel('UV power (GW)', rotation=270, labelpad=15)
        ax2.yaxis.set_label_position('right')
        ax2.yaxis.tick_right()
        ax2.set_yscale('log')
        ax2.set_ylim([5e-1,1e2])
    
    # panel labeling
    for ax in [ax1, ax2]:
        if not present:
            tmpax = ax if day_selec[dctr,5]==0 else ax.big_ax
            txt = tmpax.text(0.03, 0.92,
                             '({})'.format(panels[next(pan_iter)]),
                              transform=tmpax.transAxes, ha='center', va='center',
                              color='k', fontweight='bold', fontsize=15)
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
    
    if day_selec[dctr,5]>0:
#        fig.autofmt_xdate()
        for ax in [ax1,ax2]:
            for sax in ax.axs:
                sax.xaxis.set_major_locator(mdates.HourLocator(byhour=[0,12]))
                sax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%j\n%H:%M'))
                sax.xaxis.set_minor_locator(mdates.HourLocator(byhour=np.arange(0,24,3)))
#                sax.tick_params(axis='x', which='both', labelbottom=False if ax==ax1 else True)
            for iii in range(len(fractimes)):
                ax.axs[iii].tick_params(axis='both', which='both',
                      left=True if iii==0 else False,
                      labelleft=True if iii==0 else False,
                      right=True if iii==len(fractimes)-1 else False,
                      labelright=True if (iii==len(fractimes)-1 and ax==ax2) else False,
                      top=True,
                      bottom=True,
                      labelbottom=False if ax==ax1 else True)
    else:
        fig.autofmt_xdate()
        ax1.xaxis.set_major_locator(mdates.HourLocator(interval=3))
        ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%j\n%H:%M'))
        ax1.xaxis.set_minor_locator(mdates.HourLocator(interval=1))
        ax1.set_xlim([lower, upper])
        
    # set title
    fmt = '%Y-%m-%d (DOY %j)'
#    ax1.set_title('{0} ({1})'.format(datetime.strftime(mid, fmt), thishem))
    print('Saving {0} ({1})'.format(datetime.strftime(mid, fmt), thishem))
    
    # save
    fname = '{}/UV_dawn_dusk_{}comp_{}_{}_{}'.format(savepath,
             'long' if day_selec[dctr,5] else '',
             int(day_selec[dctr,0]),
             int(day_selec[dctr,1]),
             int(day_selec[dctr,2]))
    plt.savefig(fname+'.png', bbox_inches='tight', dpi=mydpi)
#    plt.savefig(fname+'.pdf', bbox_inches='tight',)
    plt.close()
    
#%% 
# =============================================================================
# plot long sequences
# =============================================================================

# get Tao SW model data
swpath = '{}/SW propagation/Tao/2014/TAO2014_OM.txt'.format(boxpath)
names = ['yy', 'mon', 'day', 'time', 'rho', 'T', 'vx', 'vy', 'by', 'pdy', 'dphi']
dtypes = [int, int, int, 'U8', float, float, float, float, float, float, float]
tao = {}
for nnn in range(len(names)):
    tao[names[nnn]] = np.genfromtxt(swpath, usecols=(nnn), dtype=dtypes[nnn])
tao['hr'] = np.array([int(ttt.split(':')[0]) for ttt in tao['time']])
tao['dt'] = np.array([
        datetime(int(tao['yy'][iii]),
                 int(tao['mon'][iii]),
                 int(tao['day'][iii]),
                 int(tao['hr'][iii]))
        for iii in range(len(tao['yy']))])

# get mswim SW model data
swpath = '{}/SW propagation/mswim/MSWIM2014_OM.dat'.format(boxpath)
names = np.genfromtxt(swpath, dtype='U5', skip_header=5, max_rows=1)
mswim = {}
for nnn in range(len(names)):
    mswim[names[nnn]] = np.genfromtxt(swpath, skip_header=6, usecols=(nnn))
mswim['dt'] = np.array([
        datetime(int(mswim['yy'][iii]),
                 int(mswim['mon'][iii]),
                 int(mswim['day'][iii]),
                 int(mswim['hr'][iii]))
        for iii in range(len(mswim['yy']))])
# number density, /m3
rho = mswim['rho']*1e6
# mass density assuming protons, kg/m3
rho *= 1.67e-27
# total velocity, m/s
vtot = np.sqrt(((mswim['vr']**2)+(mswim['vt']**2)+(mswim['vn']**2)))*1e3
# pressure p = rho*u**2/2, nPa
mswim['pdyn'] = rho*vtot**2 * 1e9
    
fig = plt.figure()
if present:
    fig.set_size_inches(16,6.5)
else:
    fig.set_size_inches(20,8)
gs = gridspec.GridSpec(4, 2, wspace=0.03, hspace=0.1, width_ratios=(1,0.01))
ax1 = plt.subplot(gs[0,0])
ax2 = plt.subplot(gs[1,0], sharex=ax1)
ax3 = plt.subplot(gs[2,0], sharex=ax1)
ax4 = plt.subplot(gs[3,0], sharex=ax1)

# plot UV
quad = ax1.pcolormesh(tc.et2datetime(times_uvis), ltbins_uvis,
                    keo_uvis.T, cmap=cmap_UV,
                    norm=mcolors.LogNorm(vmin=1e-1, vmax=1e1))
cbar = plt.colorbar(quad, cax=plt.subplot(gs[0,1]))
cbar.set_label('UV power (GW)', rotation=270,
                labelpad=20)

# plot dawn sector sums
LTMIN = [0,3,15]
LTMAX = [24,9,21]
name = ['total','{}-{} LT'.format(LTMIN[1], LTMAX[1]), '{}-{} LT'.format(LTMIN[2], LTMAX[2])]
colors = ['k', myblue, myred]

for iii in range(len(LTMIN)):
    val_lt = np.where((ltcenters_uvis>LTMIN[iii]) & (ltcenters_uvis<LTMAX[iii]))[0]
    uv = np.sum(keo_uvis[:,val_lt], axis=1)
    xs = times_uvis[:-1] + np.diff(times_uvis)/2
    uvis_dt = tc.et2datetime(xs[np.isfinite(uv)])
    uv = uv[np.isfinite(uv)]
    ins = np.where(np.diff(uvis_dt)>timedelta(hours=1.5))[0]+1
    uvis_dt = np.insert(uvis_dt, ins, uvis_dt[ins]-timedelta(minutes=10))
    uv = np.insert(uv, ins, np.nan)
    ax2.scatter(uvis_dt, uv, s=1, color=colors[iii], marker='o', label=name[iii])
    ax2.plot(uvis_dt, uv, color=colors[iii], ls='-', lw=0.3)
    ax2.set_ylabel('UV power (GW)')
    ax2.legend(loc=1, ncol=3, shadow=True)

for iii in range(1,3):
    val_lt = np.where((ltcenters_uvis>LTMIN[iii]) & (ltcenters_uvis<LTMAX[iii]))[0]
    minlt = ltbins_uvis[val_lt[0]]
    maxlt = ltbins_uvis[val_lt[-1]+1]
    for lim in [minlt, maxlt]:
        ax1.axhline(lim, color='w', linestyle='-', linewidth=1.5)
        ax1.axhline(lim, color=colors[iii], linestyle='--', linewidth=1)

axs = [ax1]
for ax in axs:
    ax.set_facecolor('grey')
    ax.set_xlim([tmin, tmax])
    ax.set_yticks(np.arange(0,25,6))
    ax.set_ylabel('LT (h)')
    
ax3.plot(mswim['dt'], mswim['pdyn'], c='k', ls='-', label='mSWIM')
ax3.plot(tao['dt'], tao['pdy'], c='grey', ls='-', label='Tao')
ax3.set_yscale('log')
ax3.set_ylim([0.5e-3,0.7e-1])
ax3.set_ylabel('SW $p_\mathrm{dyn}$ (nPa)')
ax3.legend(loc=1, ncol=2, shadow=True)

ax4.plot(mswim['dt'], mswim['vr'], c='k', ls='-', label='mSWIM')
ax4.plot(tao['dt'], tao['vx'], c='grey', ls='-', label='Tao')
ax4.set_ylim([300,500])
ax4.set_ylabel('SW $v$ (km/s)')
ax4.legend(loc=1, ncol=2, shadow=True)
    
panels = 'abcdefghijk'
pan_iter = iter(range(len(panels)))
for ax in [ax1,ax2,ax3,ax4]:    
    ax.grid(which='major', color='k', linestyle='-', linewidth=0.5, alpha=0.5) 
    ax.grid(which='minor', color='k', linestyle='--', linewidth=0.5, alpha=0.6)
    if not present:
        txt = ax.text(0.02, 0.90, '({})'.format(panels[next(pan_iter)]),
                      transform=ax.transAxes, ha='center', va='center',
                      color='k', fontweight='bold', fontsize=12)
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])

fig.autofmt_xdate()
    
day_selec = np.array([
        [2014,5,25,5,415], #extended range
        ])
        
for dctr in range(np.shape(day_selec)[0]):
    lower = datetime(int(day_selec[dctr,0]),
                     int(day_selec[dctr,1]),
                     int(day_selec[dctr,2]),
                     int(day_selec[dctr,3]))
    upper = lower+timedelta(hours=float(day_selec[dctr,4]))
    # set x axis range
    validtimes = times_uvis[(tc.datetime2et(lower) < times_uvis) &
                            (tc.datetime2et(upper) > times_uvis)]
    if not np.any(validtimes):
        continue
    ax1.set_xlim([tc.et2datetime(validtimes[0]), tc.et2datetime(validtimes[-1])])    
    
    ax1.xaxis.set_major_locator(mdates.DayLocator(interval=1))
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%j'))
    ax1.xaxis.set_minor_locator(mdates.HourLocator(byhour=[0,6,12,18,24]))
    
    # set title
    fmt = '%Y-%m-%d (DOY %j)'
#    ax1.set_title('{0} to {1} ({2})'.format(datetime.strftime(lower, fmt),
#                                            datetime.strftime(upper, fmt),
#                                            'North'))
    
    # save
    fname = '{}/long_UV_dawn_dusk_comp_{}_{}_{}'.format(savepath,
             int(day_selec[dctr,0]),
             int(day_selec[dctr,1]),
             int(day_selec[dctr,2]))
    
    ax2.set_ylim([0,1e2])
    fig.set_size_inches(12 if present else 10, 9)
    plt.savefig('{}.png'.format(fname), bbox_inches='tight', dpi=mydpi)
#    plt.savefig('{}.pdf'.format(fname), bbox_inches='tight')
    
    ax2.set_yscale('log')
    ax2.set_ylim([5e-1,2.5e2])
    plt.savefig('{}_log.png'.format(fname), bbox_inches='tight', dpi=mydpi)
#    plt.savefig('{}_log.pdf'.format(fname), bbox_inches='tight')
    plt.close()

#%% 
# =============================================================================
# plot histograms
# =============================================================================
#for dctr in range(1,3):
for dctr in [3]:
    if dctr == 0:
        # all data
        lower = tc.et2datetime(imglist['ET_START'].iloc[0])
        upper = tc.et2datetime(imglist['ET_STOP'].iloc[-1])
        sect = selec[(selec['ET_STOP']>tc.datetime2et(lower)) & 
                     (selec['ET_START']<tc.datetime2et(upper))]
        selecname = 'all'
        selectitle = 'All UVIS data ({} images, {} h)'.format(
                len(sect), int(sect['EXP'].sum()/3600))
        count_max = 3000
        bins = np.logspace(np.log10(3e-3),np.log10(3e1),num=51)
        
        # set up figure
        gs = gridspec.GridSpec(1, 4, wspace=0.03,
                           width_ratios=(1, 0.05, 0.15, 1))
        fig = plt.figure()
        if present:
            fig.set_size_inches(12,4)
        else:
            fig.set_size_inches(17,6)
        ax1 = plt.subplot(gs[0,0])
        cax = plt.subplot(gs[0,1])
        ax2 = plt.subplot(gs[0,3])
    elif dctr == 1:
        # 2014 long sequence
        lower = datetime(2014,5,25,5)
        upper = lower+timedelta(hours=415)
        sect = selec[(selec['ET_STOP']>tc.datetime2et(lower)) & 
                     (selec['ET_START']<tc.datetime2et(upper))]
        selecname = '2014_long_quiet_comb'
        selectitle = '2014 DOY 145-162 ({} images, {} h)'.format(
                len(sect), int(sect['EXP'].sum()/3600))
        count_max = 1500
        bins = np.logspace(np.log10(3e-3),np.log10(2e1),num=31)
        
        # set up figure
        gs = gridspec.GridSpec(2, 4, wspace=0.08, hspace=0.3,
                           width_ratios=(1, 0.05, 0.15, 1))
        fig = plt.figure()
        fig.set_size_inches(13,10)
        ax1 = plt.subplot(gs[1,0])
        cax = plt.subplot(gs[1,1])
        ax2 = plt.subplot(gs[1,3])
    elif dctr == 2:
        #quiet periods
        day_selec = np.array([
                [2014,5,10,10,12],
                [2014,5,27,12,12],
                [2014,6,7,20,9],
                [2014,11,7,6,8],
                [2017,3,20,16,16],
        ])
        valind = []
        for dctr in range(np.shape(day_selec)[0]):
            mid = datetime(int(day_selec[dctr,0]),
                           int(day_selec[dctr,1]),
                           int(day_selec[dctr,2]),
                           int(day_selec[dctr,3]))
            lower = mid-timedelta(hours=float(day_selec[dctr,4]))
            upper = mid+timedelta(hours=float(day_selec[dctr,4]))
            tmp = selec[(selec['ET_STOP']>tc.datetime2et(lower)) &
                        (selec['ET_START']<tc.datetime2et(upper))]
            valind = np.append(valind, tmp.index)
        sect = selec.loc[valind]
        selectitle = 'Quiet periods ({} images, {} h)'.format(
                len(sect), int(sect['EXP'].sum()/3600))
        count_max = 800
        bins = np.logspace(np.log10(3e-3),np.log10(2e1),num=26)
        
        # set up figure
        ax1 = plt.subplot(gs[0,0])
        cax = plt.subplot(gs[0,1])
        ax2 = plt.subplot(gs[0,3])
    if dctr == 3:
        # Carbary data
        start = datetime(2007,4,6)
        stop = datetime(2009,1,23)
        sect = selec[(selec['ET_STOP']>tc.datetime2et(start)) & 
                     (selec['ET_START']<tc.datetime2et(stop)) &
                     (selec['HEMISPHERE']=='North')]
        selecname = 'carbary'
        selectitle = 'Carbary 2012 UVIS data ({} images, {} h)'.format(
                len(sect), int(sect['EXP'].sum()/3600))
        count_max = 300
        bins = np.logspace(np.log10(3e-3),np.log10(3e1),num=51)
        
        # set up figure
        gs = gridspec.GridSpec(1, 4, wspace=0.03,
                           width_ratios=(1, 0.05, 0.15, 1))
        fig = plt.figure()
        if present:
            fig.set_size_inches(12,4)
        else:
            fig.set_size_inches(17,6)
        ax1 = plt.subplot(gs[0,0])
        cax = plt.subplot(gs[0,1])
        ax2 = plt.subplot(gs[0,3])
    print(selectitle, sect['EXP'].sum())

    data = np.concatenate([sect[SECTIONS_TO_USE].tolist()])/1e9
    
    bincenters = bins[:-1]+np.diff(bins)/2
    histogram = np.zeros((np.shape(data)[1], len(bins)-1))
    for iii in range(np.shape(data)[1]):
        tmp = data[:,iii]
        hist, _ = np.histogram(tmp[np.isfinite(tmp)], bins=bins)
        histogram[iii,:] = hist
        
    ltcenters = ltbins_uvis[:-1]+np.diff(ltbins_uvis)/2    
    
    quad = ax1.pcolormesh(bins,
                         ltbins_uvis,
                         histogram,
                         cmap=cmap_UV,
                         norm=mcolors.LogNorm(
                                 vmin=1,
                                 vmax=histogram.max()))
    
    ax1.set_facecolor('0.1')    
    ax1.set_xlim([np.min(bins), np.max(bins)])
    ax1.set_xscale('log')
    ax1.set_xlabel('UV power (GW)')
    ax1.set_ylim([0,24])
    ax1.set_yticks(np.arange(0,25,6))
    ax1.set_ylabel('LT (h)')
    ax1.grid(which='major', color='0.5', linestyle='-', linewidth=1, alpha=0.8) 
    ax1.grid(which='minor', color='0.5', linestyle='--', linewidth=1, alpha=0.8)
    cbar = plt.colorbar(quad, cax=cax)
    ax1.set_title(selectitle)
    if not present:
        ax1.text(0.05, 0.95, '({})'.format('a' if dctr!=1 else 'c'), transform=ax1.transAxes, 
                 fontsize=16, fontweight='bold',
                 va='center', ha='center',
                 color='w')
    
    ydata = ltcenters
    ydata = np.concatenate([[ltcenters[-1]-24], ltcenters, [ltcenters[0]+24]])
    
    xdata = np.nanmean(data, axis=0)
    xdata = np.concatenate([[xdata[-1]],xdata,[xdata[0]]])
    error = np.nanstd(data, axis=0)
    error = np.concatenate([[error[-1]],error,[error[0]]])
    ax1.plot(xdata, ydata, c='k', ls='-', marker='o', label='mean (std)',
             markersize=3, linewidth=1.5)
    ax1.fill_betweenx(ydata, xdata, xdata+error, color='k', alpha=0.3)
    ax1.plot(xdata+error, ydata, color='k', alpha=0.4)
    
    xdata = np.nanmedian(data, axis=0)
    xdata = np.concatenate([[xdata[-1]],xdata,[xdata[0]]])
    error = np.nanmedian(np.abs(data-np.nanmedian(data, axis=0)), axis=0)
    error = np.concatenate([[error[-1]],error,[error[0]]])
    ax1.plot(xdata, ydata, c='sienna', ls='--', marker='x', label='median (mad)',
             markersize=4, linewidth=1.5)
    ax1.fill_betweenx(ydata, xdata, xdata+error, color='sienna', alpha=0.3)
    ax1.plot(xdata+error, ydata, color='sienna', alpha=0.4)
    
    ax1.legend(loc=1, shadow=True)
    
    
    diff = 2.1
    dd = [6,18]
    nn = ['Dawn', 'Dusk']
    cc = [myblue, myred]
    for iii in range(len(dd)):
        arg = np.where(np.abs(ltcenters-dd[iii])<diff)[0]
        thismedian = np.nanmedian(data[:,arg])
        tmp = np.sum(histogram[arg,:], axis=0)
        ax2.step(bins[:-1], tmp, where='post', color=cc[iii])
        ax2.bar(bins[:-1]+np.diff(bins)/2, tmp, width=np.diff(bins),
               color=cc[iii], alpha=0.5, label=nn[iii])
        # smaller than limit
        n = np.sum(data[:,arg]<bins[0])
        shift = np.diff(bins)[0]/4 * (1 if dd[iii]==6 else 3)
        ax2.bar(bins[0]+shift, n, width=np.diff(bins)[0]/2,
               color=cc[iii], alpha=0.5, edgecolor='k', linewidth=1,
               hatch='xxx')
        
        # larger than limit
        n = np.sum(data[:,arg]>bins[-1])
        shift = np.diff(bins)[-1]/4 * (1 if dd[iii]==6 else 3)
        ax2.bar(bins[-2]+shift, n, width=np.diff(bins)[-1]/2,
               color=cc[iii], alpha=0.5, edgecolor='k', linewidth=1,
               hatch='xxx')
        
        
        ax2.axvline(thismedian, color=cc[iii], ls='-', lw=2)
        
        ax1.axhline(ltbins_uvis[arg[0]], linestyle='--', linewidth=2,
                    color=cc[iii])
        ax1.axhline(ltbins_uvis[arg[-1]+1], linestyle='--', linewidth=2,
                    color=cc[iii])
#    ax2.set_yscale('log')
#    ax2.set_ylim([10, count_max])
    ax2.set_ylim([0, count_max])
    ax2.set_xscale('log')
    ax2.set_xlim([bins[0], bins[-1]])
    ax2.grid(which='major', color='0.5', linestyle='-', linewidth=1, alpha=0.8) 
    ax2.grid(which='minor', color='0.5', linestyle='--', linewidth=1, alpha=0.8)
    ax2.legend(loc=1, shadow=True)
    ax2.set_xlabel('UV power(GW)')
    ax2.set_ylabel('Occurrence')
    ax2.set_title(selectitle)
    if not present:
        ax2.text(0.05, 0.95, '({})'.format('b' if dctr!=1 else 'd'), transform=ax2.transAxes, 
                 fontsize=16, fontweight='bold',
                 va='center', ha='center',
                 color='k')
    
    if not dctr ==1:
        plt.savefig('{}/UVIS_hist_{}.png'.format(savepath, selecname),
                    bbox_inches='tight', dpi=mydpi)
        plt.savefig('{}/UVIS_hist_{}.pdf'.format(savepath, selecname),
                    bbox_inches='tight')
        plt.show()
        plt.close()
        
sys.exit()
   
#%%
# =============================================================================
#     plot average maps
# =============================================================================
# 2014 long sequence
lower = datetime(2014,5,25,5)
upper = lower+timedelta(hours=415)
sect = selec[(selec['ET_STOP']>tc.datetime2et(lower)) & 
             (selec['ET_START']<tc.datetime2et(upper))]
selecname = '2014_long'
selectitle = '2014 DOY 145-162 ({} images, {} h)'.format(
        len(sect), int(sect['EXP'].sum()/3600))

# get first map
fname = '{}{}'.format(uvispath, sect.loc[:,'FILEPATH'].iloc[0])
img, angles, _ = get_image.getRedUVIS(fname, minangle=10)
data = np.full(((len(sect),)+np.shape(img)), np.nan)
num = np.zeros((np.shape(img)))

for iii in range(len(sect)):
    thisfile = '{}{}'.format(uvispath, sect.loc[:,'FILEPATH'].iloc[iii])
    rsr.calculate(thisfile)
    data[iii,:,:] = rsr.redImage_aur
    num += np.isfinite(rsr.redImage_aur)
    if not (iii+1)%20:
        print('{} / {}'.format(iii+1, len(sect)))
    
mean = np.nanmean(data, axis=0)
median = np.nanmedian(data, axis=0)

fig = plt.figure()
fig.set_size_inches(12, 6)
gs = gridspec.GridSpec(1, 3, wspace=0.1, width_ratios=(1,1,0.05))

panels = 'abcdefghijk'
pan_iter = iter(range(len(panels)))

for ctr, mmm, name in zip(range(2), [mean, median], ['mean','median']):        
    ax = plt.subplot(gs[0,ctr], projection='polar')
    
    KR_MIN=1
    KR_MAX=5
    data = mmm
    data[data<0.1] = 0.1
    lonbins = np.linspace(0, 2*np.pi, num=np.shape(data)[0]+1)
    colatbins = np.linspace(0, 30, num=np.shape(data)[1]+1)
    quad = ax.pcolormesh(lonbins, colatbins, data.T, cmap=cmap_UV)
    quad.set_clim(0, KR_MAX)
#    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    ax.set_facecolor('gray')
    
    ax.set_title('{}\n{}'.format(selectitle,name))
    ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
    ticklabels = ['00','06','12','18']
    for iii in range(len(ticklabels)):
        txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                      ha='center',va='center')
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
    ax.set_xticks(ticks)
    ax.set_xticklabels([])
    ax.set_yticks([10,20,30])
    ax.set_yticklabels([])
    ax.grid('on', color='0.8', linewidth=1)
    ax.set_theta_zero_location("N")
    ax.set_rmax(30)
    ax.set_rmin(0)
    
    if not ctr:
        # plot colorbar
        cbar = plt.colorbar(quad, cax=plt.subplot(gs[0,2]))
        cbar.set_label('Intensity (kR)', labelpad=15, fontsize=11, rotation=270)
#        cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
#                                           np.arange(1,10,1)),
#                                 np.arange(10,101,10)))
        cbar.ax.tick_params(labelsize=10)
    txt = ax.text(0, 1, '({})'.format(panels[next(pan_iter)]),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=20)
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
plt.savefig('{}/UVIS_map_{}.png'.format(savepath, selecname),
            bbox_inches='tight',
            dpi=mydpi)
#plt.savefig('{}/UVIS_map_{}.pdf'.format(savepath, selecname),
#            bbox_inches='tight')
plt.show()
plt.close()
        
    
#sel = imglist[(imglist['YEAR']==2014)&(imglist['DOY']>144) & (imglist['DOY']<163)]
