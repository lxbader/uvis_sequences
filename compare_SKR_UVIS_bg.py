import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.optimize as sopt
import skr_power
import time_conversions as tc
import uvisdb

bgfile = '{}/imglists/UVIS_power_bg.txt'.format(boxpath)

udb = uvisdb.UVISDB()
imgdb = udb.currentDF

def linFit(x, m, k):
    return m*x+k

def set_grid(ax, color='k'):
        ax.grid(which='major', color=color, linestyle='-', linewidth=0.5, alpha=0.5) 
        ax.grid(which='minor', color=color, linestyle='--', linewidth=0.5, alpha=0.6)

savepath = '%s/Plots/UV_seq/SKR_UV_comp' % (boxpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

skrpw = skr_power.SKRpower(datapath)


imgdb['ET'] = np.mean(imgdb.loc[:,['ET_START','ET_STOP']], axis=1)
imgdb['UV_POW_BG'] = np.nan

tmp = np.genfromtxt(bgfile, skip_header=1, usecols=(1,2))
indices = [np.argmin(np.abs(imgdb['ET']-tmp[iii,0])) for iii in range(len(tmp))]
imgdb.at[indices,'UV_POW_BG'] = tmp[:,1]

hems = ['North','South']
sys.exit()
    
#====================
# define time windows
subintervnames = ['all','pre-equinox','post-equinox','2008','2013','2014','2016','2017']
for iii in range(len(subintervnames)):
    si = subintervnames[iii]
    imgdb[si] = 0
    if si=='all':
        imgdb[si] = 1
    elif si=='pre-equinox':
        valid = np.where(imgdb['ET']<tc.datetime2et(datetime(2010,1,1)))[0]
        imgdb.at[valid,si] = 1
    elif si=='post-equinox':
        valid = np.where(imgdb['ET']>tc.datetime2et(datetime(2010,1,1)))[0]
        imgdb.at[valid,si] = 1
    elif si[:2]=='20':
        year = int(si)
        valid = np.where((imgdb['ET']>tc.datetime2et(datetime(year,1,1)))*
                         (imgdb['ET']<tc.datetime2et(datetime(year+1,1,1)))
                         )[0]
        imgdb.at[valid,si] = 1
        
#=========================
# plot        
for plotctr in range(len(subintervnames)):
    plot_subinterv = [subintervnames[plotctr]]
    colors = ['k']
    
    #plot_subinterv = ['pre-equinox','post-equinox']
    #colors = ['r','b']
    
    #plot_subinterv = ['2008','2013','2014','2016','2017']
    #colors = ['r','b','g','y','k']
    
    fig, axes = plt.subplots(1, 2, sharex='all', sharey='all')
    fig.set_size_inches(12,6)
    for iii in range(2):
        ax = axes[iii]    
        ax.set_title(hems[iii])
        ax.set_xlabel('SKR power (W/m^2)')
        
        thishem = hems[iii]
        
        for sss in range(len(plot_subinterv)):
            # find right images            
            arg = np.where((imgdb.loc[:,'HEMISPHERE'] == thishem)*
                           (imgdb.loc[:,plot_subinterv[sss]])
                           )[0]
            
            # get valid data
            valtimes = imgdb.loc[arg,'ET']
            xdata = np.log10(skrpw.get1hSKR(valtimes, 'RH' if thishem=='North' else 'LH'))
            ydata = np.array(np.log10(imgdb.loc[arg,'UV_POW_BG']))
            valid = np.where((np.isfinite(xdata))&(np.isfinite(ydata)))[0]
            xdata = xdata[valid]
            ydata = ydata[valid]
            
            xplotdata = np.log10(skrpw.get1hSKR(valtimes, 'RH' if thishem=='North' else 'LH'))
            ax.scatter(skrpw.get1hSKR(valtimes, 'RH' if thishem=='North' else 'LH'),
                       imgdb.loc[arg,'UV_POW_BG'], marker='+',
                       color=colors[sss], edgecolor='none', s=100, linewidth=0.5,
                       label=plot_subinterv[sss])
#            ax.scatter(imgdb.loc[arg,'SKR_POWER_N_RH_INST' if thishem=='North' else 'SKR_POWER_S_LH_INST'],
#                       imgdb.loc[arg,'UV_POWER'], marker='+',
#                       color=colors[sss], edgecolor='none', s=120, linewidth=0.5,
#                       label=plot_subinterv[sss])
    
            if np.size(valid) < 10:
                continue                
            opt, cov = sopt.curve_fit(linFit, xdata, ydata)
            xs = np.logspace(-20,-13)    
            ys = 10**opt[1]*xs**opt[0]
            ax.plot(xs,ys, c=colors[sss])
        ax.legend()
        set_grid(ax)
    axes[0].set_xscale('log')
    axes[0].set_yscale('log')
    if plot_subinterv[0]=='post-equinox':
        axes[0].set_xlim([1e-18, 5e-14])
        axes[0].set_ylim([4e9, 3e10])
    else:
        axes[0].set_xlim([1e-20, 5e-14])
        axes[0].set_ylim([1e9, 3e10])
    axes[0].set_xlabel('SKR power (W/m^2)')
    axes[0].set_ylabel('UV power (W/sr)')
    
    
    plt.subplots_adjust(wspace=0.1)
    plt.savefig('{}/skr_comp_{}.png'.format(savepath, plot_subinterv[0]), bbox_inches='tight', dpi=250)
    plt.savefig('{}/skr_comp_{}.pdf'.format(savepath, plot_subinterv[0]), bbox_inches='tight', dpi=250)
    plt.show()
    plt.close()

