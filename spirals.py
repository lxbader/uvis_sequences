import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
plotpath = pr.plotpath
uvispath = pr.uvispath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import os
import remove_solar_reflection
import skimage.filters
import uvisdb

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

udb = uvisdb.UVISDB()
imglist = udb.currentDF

rsr = remove_solar_reflection.RemoveSolarReflection()

savepath = '{}/spirals'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

selec = imglist[(imglist['YEAR']==2016) & (imglist['DOY']==303)]

#%%
for ctr in range(len(selec)):
    rsr.calculate('{}{}'.format(uvispath, selec['FILEPATH'].iloc[ctr]))
    image = rsr.redImage_aur
    colatbins = np.linspace(0, 30, num=image.shape[1]+1)
    colatcent = colatbins[:-1] + np.diff(colatbins)/2
    lonbins = np.linspace(0, 360, num=image.shape[0]+1)
    loncent = lonbins[:-1] + np.diff(lonbins)/2
    
    newlons = np.linspace(0, 360, num=37)
    newcolats = np.linspace(5, 25, num=41)
    
    arglon = np.digitize(loncent, newlons)-1
    argcolat = np.digitize(colatcent, newcolats)-1
    
    reduced = np.full((len(newlons)-1, len(newcolats)-1), np.nan)
    for iii in range(len(newlons)-1):
        for jjj in range(len(newcolats)-1):
            xval = np.where(arglon==iii)[0]
            yval = np.where(argcolat==jjj)[0]
            X, Y = np.meshgrid(xval, yval)
            reduced[iii,jjj] = np.nanmedian(image[X, Y])
    
    fig = plt.figure()
    fig.set_size_inches(9,6)
    ax = plt.subplot()
    reduced[reduced<0.1] = 0.1
    quad = ax.pcolormesh(newlons/180*12, newcolats, reduced.T, cmap=cmap_UV)
    KR_MIN=0.5
    KR_MAX=30
    quad.set_clim(0, KR_MAX)
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    ax.set_facecolor('gray')
    ax.set_xticks(np.arange(0, 25, 3))
    ax.set_yticks(np.arange(5, 26, 5))
    ax.set_xlabel('LT (hr)')
    ax.set_ylabel('Colatitude (deg)')
    plt.colorbar(quad, extend='both')
    plt.savefig('{}/{:03d}.png'.format(savepath, ctr), bbox_inches='tight', dpi=300)
    plt.close()

#%%
tmp = skimage.filters.try_all_threshold(reduced)
plt.pcolormesh(tmp.T)





















