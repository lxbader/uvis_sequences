import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cubehelix' % gitpath)
sys.path.insert(0, '%s/cassinipy' % gitpath)

from astropy.io import fits
import cassinipy
import cubehelix
from datetime import datetime, timedelta
import get_image
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.ndimage as ndi
import spiceypy as spice
import time_conversions as tc

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0, gamma=1.5)

smoothen = False

files = [r'{}\HST\2014-100 timetag\Projected\sat_14-100-03-44-20_stis_f25srf2_0{}_proj_nobg.fits'.format(datapath, iii)
            for iii in range(6)]

files = [r'{}\Alex_Cassini\HST_timetag\ProjectedData\sat_14-100-03-44-20_stis_f25srf2_0{}_proj_nobg.fits'.format(boxpath, iii)
            for iii in range(6)]

spice.kclear()
spice.furnsh('{}/SPICE/metakernels/cassini_generic.mk'.format(datapath))
cd = cassinipy.CassiniData(datapath)


samplepath = '{}hst_2014_100'.format(__file__.strip('sample_HST.py'))
if not os.path.exists(samplepath):
    os.makedirs(samplepath)
   
# get image data
origs = np.full((6,1440,120), np.nan)
times = []
for iii in range(6):
    hdulist = fits.open(files[iii])
    hdu = hdulist[0]
#    orig = np.flip(hdu.data[int(5/6*np.shape(hdu.data)[0]):np.shape(hdu.data)[0],:], axis=0).T
#    cml = hdu.header['CML']
#    origs[iii,:,:] = np.roll(orig, int(np.round((cml-180)*4)),axis=0)
    orig, _ , _ = get_image.getRedHST(files[iii], minangle=10)
    origs[iii,:,:] = orig
    fmt = '%Y-%m-%d %H:%M:%S'
    time = datetime.strptime(hdu.header['UDATE'], fmt)
    _, lt = spice.spkpos('SATURN', tc.datetime2et(time), 'IAU_EARTH', 'NONE', 'EARTH')
    time = time - timedelta(seconds=lt)
    times.append(time)
    
theta = np.linspace(0,2*np.pi,num=np.shape(origs)[1]+1)
r = np.linspace(0,30,num=np.shape(origs)[2]+1)
theta_cent = theta[:-1]+np.diff(theta)/2
r_cent = r[:-1]+np.diff(r)/2
x = np.full_like(origs[0,:,:], np.nan)
y = np.copy(x)
for iii in range(np.shape(origs)[1]):
    for jjj in range(np.shape(origs)[2]):
        x[iii,jjj] = r_cent[jjj]*np.cos(theta_cent[iii])
        y[iii,jjj] = r_cent[jjj]*np.sin(theta_cent[iii])
        
if smoothen:
    resize = 1
    distlim = 1
    theta_new = np.linspace(0,2*np.pi,num=int(np.shape(origs)[1]/resize)+1)
    r_new = np.linspace(0,30,num=int(np.shape(origs)[2]/resize)+1)
    theta_new_cent = theta_new[:-1]+np.diff(theta_new)/2
    r_new_cent = r_new[:-1]+np.diff(r_new)/2
    x_new = np.full((int(np.shape(origs)[1]/resize), int(np.shape(origs)[2]/resize)), np.nan)
    y_new = np.copy(x_new)
    for iii in range(np.shape(x_new)[0]):
        for jjj in range(np.shape(x_new)[1]):
            x_new[iii,jjj] = r_new_cent[jjj]*np.cos(theta_new_cent[iii])
            y_new[iii,jjj] = r_new_cent[jjj]*np.sin(theta_new_cent[iii])
    
    file = '{}/smooth_resize_{}_dist_{}.npz'.format(samplepath,int(resize), int(distlim))
    try:
        images = np.load(file)['images']
    except:
        images = np.full((np.shape(origs)[0], np.shape(x_new)[0], np.shape(x_new)[1]), np.nan)
        for iii in range(np.shape(images)[1]):
            if not (iii+1) % 10:
                print(iii+1)
            for jjj in range(np.shape(images)[2]):
                a = x-x_new[iii,jjj]
                b = y-y_new[iii,jjj]
                dist = np.sqrt(a**2+b**2)
                tmp = np.where(dist<distlim)
                images[:,iii,jjj] = np.nanmean(origs[:,tmp[0],tmp[1]], axis=1)
        for iii in range(6):
            tmp = np.where(x_new>15)
            images[iii, tmp[0], tmp[1]] = np.nan
        np.savez(file, images=images)
else:
    images = origs
    theta_new = theta
    r_new = r
    
panels = 'abcdefghijklmnopqrstuvwxyz'

#%%
    
fig = plt.figure()
fig.set_size_inches(15,16)

outer = gridspec.GridSpec(3, 1, height_ratios=(5,1.5,2), hspace=0.1)
gs1 = gridspec.GridSpecFromSubplotSpec(2, 4, width_ratios=(10,10,10,1),
                        wspace=0.15, hspace=0.1, subplot_spec=outer[0,0])
gs2 = gridspec.GridSpecFromSubplotSpec(2, 8, width_ratios=(1,1,1,1,1,1,0.1,0.1),
                                       height_ratios=(1,0.1),
                        wspace=0.2, subplot_spec=outer[1,0])
gs3 = gridspec.GridSpecFromSubplotSpec(2, 2, hspace=0.05, height_ratios=(9,10),
                        wspace=0.15, subplot_spec=outer[2,0])
smooths = np.array([])
selecs = np.array([])
areas = {}
# startimage, stopimage, mincolat, maxcolat, minLT, maxLT
areas['a'] = np.array([0,3,11.5,13,16,18])
areas['b'] = np.array([3,5,11,13,18,21])

acs = {}
acs['a'] = myred
acs['b'] = myblue

ltlat = {}
for lbl in [*areas]:
    ltlat[lbl] = np.full((6,2), np.nan)

for imgctr in range(6):
    lin = imgctr // 3
    col = imgctr % 3
    ax = plt.subplot(gs1[lin, col], projection='polar')            
    image = images[imgctr,:,:]
    image[np.where(image<=0)] = 0.00001
    
    # plot colormap and colorbar
    KR_MIN=1
    KR_MAX=50
    scale = 'log'
    quad = ax.pcolormesh(theta_new, r_new, image.T, cmap=cmap_UV)
    quad.set_clim(0, KR_MAX)
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    quad.cmap.set_under('k')
    ax.set_facecolor('gray')
    
    # plot Cassini footprint
    tmp = cd.get_ionfootp(tc.datetime2et(times[imgctr]))
    ax.scatter(tmp[3]/12*np.pi, tmp[1], marker='o', edgecolors=myred,
               linewidths=3,
               facecolors='none', s=40, zorder=30)
    
    if not imgctr:
        subax = plt.subplot(gs1[0:2,3])
        cbar = plt.colorbar(quad, pad=0.1, cax=subax, extend='both')
        cbar.set_label('Intensity (kR)', labelpad=15, fontsize=15, rotation=270)
        cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
                                           np.arange(1,10,1)),
                                 np.arange(10,101,10)))
        cbar.ax.tick_params(labelsize=14)
            
    ax.set_title(datetime.strftime(times[imgctr], fmt), fontsize=13, y=1.0)
    ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
    ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
    ticklabels = ['00','06','12','18']
    for iii in range(len(ticklabels)):
        txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                      ha='center',va='center')
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
    ax.set_xticklabels([])
    ax.tick_params(axis='x', which='major', pad=-20)
    ax.tick_params(axis='x', which='major', color='w')
    ax.set_yticks([10,20,30])
    ax.set_yticklabels([])
    ax.grid('on', color='0.8', linewidth=1.5)
    ax.set_theta_zero_location("N")
    ax.set_rmax(30)
    ax.set_rmin(0)
    ax.text(0,1,'({})'.format(panels[imgctr]), ha='left', va='top',
            fontsize=15, fontweight='bold',
            transform=ax.transAxes)
 
    ax = plt.subplot(gs2[0,imgctr])
#    smooth = ndi.convolve(images[imgctr], np.full((81,11), 1.0/81/11))
#    smooth = ndi.median_filter(images[imgctr], footprint=np.ones((81,11)))
#    smooth = images[imgctr]
    smooth = ndi.gaussian_filter(images[imgctr], sigma=(20,2.5))
    if not np.any(smooths):
        smooths = np.expand_dims(smooth, axis=0)
    else:
        smooths = np.concatenate([smooths, np.expand_dims(smooth, axis=0)], axis=0)
    quad = ax.pcolormesh(theta_new/np.pi*12, r_new, smooth.T, cmap=cmap_UV)
    quad.set_clim(5,45)
    
    ax.scatter(tmp[3], tmp[1], marker='o', edgecolors=myred,
               linewidths=3,
               facecolors='none', s=40, zorder=30)
    
    ax.set_facecolor('gray')
    ax.set_title(datetime.strftime(times[imgctr], '%H:%M:%S'), fontsize=10)
    ax.set_xlim([14,22])
    ax.set_ylim([7,15]) 
    ax.set_xticks([15,18,21])
    ax.set_yticks(np.arange(7,16,2))
    if imgctr in [1,2,3,4]:
        ax.set_yticklabels([])
    if imgctr==5:
        ax.yaxis.tick_right()
    ax.set_xlabel('LT (h)')
    if imgctr==0:
        ax.set_ylabel('Colatitude (deg)')
    elif imgctr==5:
        ax.set_ylabel('Colatitude (deg)', labelpad=15, rotation=270)
        ax.yaxis.set_label_position('right')
#    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    ax.grid(which='major', color='w', linestyle='--', linewidth=0.5, alpha=0.6)
    ax.text(0.03,0.96,'({})'.format(panels[imgctr+6]), ha='left', va='top',
            fontsize=15, fontweight='bold', color='w',
            transform=ax.transAxes)
    
    if not imgctr:
        subax = plt.subplot(gs2[0,-1])
        cbar = plt.colorbar(quad, pad=0.1, cax=subax, extend='both')
        cbar.set_label('Intensity (kR)', labelpad=15, rotation=270)
        cbar.set_ticks(np.arange(5,56,10))
    
    a = np.where(theta_new[:-1]>=np.pi)[0]
    b = np.where((r_new[:-1]>=5) & (r_new[:-1]<17.5))[0]
    A, B = np.meshgrid(a,b)
    selec = smooth[A, B]
    if not np.any(selecs):
        selecs = np.expand_dims(selec, axis=0)
    else:
        selecs = np.concatenate([selecs, np.expand_dims(selec, axis=0)], axis=0)
    
    selec_max = np.unravel_index(np.nanargmax(selec),np.shape(selec))
    latmax = r_new[b[selec_max[0]]] + np.diff(r_new)[0]/2
    lonmax = theta_new[a[selec_max[1]]] + np.diff(theta_new)[0]/2
    
#    latmax = r_new[b[np.argmax(np.nanmean(selec, axis=1))]] + np.diff(r_new)[0]/2
#    lonmax = theta_new[a[np.argmax(np.nanmean(selec, axis=0))]] + np.diff(theta_new)[0]/2
    ltmax = lonmax/np.pi*12
#    ax.axhline(latmax, color='r')
#    ax.axvline(ltmax, color='b')
    
    for lbl in [*areas]:
        area = areas[lbl]
        if (imgctr<area[0]) | (imgctr>area[1]):
            continue
        a = np.where((theta_new[:-1]>=area[4]/12*np.pi) & (theta_new[:-1]<area[5]/12*np.pi))[0]
        b = np.where((r_new[:-1]>=area[2]) & (r_new[:-1]<area[3]))[0]
        A, B = np.meshgrid(a,b)
        selec = smooth[A, B]
        selec_max = np.unravel_index(np.nanargmax(selec),np.shape(selec))
        latmax = r_new[b[selec_max[0]]] + np.diff(r_new)[0]/2
        lonmax = theta_new[a[selec_max[1]]] + np.diff(theta_new)[0]/2
        ltmax = lonmax/np.pi*12
        ax.axhline(latmax, color=acs[lbl], linestyle='--', linewidth=1, zorder=5)
        ax.axhline(latmax, color='w', linestyle='-', linewidth=2, zorder=3, alpha=0.6)
        ax.axvline(ltmax, color=acs[lbl], linestyle='--', linewidth=1, zorder=5)
        ax.axvline(ltmax, color='w', linestyle='-', linewidth=2, zorder=3, alpha=0.6)
        ltlat[lbl][imgctr,:] = np.array([ltmax, latmax])

# corotation / latitude plots
seconds_after_start = np.arange(70,840,140)
ax_top = plt.subplot(gs3[0,0])
ax_bottom = plt.subplot(gs3[1,0], sharex=ax_top)

#fig, [ax_top, ax_bottom] = plt.subplots(2,1, sharex=True)
for ax in [ax_top, ax_bottom]:
    for lbl in [*ltlat]:
        data = ltlat[lbl][:,0]
        ax.plot(seconds_after_start, data,
                    color='k', linestyle='--',
                    marker='x', 
                    zorder=1,
                    label='Data' if lbl=='a' else None)
        ax.errorbar(seconds_after_start, data,
                    xerr=0, yerr=1/3,
                    color='k', linestyle='',
                    ecolor='k', elinewidth=1, 
                    capsize=2,
                    zorder=0)
        ax.scatter(seconds_after_start, data,
                   facecolor=acs[lbl], marker='X',
                   edgecolor='k', zorder=5, s=80, linewidth=1.5)
        valid = np.where(np.isfinite(data))[0]
        corot = np.zeros((len(valid)))
        corot_t = np.arange(seconds_after_start[valid[0]]-30,
                            seconds_after_start[valid[-1]]+35,10)
        corot = data[valid[0]]+(corot_t-seconds_after_start[valid[0]])*24/(10.66*3600)
        ax.plot(corot_t, corot,
                color='dimgrey', linestyle='-.', linewidth=1, zorder=0,
                label='Rigid corotation' if lbl=='a' else None)
    ax.grid()
ax_bottom.set_xlabel('Time after sequence start (s)')
ax_bottom.set_xlim([0,840])
ax_bottom.set_ylabel('LT (h)', y=1.1)
ax_bottom.legend(loc=4)
ax_bottom.set_ylim([16.75,18.0])
ax_top.set_ylim([18.95,20.05])
ax_bottom.set_yticks(np.arange(16.8,18.0,0.2))
ax_top.set_yticks(np.arange(19.0,20.0,0.2))
ax_top.spines['bottom'].set_visible(False)
ax_bottom.spines['top'].set_visible(False)
#ax_top.xaxis.tick_top()
ax_top.tick_params(axis='x', which='both', top=False, bottom=False, labelbottom=False) 
ax_bottom.xaxis.tick_bottom()
ax_top.text(0.02,0.94,'({})'.format(panels[2*imgctr+2]), ha='left', va='top',
            fontsize=15, fontweight='bold', color='k',
            transform=ax_top.transAxes)

d = .015  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
kwargs = dict(transform=ax_top.transAxes, color='k', clip_on=False)
ax_top.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
ax_top.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

kwargs.update(transform=ax_bottom.transAxes)  # switch to the bottom axes
ax_bottom.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
ax_bottom.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal



ax = plt.subplot(gs3[:,1])
for lbl in [*ltlat]:
    data = ltlat[lbl][:,1]
    ax.plot(seconds_after_start, data, color='k', linestyle='--',
            marker='x', zorder=1)
    ax.errorbar(seconds_after_start, data,
                xerr=0, yerr=1,
                color='k', linestyle='',
                ecolor='k', elinewidth=1, 
                capsize=2,
                zorder=0)
    ax.scatter(seconds_after_start, data,
               facecolor=acs[lbl], marker='X',
               edgecolor='k', zorder=5, s=80, linewidth=1.5)
ax.grid()
ax.set_xlabel('Time after sequence start (s)')
ax.set_ylabel('Colatitude (deg)')
ax.text(0.02,0.97,'({})'.format(panels[2*imgctr+3]), ha='left', va='top',
            fontsize=15, fontweight='bold', color='k',
            transform=ax.transAxes)

plt.savefig('{}/2014_100_HST_tile.png'.format(samplepath),bbox_inches='tight', dpi=300)
plt.show() 
plt.close()

  