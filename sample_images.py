# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
from datetime import datetime
import get_image
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import remove_solar_reflection
import time_conversions as tc
#import uvisdb

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

#udb = uvisdb.UVISDB(update=False)
#imglist = udb.currentDF

import pandas as pd
picklefile = '{}/imglists/UVIS_current_pickle - Copy'.format(boxpath)
imglist = pd.read_pickle(picklefile)

rsr = remove_solar_reflection.RemoveSolarReflection()

dtmin = datetime(2014,5,25,12,0,0)
dtmax = datetime(2014,5,25,16,30,0)
cond1 = imglist['ET_START'] > tc.datetime2et(dtmin)
cond2 = imglist['ET_START'] < tc.datetime2et(dtmax)
selec_UVIS = imglist[cond1 & cond2]
#filelist = np.array(['{}{}'.format(uvispath, selec_UVIS.loc[iii, 'FILEPATH']) for iii in selec_UVIS.index])

samplepath = '{}data_sample'.format(__file__.strip('sample_images.py'))
if not os.path.exists(samplepath):
    os.makedirs(samplepath)

panels = 'abcdefghijklmnopqrstuvwxyz'
arrows = np.array([[1,240,27],
                   [5,240,27],
                   [11,280,27],
                   [15,280,27],
                   [21,280,27]])

for side in [2,3,5]:
    fig = plt.figure()
    if side == 5:
        fig.set_size_inches(16,20)
        gs = gridspec.GridSpec(6, 5, height_ratios=(1,1,1,1,1,0.1),
                               left=0.03, right=0.97,
                               bottom=0.05, top=0.98,
                               wspace=0.05, hspace=0.02)
    elif side == 3:
        fig.set_size_inches(16,20)
        gs = gridspec.GridSpec(4, 3, height_ratios=(1,1,1,0.1),
                               left=0.03, right=0.97,
                               bottom=0.05, top=0.98,
                               wspace=0.05, hspace=0.02)
    elif side == 2:
        fig.set_size_inches(9,12)
        gs = gridspec.GridSpec(3, 2, height_ratios=(1,1,0.1),
                               left=0.03, right=0.97,
                               bottom=0.1, top=0.98,
                               wspace=0.05, hspace=0.02)
    for imgctr in range(side**2):
        
        thisimg = selec_UVIS.iloc[imgctr]
        lin = imgctr // side
        col = imgctr % side
        
        ax = plt.subplot(gs[lin, col], projection='polar')
        
        # get image data
        samplefile = '{}{}'.format(uvispath, thisimg['FILEPATH'])
        image, angles, hemsph = get_image.getRedUVIS(samplefile, minangle=0)
        rsr.calculate(samplefile)
        image = rsr.redImage_aur
        image[np.where(image<=0)] = 0.00001
        theta = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1,endpoint=True)
        r = np.linspace(0,30,num=np.shape(image)[1]+1,endpoint=True)
    
        # plot colormap and colorbar
        KR_MIN=1
        KR_MAX=25
        scale = 'log'
        quad = ax.pcolormesh(theta, r, image.T, cmap=cmap_UV)
        quad.set_clim(0, KR_MAX)
        quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
        ax.set_facecolor('gray')
        
        tmp = np.where(imgctr==arrows[:,0])[0]
        if np.size(tmp):
            for iii in tmp:
                ax.arrow(arrows[iii,1]/180*np.pi, arrows[iii,2], 0, -8,
                         width=0.08, edgecolor='black', head_length=4,
                         head_width=0.22, overhang=0.2,
                         length_includes_head=True,
                         facecolor='gold', lw=1.5, zorder=5)
            
        if imgctr==0:
            if side == 5:
                subax = plt.subplot(gs[5,1:4])
            elif side == 3:
                subax = plt.subplot(gs[3,:])
            elif side == 2:
                subax = plt.subplot(gs[2,:])
            cbar = plt.colorbar(quad, cax=subax, extend='both',
                                orientation='horizontal')
            cbar.set_label('Intensity (kR)', labelpad=10, fontsize=15)
            cbar.set_ticks(np.append(np.append(np.linspace(0.1,0.9,num=9),
                                               np.linspace(1,9,num=9)),
                                     np.linspace(10,90,num=9)))
            cbar.ax.tick_params(labelsize=14)
        # PPO phase angle
        ppo_n = thisimg['PPO_PHASE_N']
        ppo_s = thisimg['PPO_PHASE_S']
        if np.isfinite(ppo_n):
            ax.plot([ppo_n,ppo_n], [0,30], color=myred, lw=2, zorder=5)
            ax.plot([ppo_n,ppo_n], [0,30], color='w', lw=3, zorder=3)
            txt = ax.text(ppo_n+0.12, 26.5, 'N', color='w', fontsize=14, fontweight='bold',
                          ha='center',va='center')
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground=myred)])
        if np.isfinite(ppo_s):
            ax.plot([ppo_s,ppo_s], [0,30], color=myblue, lw=2, zorder=5)
            ax.plot([ppo_s,ppo_s], [0,30], color='w', lw=3, zorder=3)
            txt = ax.text(ppo_s+0.12, 26.5, 'S', color='w', fontsize=14, fontweight='bold',
                          ha='center',va='center')
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground=myblue)])
            
        tmp = tc.et2datetime(thisimg['ET_START'])
        timestr = datetime.strftime(tmp, '%Y-%m-%d (DOY %j) %H:%M:%S')
        ax.set_title('{}\n${}\,\mathrm{{s}}$'.format(timestr, int(thisimg['EXP'])),
                     fontsize=10)
        ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
        ax.set_xticklabels([])
        ax.set_yticks([10,20,30])
        ax.set_yticklabels([])
        ax.grid('on', color='0.8', linewidth=1.5)
        ax.set_theta_zero_location("N")
        ax.set_rmax(30)
        ax.set_rmin(0)
        
        ax.text(0,0.95,'({})'.format(panels[imgctr]),transform=ax.transAxes,fontsize=15,fontweight='bold')
        
    fs = 25
    plt.text(0.5, 1, '0 LT', rotation=0, fontsize=fs, fontweight='bold',
             transform=fig.transFigure, ha='center', va='center')
    plt.text(0, 0.5, '6 LT', rotation=90, fontsize=fs, fontweight='bold',
             transform=fig.transFigure, ha='center', va='center')
    plt.text(0.5, 0, '12 LT', rotation=0, fontsize=fs, fontweight='bold',
             transform=fig.transFigure, ha='center', va='center')
    plt.text(1, 0.5, '18 LT', rotation=270, fontsize=fs, fontweight='bold',
             transform=fig.transFigure, ha='center', va='center')
        
    
    #=======
    # Saving
    plt.savefig('{}/2014_145_tile_{}.png'.format(samplepath, side),bbox_inches='tight', dpi=300)
#    plt.savefig('{}/2014_145_tile_{}.pdf'.format(samplepath, side),bbox_inches='tight')
    plt.close()

  