# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cassinipy
from datetime import datetime, timedelta
import glob
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import os
import read_crossing_list as rcl
import scipy.optimize as opt
import sys
import time_conversions as tc
import uvisdb

savepath = '{}/SW_2014'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

myblue='royalblue'
myred='crimson'

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF

cd = cassinipy.CassiniData(datapath)
cd.set_refframe('KSM')

cl = rcl.CrossingList()

# get Cassini solar wind periods    
swmark = np.zeros((0,2))
region_df = cl.get_regions(tc.datetime2et([datetime(2014,1,1), datetime(2015,1,1)]))
for iii in region_df.index:
    if not region_df.loc[iii, 'REGION']=='SW':
        continue
    swmark = np.append(swmark, [tc.et2datetime(region_df.loc[iii,['ETSTART','ETSTOP']].as_matrix())], axis=0)
    
# get Cassini MP crossing times and position
crossing_df = cl.get_crossings(tc.datetime2et([datetime(2014,1,1), datetime(2015,1,1)]))
mp_cross_times = crossing_df['ETTIME']
mp_cross_ksm = cd.get_locations(mp_cross_times)[1:]
mp_cross_ksm = np.array([mp_cross_ksm[0],mp_cross_ksm[1],mp_cross_ksm[2]])
    
# Magnetopause model
# Input: Dp (dynamic pressure in nPa)
# Output: X_KSM, R_KSM in R_S
def mploc(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    theta = np.arange(0,170,0.1)/180*np.pi
    radius = r0*(2/(1+np.cos(theta)))**K
    [x,r] = radius*[np.cos(theta), np.sin(theta)]
    return [x,r]

def mp_standoff(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    radius = r0*(2/(1+np.cos(0)))**K
    return radius

def mpdist_theta_radius(Dp, theta_sc, radius_sc):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    radius = r0*(2/(1+np.cos(theta_sc)))**K
    return radius-radius_sc

def find_mp_standoff(xyz):
    x_pos = xyz[0]
    r_pos = np.sqrt(xyz[1]**2+xyz[2]**2)
    theta = np.arctan2(r_pos, x_pos)
    dp_opt = opt.brentq(mpdist_theta_radius, 0.0001, 1, args=(theta, np.sqrt(x_pos**2+r_pos**2)))
    return dp_opt

mp_cross_standoff = mp_standoff(
        np.array([find_mp_standoff(mp_cross_ksm[:,iii]) for iii in range(np.shape(mp_cross_ksm)[1])]))

# get Tao SW model data
swpath = '{}/SW propagation/Tao_2014.txt'.format(boxpath)
swtimes = np.genfromtxt(swpath, comments='#', usecols=(0), dtype='U23')
swtimes = np.array([datetime.strptime(iii, '%Y-%m-%dT%H:%M:%S.%f') for iii in swtimes])
swpdyn = np.genfromtxt(swpath, comments='#', usecols=(4), dtype=np.float)
swvtan = np.genfromtxt(swpath, comments='#', usecols=(6), dtype=np.float)
# model MP standoff distance
mp_dist = mp_standoff(swpdyn)

# define timeframe
fmt = '%Y-%m-%d %H:%M:%S'
start = '2014-01-01 00:00:00'
stop = '2014-12-31 00:00:00'
dtstart = datetime.strptime(start, fmt)
dtstop = datetime.strptime(stop, fmt)
etstart = tc.datetime2et(dtstart)
etstop = tc.datetime2et(dtstop)

# get sc distance from Saturn
scdist_times = np.linspace(etstart, etstop, num=10000)
scdist = cd.get_locations(scdist_times, refframe='KSM')
scdist = np.concatenate([np.array(scdist[1:])], axis=1)
sc_standoff = mp_standoff(
        np.array([find_mp_standoff(scdist[:,iii]) for iii in range(np.shape(scdist)[1])]))

cd.set_timeframe([etstart, etstop])
cmag = cd.get_MAG(res='1M')
magdata = cmag.data.T
mag_tot = np.linalg.norm(magdata, axis=0)
times = tc.et2datetime(cmag.ettimes)

# check for magdata in SW
tmp_times = np.repeat([times], np.shape(swmark)[0], axis=0).T
tmp_swlim = np.repeat([swmark], len(times), axis=0)
tmp = ((tmp_times>tmp_swlim[:,:,0]+timedelta(minutes=2)) &
       (tmp_times<tmp_swlim[:,:,1]-timedelta(minutes=2)))
valid = np.sum(tmp, axis=1)

# only MAG data from the SW
tmp = np.where(valid==1)[0]
magdata = magdata[:,tmp]
mag_tot = mag_tot[tmp]
times = times[tmp]

# insert nan for data gaps
tmp = np.where(np.diff(times)>timedelta(seconds=120))[0]
magdata = np.insert(magdata, tmp+1, np.full((len(tmp),3), np.nan).T, axis=1)
mag_tot = np.insert(mag_tot, tmp+1, np.full((len(tmp)), np.nan))
times = np.insert(times, tmp+1, times[tmp+1]+timedelta(seconds=1))

NUM_subplots = 6
sp_iter = iter(range(NUM_subplots))
gs = gridspec.GridSpec(NUM_subplots,1, hspace=0.1)

fig = plt.figure()
fig.set_size_inches(18,12)

thissp = next(sp_iter)
ax_tot = plt.subplot(gs[thissp, 0])
ax_tot.scatter(times, mag_tot, marker='.', s=0.5, color='k', lw=0.5)
ax_tot.grid()
ax_tot.set_ylabel(r'|B| (nT)')

thissp = next(sp_iter)
ax_xyz = plt.subplot(gs[thissp, 0], sharex=ax_tot)
ax_xyz.scatter(times, magdata[0,:], marker='.', s=0.5,
               color='k', label='$B_x$')
ax_xyz.scatter(times, magdata[1,:], marker='.', s=0.5,
               color=myred, label='$B_y$')
ax_xyz.scatter(times, magdata[2,:], marker='.', s=0.5,
               color=myblue, label='$B_z$')
ax_xyz.grid()
ax_xyz.set_ylabel(r'B (nT)')

thissp = next(sp_iter)
ax_imf = plt.subplot(gs[thissp, 0], sharex=ax_tot)
ax_imf.scatter(times,
               np.degrees(np.arctan2(magdata[1,:],magdata[2,:]))%360,
               marker='.',
               color='k', s=0.5)
ax_imf.set_yticks([0,90,180,270,360])
ax_imf.set_ylim([0,360])
ax_imf.grid()
ax_imf.set_ylabel(r'IMF clock angle (deg)')

thissp = next(sp_iter)
ax_el = plt.subplot(gs[thissp, 0], sharex=ax_tot)
# electron density
edensdata = cd.crpws
edensdata.setETlim([etstart,etstop])
edensdata.readDensities()
# only SW values
tmp_times = np.repeat([tc.et2datetime(edensdata.dens_e_et)],
                       np.shape(swmark)[0], axis=0).T
tmp_swlim = np.repeat([swmark], len(edensdata.dens_e_et), axis=0)
tmp = ((tmp_times>tmp_swlim[:,:,0]+timedelta(minutes=2)) &
       (tmp_times<tmp_swlim[:,:,1]-timedelta(minutes=2)))
valid = np.sum(tmp, axis=1)
data = edensdata.dens_e_val.as_matrix()
data[np.where(valid==0)] = np.nan
ax_el.scatter(tc.et2datetime(edensdata.dens_e_et),
              data,
              marker='.', c='k', s=0.5)
ax_el.set_yscale('log')
ax_el.set_ylim([1e-3,1e1])
ax_el.grid()
ax_el.set_ylabel('e$^-$ density (cm$^{-3}$)')

thissp = next(sp_iter)
ax_swsp = plt.subplot(gs[thissp, 0], sharex=ax_tot)
ax_swsp.scatter(swtimes,
                swvtan,
                marker='.', c='k', s=0.5)
ax_swsp.set_ylim([250,700])
ax_swsp.grid()
ax_swsp.set_ylabel('SW speed (km/s)')

thissp = next(sp_iter)
ax_mp = plt.subplot(gs[thissp, 0], sharex=ax_tot)
ax_mp.scatter(swtimes,
              mp_dist,
              marker='.', c='k', s=0.5)
ax_mp.scatter(tc.et2datetime(mp_cross_times),
              mp_cross_standoff,
              marker='.', c=myred, s=10)
ax_mp.plot(tc.et2datetime(scdist_times),
           sc_standoff, '-', c=myred, lw=0.5)
ax_mp.set_ylim([10,60])
ax_mp.grid()
ax_mp.set_ylabel('MP standoff dist (R$_\mathrm{S}$)')


axs = [ax_tot, ax_xyz, ax_imf, ax_el, ax_swsp, ax_mp]
for ax in axs:
    if ax == ax_mp:
        continue
    plt.setp(ax.get_xticklabels(), visible=False)

# mark SW periods
for sss in range(np.shape(swmark)[0]):
    for ax in axs:
        ax.axvspan(swmark[sss,0], swmark[sss,1], alpha=0.2, color='grey',
                   edgecolor=None)

# mark UVIS periods
imglist_selec = imglist[imglist['YEAR']==2014]
indexlist = imglist_selec.index
uvismark = np.zeros((0,2))
start = -1
for iii in range(len(indexlist)):
    if start == -1:
        start = imglist.loc[indexlist[iii],'ET_START']
    try:
        tmp1 = imglist.loc[indexlist[iii+1],'ET_START']
        tmp2 = imglist.loc[indexlist[iii],'ET_STOP']
        if tmp1-tmp2 < 3600:
            continue
    except:
        pass
    stop = imglist.loc[indexlist[iii],'ET_STOP']
    uvismark = np.append(uvismark,
                         [[tc.et2datetime(start),
                         tc.et2datetime(stop)]], axis=0)
    start = -1
        
for iii in range(np.shape(uvismark)[0]):
    for ax in axs:
        ax.axvspan(uvismark[iii,0],
                   uvismark[iii,1],
                   alpha=0.2, color=myred, edgecolor=None)
        
# mark HST images
hstpath = r'E:\data\HST\2014\all'
hsttimes = np.zeros((0,2))
hstfiles = glob.glob('{}/*.fits'.format(hstpath))
for iii in range(len(hstfiles)):
    hdulist = fits.open(hstfiles[iii])
    datestr = hdulist[0].header['UDATE']
    fmt = '%Y-%m-%d %H:%M:%S'
    time = datetime.strptime(datestr, fmt)
    lighttime = hdulist[0].header['LIGHTIME']
    exp = hdulist[0].header['EXPT']
    start = time-timedelta(seconds=lighttime)
    stop = start+timedelta(seconds=exp)
    hsttimes = np.append(hsttimes, [[start, stop]], axis=0)
    
hstmark = np.zeros((0,2))
start = -1
for iii in range(np.shape(hsttimes)[0]):
    if start == -1:
        start = hsttimes[iii,0]
    try:
        tmp1 = hsttimes[iii+1,0]
        tmp2 = hsttimes[iii,1]
        if tmp1-tmp2 < timedelta(hours=1):
            continue
    except:
        pass
    stop = hsttimes[iii,1]
    hstmark = np.append(hstmark,
                         [[start,
                         stop]], axis=0)
    start = -1
        
for iii in range(np.shape(hstmark)[0]):
    for ax in axs:
        ax.axvspan(hstmark[iii,0],
                   hstmark[iii,1],
                   alpha=0.2, color=myblue, edgecolor=None)
    
ax_tot.set_xlim([datetime(2014,1,1), datetime(2014,12,31)])
if not os.path.exists(savepath):
    os.makedirs(savepath)
plt.savefig('{}/2014_full'.format(savepath), dpi=300)
    
for mmm in range(1,10):
    ax_tot.set_xlim([datetime(2014,mmm,1), datetime(2014,mmm,1)+timedelta(days=95)])
    fullsavepath = '{}/3month'.format(savepath)
    if not os.path.exists(fullsavepath):
        os.makedirs(fullsavepath)
    plt.savefig('{}/2014_3month_{}'.format(fullsavepath,mmm), dpi=300)
        
for mmm in range(1,12):
    for ddd in [1,11,21]:
        ax_tot.set_xlim([datetime(2014,mmm,ddd), datetime(2014,mmm+1,ddd)])
        fullsavepath = '{}/month'.format(savepath)
        if not os.path.exists(fullsavepath):
            os.makedirs(fullsavepath)
        plt.savefig('{}/2014_month_{}_{}'.format(fullsavepath,mmm,ddd), dpi=300)
        
for mmm in range(1,12):
    for ddd in [1,6,11,16,21,26]:
        ax_tot.set_xlim([datetime(2014,mmm,ddd), datetime(2014,mmm,ddd)+timedelta(days=16)])
        fullsavepath = '{}/16days'.format(savepath)
        if not os.path.exists(fullsavepath):
            os.makedirs(fullsavepath)
        plt.savefig('{}/2014_16days_{}_{}'.format(fullsavepath,mmm,ddd), dpi=300)

plt.close()