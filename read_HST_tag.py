import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cubehelix
import get_image
import matplotlib.colors as mcolors
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sgn
import scipy.ndimage as ndi

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0, gamma=1.5)

ttfile = '{}/HST/2014-100 timetag/occgk3riq_tag.fits'.format(datapath)
projfile = '{}/HST/2014/all/sat_14-100-03-44-20_stis_f25srf2_sm_proj_nobg.fits'.format(datapath)
projfile = '{}\Alex_Cassini\HST_timetag\ProjectedData\sat_14-100-03-44-20_stis_f25srf2_04_proj_nobg.fits'.format(boxpath)

# =============================================================================
# Original file
# =============================================================================
tmp = fits.open(ttfile)

file = tmp[1]
btime = file.data.field('TIME')
bx = file.data.field('AXIS1')-1
by = file.data.field('AXIS2')-1

N = 6
img = np.zeros((N,2048,2048))
dt = np.max(btime)/N+0.1

for iii in range(len(btime)):
    nimg = int(np.floor(btime[iii]/dt))
    img[nimg, bx[iii], by[iii]] += 1

selec = np.copy(img[:, 250:750, 400:900])
for iii in range(N):
#    selec[iii,:,:] = sgn.medfilt(selec[iii,:,:])
    selec[iii,:,:] = ndi.convolve(selec[iii,:,:], np.full((9,9), 1.0/81))
    
#fig, axx = plt.subplots(2, int(np.ceil(N/2)))
#axx = np.ravel(axx)
#for iii in range(N):
#    axx[iii].imshow(selec[iii], cmap='magma', vmin=0, vmax=np.max(selec))
#
#plt.show()

#full = np.sum(img, axis=0)
full = img[3]
full_sm = ndi.convolve(full, np.full((9,9), 1.0/81))
plt.imshow(full_sm[400:670, 580:880])
plt.show()

bg = np.copy(full[600:650, 600:700])
full_sm[600:650, 600:700] *= 3
plt.imshow(full_sm[400:670, 580:880])
plt.show()

au = np.copy(full[400:670, 580:880])
val = np.ones_like(au)
val[np.where(full_sm[400:670, 580:880] < np.mean(bg)-np.std(bg)/4)] = 0
plt.imshow(val)
plt.show()

au[val==0] = np.nan
plt.imshow(au)
plt.show()

signal = np.nansum(au-np.mean(bg))
exp = np.max(btime)/6
cps = signal/exp
watts = cps*9*1e-10* (9.0544945*1.496e8)**2
gwatts = watts/1e9

# =============================================================================
# Projected file
# =============================================================================

image, angles, _ = get_image.getRedHST(projfile, minangle=10)
#angles[angles>90] = np.nan
#image[np.where(image<=0)] = 0

try:
    sections = False
    MAX_COLAT = 18
    # calculate surface of each lon-lat bin
    # using a spherical shape of Saturn, not an ellipsoid
    lons = np.linspace(0, 2*np.pi, num=np.shape(image)[0]+1)
    maxbin = int(MAX_COLAT/30*np.shape(image)[1])
    lats = np.linspace(0, MAX_COLAT/180*np.pi, num=maxbin+1)
    sqmsize = np.zeros_like(image[:,:maxbin])
    lon_binsize = np.mean(np.diff(lons))
    for iii in range(np.shape(sqmsize)[1]):
        # dS = r**2*dlon*(cosphi1-cosphi2)
        sqmsize[:,iii] = 58232e3**2*lon_binsize*np.abs(np.cos(lats[iii])-np.cos(lats[iii+1]))
        
    
    # take only section between 0-MAX_COLAT deg colatitude
    # multiply by sin(Cassini elevation angle) to get correct intensity values
    cut = image[:,:maxbin]
    # drop if too many nans
    if np.sum(np.isnan(cut))>np.size(cut)/3:
        raise Exception
    
    # if bins without value
    if np.sum(np.isnan(cut)):
        # average over longitude
        lonmean = np.repeat([np.nanmean(cut, axis=0)], np.shape(cut)[0], axis=0)
        # fill empty pixels with average values
        tmp = np.where(np.isnan(cut))
        cut[tmp] = lonmean[tmp]
        # if still empty pixels
        if np.sum(np.isnan(cut)):
            raise Exception
    # convert kR to photon radiance L [photons/m^2/s/sr]
    L = cut*1e3/(4*np.pi*1e-10)
    # [J/m^2/s/sr] (Kurth 2016)
    L *= 1.6e-18
    # [J/s/sr] = [W/sr]
    L *= sqmsize*np.sin(np.radians(angles[:,:maxbin]))
    # [W]
    L *= 4*np.pi
    # integrate
    if not sections:
        gwatts_proj = np.sum(L)/1e9
    else:
        # bins per degree longitude
        bpd = int(np.shape(L)[0]/360)
        bins = (np.linspace(0,360,num=37)*bpd).astype(int)
        sect_power = np.array([np.sum(L[bins[iii]:bins[iii+1]]) for iii in range(len(bins)-1)])
        gwatts_proj = sect_power/1e9
except:
    print('Error')
                
image, angles, _ = get_image.getRedHST(projfile, minangle=10)
angles[angles<0] = np.nan
image[np.where(image<=0)] = 0.00001

theta = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1)
r = np.linspace(0,30,num=np.shape(image)[1]+1)

fig = plt.figure()
fig.set_size_inches(8,8)
ax = fig.add_subplot(111, projection='polar')
# plot colormap and colorbar
KR_MIN=1
KR_MAX=50
scale = 'log'
quad = ax.pcolormesh(theta, r, image.T, cmap=cmap_UV)
quad.set_clim(0, KR_MAX)
quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
quad.cmap.set_under('k')
ax.set_facecolor('gray')

cbar = plt.colorbar(quad, pad=0.1, ax=ax, extend='both')
cbar.set_label('Intensity (kR)', labelpad=15, fontsize=15, rotation=270)
cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
                                   np.arange(1,10,1)),
                         np.arange(10,101,10)))
cbar.ax.tick_params(labelsize=14)
        
ax.set_title(projfile.split('/')[-1].split('\\')[-1], fontsize=13, y=1.0)
ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                  ha='center',va='center')
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax.set_xticklabels([])
ax.tick_params(axis='x', which='major', pad=-20)
ax.tick_params(axis='x', which='major', color='w')
ax.set_yticks([10,20,30])
ax.set_yticklabels([])
ax.grid('on', color='0.8', linewidth=1.5)
ax.set_theta_zero_location("N")
ax.set_rmax(30)
ax.set_rmin(0)
plt.show()


fig = plt.figure()
fig.set_size_inches(8,8)
ax = fig.add_subplot(111, projection='polar')
quad = ax.pcolormesh(theta, r, angles.T, cmap=cmap_UV)
quad.set_clim(0, 90)
ax.set_facecolor('gray')

cbar = plt.colorbar(quad, pad=0.1, ax=ax)
cbar.set_label('Elevation angle (deg)', labelpad=15, fontsize=15, rotation=270)
cbar.set_ticks(np.arange(0,100,10))
cbar.ax.tick_params(labelsize=14)
        
ax.set_title(projfile.split('/')[-1].split('\\')[-1], fontsize=13, y=1.0)
ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                  ha='center',va='center')
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax.set_xticklabels([])
ax.tick_params(axis='x', which='major', pad=-20)
ax.tick_params(axis='x', which='major', color='w')
ax.set_yticks([10,20,30])
ax.set_yticklabels([])
ax.grid('on', color='0.8', linewidth=1.5)
ax.set_theta_zero_location("N")
ax.set_rmax(30)
ax.set_rmin(0)
plt.show()

print('Unprojected power: ', gwatts, ' GW')
print('Projected power: ', gwatts_proj, ' GW')

# =============================================================================
# Conversion from Gustin Tbl1 to Tbl2
# =============================================================================
cps = 1
kR = cps*3948
photons = kR*1e3*1e10
SI = 1.6e-18*photons
pixelangle = np.radians(25/1024/60/60)
a_px = np.sin(pixelangle)**2*1e6
#print('Pixel height on Saturn: ', np.sqrt(a_px)/1e3, 'km')
print(SI*a_px)


fct = (4*np.pi*1.6*1e-18)/a_px/1e6
print(fct*3948)

img, ang, _ = get_image.getRedUVIS(r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0021\2007_350T23_33_28.fits')
