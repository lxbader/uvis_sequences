import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cubehelix' % gitpath)
sys.path.insert(0, '%s/cassinipy' % gitpath)

from astropy.io import fits
import cassinipy
import cubehelix
from datetime import datetime, timedelta
import get_image
import glob
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import spiceypy as spice
import time_conversions as tc

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0, gamma=1.5)

files = glob.glob(r'{}\HST\2014-100 selec\*.fits'.format(datapath))

spice.kclear()
spice.furnsh('{}/SPICE/metakernels/cassini_generic.mk'.format(datapath))
cd = cassinipy.CassiniData(datapath)

samplepath = '{}hst_2014_100'.format(__file__.strip('sample_HST_2014_100.py'))
if not os.path.exists(samplepath):
    os.makedirs(samplepath)
   
# get image data
images = np.full((6,1440,120), np.nan)
times = []
for iii in range(6):
    hdulist = fits.open(files[iii])
    hdu = hdulist[0]
    orig, _ , _ = get_image.getRedHST(files[iii], minangle=10)
    images[iii,:,:] = orig
    fmt = '%Y-%m-%d %H:%M:%S'
    time = datetime.strptime(hdu.header['UDATE'], fmt)
    _, lt = spice.spkpos('SATURN', tc.datetime2et(time), 'IAU_EARTH', 'NONE', 'EARTH')
    time = time - timedelta(seconds=lt)
    times.append(time)
    
panels = '123456789'
    
fig = plt.figure()
fig.set_size_inches(15,10)

gs = gridspec.GridSpec(2, 4, width_ratios=(10,10,10,1),
                        wspace=0.15, hspace=0.1)

theta = np.linspace(0,2*np.pi,num=np.shape(images)[1]+1)
r = np.linspace(0,30,num=np.shape(images)[2]+1)

for imgctr in range(6):
    lin = imgctr // 3
    col = imgctr % 3
    ax = plt.subplot(gs[lin, col], projection='polar')            
    image = images[imgctr,:,:]
    image[np.where(image<=0)] = 0.00001
    
    # plot colormap and colorbar
    KR_MIN=1
    KR_MAX=50
    scale = 'log'
    quad = ax.pcolormesh(theta, r, image.T, cmap=cmap_UV)
    quad.set_clim(0, KR_MAX)
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    quad.cmap.set_under('k')
    ax.set_facecolor('gray')
    
    # plot Cassini footprint
    tmp = cd.get_ionfootp(tc.datetime2et(times[imgctr]))
    ax.scatter(tmp[3]/12*np.pi, tmp[1], marker='o', edgecolors=myred,
               linewidths=3,
               facecolors='none', s=40, zorder=30)
    
    if not imgctr:
        subax = plt.subplot(gs[0:2,3])
        cbar = plt.colorbar(quad, pad=0.1, cax=subax, extend='both')
        cbar.set_label('Intensity (kR)', labelpad=15, fontsize=15, rotation=270)
        cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
                                           np.arange(1,10,1)),
                                 np.arange(10,101,10)))
        cbar.ax.tick_params(labelsize=14)
            
    ax.set_title(datetime.strftime(times[imgctr], fmt), fontsize=13, y=1.0)
    ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
    ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
    ticklabels = ['00','06','12','18']
    for iii in range(len(ticklabels)):
        txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                      ha='center',va='center')
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
    ax.set_xticklabels([])
    ax.tick_params(axis='x', which='major', pad=-20)
    ax.tick_params(axis='x', which='major', color='w')
    ax.set_yticks([10,20,30])
    ax.set_yticklabels([])
    ax.grid('on', color='0.8', linewidth=1.5)
    ax.set_theta_zero_location("N")
    ax.set_rmax(30)
    ax.set_rmin(0)
    ax.text(0,1,'({})'.format(panels[imgctr]), ha='left', va='top',
            fontsize=15, fontweight='bold',
            transform=ax.transAxes)

plt.savefig('{}/2014_100_HST_tile_all.png'.format(samplepath),bbox_inches='tight', dpi=300)
plt.show() 
plt.close()

  