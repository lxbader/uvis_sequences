import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cubehelix' % gitpath)
sys.path.insert(0, '%s/cassinipy' % gitpath)

from astropy.io import fits
import cubehelix
from datetime import datetime
import get_image
import glob
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.ndimage as ndi

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0, gamma=1.5)

folders = glob.glob(r'{}\HST\spot_selec\*'.format(datapath))

for fctr in range(len(folders)):
    thisfolder = folders[fctr]
    files = glob.glob('{}\*.fits'.format(thisfolder))
    
    savepath = '{}/HST_spot_samples'.format(plotpath)
    if not os.path.exists(savepath):
        os.makedirs(savepath)
            
    # get image data
    images = np.full((len(files),1440,120), np.nan)
    times = []
    for iii in range(len(files)):
        hdulist = fits.open(files[iii])
        hdu = hdulist[0]
        orig, _ , _ = get_image.getRedHST(files[iii], minangle=10)
        images[iii,:,:] = orig
        times.append(hdu.header['UDATE'])
    
    theta = np.linspace(0,2*np.pi,num=np.shape(images)[1]+1)
    r = np.linspace(0,30,num=np.shape(images)[2]+1)
    theta_cent = theta[:-1]+np.diff(theta)/2
    r_cent = r[:-1]+np.diff(r)/2
            
    panels = 'abcdefghijklmnopqrstuvwxyz'
    
    columns = int(np.ceil(np.sqrt(len(files))))
    rows = int(np.ceil(len(files)/columns))
    
    fig = plt.figure()
    fig.set_size_inches(5*columns,5*(rows+0.5))

    outer = gridspec.GridSpec(2, 1, height_ratios=(rows,0.5), hspace=0.1)
    gs1 = gridspec.GridSpecFromSubplotSpec(rows, columns,
                            wspace=0.15, hspace=0.1, subplot_spec=outer[0,0])
    gs2 = gridspec.GridSpecFromSubplotSpec(1, len(files),
                            wspace=0.05, subplot_spec=outer[1,0])
    smooths = np.array([])
    selecs = np.array([])
    areas = {}
    # startimage, stopimage, mincolat, maxcolat, minLT, maxLT
    areas['a'] = np.array([0,3,11.5,13,16,18])
    areas['b'] = np.array([3,5,11,13,18,21])
    
    acs = {}
    acs['a'] = myred
    acs['b'] = myblue
    
    ltlat = {}
    for lbl in [*areas]:
        ltlat[lbl] = np.full((6,2), np.nan)
    
    for imgctr in range(len(files)):
        lin = imgctr // columns
        col = imgctr % columns
        ax = plt.subplot(gs1[lin, col], projection='polar')            
        image = images[imgctr,:,:]
        image[np.where(image<=0)] = 0.00001
        
        # plot colormap and colorbar
        KR_MIN=1
        KR_MAX=20
        scale = 'log'
        quad = ax.pcolormesh(theta, r, image.T, cmap=cmap_UV)
        quad.set_clim(0, KR_MAX)
        quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
        quad.cmap.set_under('k')
        ax.set_facecolor('gray')
        
#        if not imgctr:
#            subax = plt.subplot(gs1[0:2,3])
#            cbar = plt.colorbar(quad, pad=0.1, cax=subax, extend='both')
#            cbar.set_label('Intensity (kR)', labelpad=15, fontsize=15, rotation=270)
#            cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
#                                               np.arange(1,10,1)),
#                                     np.arange(10,101,10)))
#            cbar.ax.tick_params(labelsize=14)
                
        ax.set_title(times[imgctr], fontsize=13, y=1.0)
        ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
        ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
        ticklabels = ['00','06','12','18']
        for iii in range(len(ticklabels)):
            txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                          ha='center',va='center')
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
        ax.set_xticklabels([])
        ax.tick_params(axis='x', which='major', pad=-20)
        ax.tick_params(axis='x', which='major', color='w')
        ax.set_yticks([10,20,30])
        ax.set_yticklabels([])
        ax.grid('on', color='0.8', linewidth=1.5)
        ax.set_theta_zero_location("N")
        ax.set_rmax(30)
        ax.set_rmin(0)
        ax.text(0,1,'({})'.format(panels[imgctr]), ha='left', va='top',
                fontsize=15, fontweight='bold',
                transform=ax.transAxes)
     
        ax = plt.subplot(gs2[0,imgctr])
        smooth = ndi.gaussian_filter(images[imgctr], sigma=(20,2.5))
        if not np.any(smooths):
            smooths = np.expand_dims(smooth, axis=0)
        else:
            smooths = np.concatenate([smooths, np.expand_dims(smooth, axis=0)], axis=0)
        quad = ax.pcolormesh(theta/np.pi*12, r, smooth.T, cmap=cmap_UV)
        quad.set_clim(1,20)
        ax.set_facecolor('gray')
        ax.set_title(times[imgctr][11:], fontsize=10)
        ax.set_xlim([14,22])
        ax.set_ylim([7,15]) 
        ax.set_xticks([15,18,21])
        ax.set_yticks(np.arange(7,16,2))
        if ((imgctr>0) & (imgctr<len(images)-1)):
            ax.set_yticklabels([])
        if imgctr==len(images)-1:
            ax.yaxis.tick_right()
        ax.set_xlabel('LT (h)')
        if imgctr==0:
            ax.set_ylabel('Colatitude (deg)')
        elif imgctr==len(images)-1:
            ax.set_ylabel('Colatitude (deg)', labelpad=15, rotation=270)
            ax.yaxis.set_label_position('right')
    #    ax.xaxis.set_ticks_position('both')
        ax.yaxis.set_ticks_position('both')
        ax.grid(which='major', color='w', linestyle='--', linewidth=0.5, alpha=0.6)
        ax.text(0.03,0.96,'({})'.format(panels[imgctr+6]), ha='left', va='top',
                fontsize=15, fontweight='bold', color='w',
                transform=ax.transAxes)
    plt.savefig('{}/{}'.format(savepath,fctr), bbox_inches='tight', dpi=100)
        
    
    plt.show() 
    plt.close()
  