import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cubehelix
import get_image
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.ndimage as ndi

plotpath = '{}sample_data'.format(__file__.strip('HST_projection_sample.py'))
if not os.path.exists(plotpath):
    os.makedirs(plotpath)

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0, gamma=1.5)

ttfile = '{}/HST/2014-100 timetag/occgk3riq_tag.fits'.format(datapath)
projfile = '{}/HST/2014/all/sat_14-100-03-44-20_stis_f25srf2_sm_proj_nobg.fits'.format(datapath)

# =============================================================================
# Original file
# =============================================================================
tmp = fits.open(ttfile)

file = tmp[1]
btime = file.data.field('TIME')
bx = file.data.field('AXIS1')-1
by = file.data.field('AXIS2')-1

img = np.zeros((2048,2048))
for iii in range(len(btime)):
    img[bx[iii], by[iii]] += 1

img_sm = ndi.convolve(img, np.full((11,11), 1.0/121))

fig = plt.figure()
fig.set_size_inches(15,15)
gs = gridspec.GridSpec(2,3, width_ratios=(1,1,0.07), hspace=0.1, wspace=0.1)

ax = plt.subplot(gs[0,0])
ax.imshow(img_sm, cmap=cmap_UV)
ax.set_xticks([])
ax.set_yticks([])
ax.text(0.02,0.98,'(a)', ha='left', va='top',
        fontsize=20, fontweight='bold',
        color='w',
        transform=ax.transAxes)

ax = plt.subplot(gs[1,0])
ax.imshow(img_sm[380:680, 580:880], cmap=cmap_UV)
ax.set_xticks([])
ax.set_yticks([])
ax.text(0.02,0.98,'(b)', ha='left', va='top',
        fontsize=20, fontweight='bold',
        color='w',
        transform=ax.transAxes)


# =============================================================================
# Projected file
# =============================================================================
                
image, angles, _ = get_image.getRedHST(projfile, minangle=10)
angles[angles<0] = np.nan

# =============================================================================
# 
# =============================================================================

ax = plt.subplot(gs[0,1], projection='polar')

theta = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1)
r = np.linspace(0,30,num=np.shape(image)[1]+1)

image[np.where(image<=0)] = 0.00001
    
# plot colormap and colorbar
quad = ax.pcolormesh(theta, r, image.T, cmap=cmap_UV)
quad.set_norm(mcolors.LogNorm(0.5,30))
ax.set_facecolor('gray')

subax = plt.subplot(gs[0,2])
cbar = plt.colorbar(quad, cax=subax, extend='both')
cbar.set_label('Intensity (kR)', labelpad=15, fontsize=14, rotation=270)
cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
                                   np.arange(1,10,1)),
                         np.arange(10,101,10)))
cbar.ax.tick_params(labelsize=12)
        
ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=16, fontweight='bold',
                  ha='center',va='center')
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax.set_xticklabels([])
ax.tick_params(axis='x', which='major', pad=-20)
ax.tick_params(axis='x', which='major', color='w')
ax.set_yticks([10,20,30])
ax.set_yticklabels([])
ax.grid('on', color='0.8', linewidth=1.5)
ax.set_theta_zero_location("N")
ax.set_rmax(30)
ax.set_rmin(0)
#ax.set_title(times[imgctr], fontsize=13, y=1.0)

ax.text(0.02,0.98,'(c)', ha='left', va='top',
        fontsize=20, fontweight='bold',
        transform=ax.transAxes)



# =============================================================================
# 
# =============================================================================


ax = plt.subplot(gs[1,1], projection='polar') 

# plot colormap and colorbar
quad = ax.pcolormesh(theta, r, angles.T, cmap=cmap_UV, vmin=0, vmax=60)
ax.set_facecolor('gray')

subax = plt.subplot(gs[1,2])
cbar = plt.colorbar(quad, cax=subax, extend='both')
cbar.set_label('Elevation angle (deg)', labelpad=18, fontsize=14, rotation=270)
cbar.set_ticks(np.arange(0,61,10))
cbar.ax.tick_params(labelsize=12)
        
ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=16, fontweight='bold',
                  ha='center',va='center')
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax.set_xticklabels([])
ax.tick_params(axis='x', which='major', pad=-20)
ax.tick_params(axis='x', which='major', color='w')
ax.set_yticks([10,20,30])
ax.set_yticklabels([])
ax.grid('on', color='0.8', linewidth=1.5)
ax.set_theta_zero_location("N")
ax.set_rmax(30)
ax.set_rmin(0)

ax.text(0.02,0.98,'(d)', ha='left', va='top',
        fontsize=20, fontweight='bold',
        transform=ax.transAxes)

plt.savefig('{}/proj_sample_2014_100T02_29_09.png'.format(plotpath),bbox_inches='tight', dpi=100)
plt.savefig('{}/proj_sample_2014_100T02_29_09.pdf'.format(plotpath),bbox_inches='tight')
plt.show() 
plt.close()
