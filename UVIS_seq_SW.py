# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime, timedelta
import get_image
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.ticker as mticker
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import os
import ppo_mag_phases
import skr_power
import time_conversions as tc
import uvisdb

myblue='royalblue'
myred='crimson'
def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_UV_short = truncate_colormap(cmap_UV, 0.1, 0.9, 100)

def MAD(x):
    a = np.nanmedian(x)
    return np.nanmedian(np.abs(x-a))

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF
imglist['ET'] = np.mean(imglist[['ET_START', 'ET_STOP']], axis=1)
imglist['UV_POW_BG'] = np.nan

skrpw = skr_power.SKRpower(datapath)
cd = cassinipy.CassiniData(datapath)
cd.set_refframe('KSM')
pmp = ppo_mag_phases.PPOMagPhases(datapath)

day_selec = np.array([
        [2008,2,6,12,12,    0], # OCB # too many gaps?
        [2008,4,18,12,8,    0], # OCB
        [2008,5,8,12,8,     0], # OCB
        [2008,7,13,7,8,     0], # OCB
        [2008,7,19,8,12,    0],
        [2008,11,30,4,8,    0], # OCB
        
        #[2013,8,23,14,12,   0], #T=30min
        [2013,8,25,16,9,   0],
        [2013,8,27,16,8,   0],
#        [2013,11,26,14,8,   0], #T=25min
        
        [2014,3,20,2,6,     0],
        [2014,3,28,14,12,   0],
        
        [2014,5,10,10,12,   0],
#        [2014,5,20,22,8,    0], #T=23min
#        [2014,5,21,22,8,    0], #<3hrs
        [2014,5,25,15,11,   0],
        [2014,5,27,12,12,   0],
        [2014,5,29,12,12,   0],
        
        [2014,5,30,20,6,    0],
        [2014,5,31,18,8,    0],
        [2014,6,1,18,8,     0],
        [2014,6,2,21,6,     0],
        [2014,6,3,21,6,     0],
        [2014,6,5,18,12,    0],
        [2014,6,7,20,9,    0],
        [2014,6,9,20,12,    0],
        [2014,6,11,0,12,    0],

        [2014,9,5,16,8,     0],
        [2014,9,13,10,8,    0],
        
        [2014,10,17,0,12,   0],
#        [2014,11,5,9,12,    0], #alternating flashes, double frequency 
        [2014,11,7,6,8,     0],
        
        [2014,11,23,15,6,   0],
        [2014,11,28,3,8,    0],
        [2014,12,1,6,7,    0],
        
        [2016,6,25,6,8,     0],
#        [2016,8,16,18,8,    0], #T=17min, N<20
#        [2016,8,17,21,12,   0], #T=21min
        [2016,9,7,0,12,     0], # OCB
        [2016,9,29,18,8,    0],
        [2016,9,30,12,12,   0],
        [2016,10,1,18,12,   0], # OCB
        [2016,10,29,6,12,   0],
        
        [2017,1,14,20,8,    0],
#        [2017,3,13,8,8,     0], #T=18min, N<20
        [2017,3,20,16,16,   0],
        [2017,4,2,19,8,     0], #incomplete
        [2017,4,18,8,8,     0],
#        [2017,7,25,0,12,    0], # incomplete / alternating scans
#        [2017,9,14,6,16,    0], #T=47min
        ])

savedir = '{}/UV_seq/SW'.format(plotpath)
if not os.path.exists(savedir):
    os.makedirs(savedir)
pdf = PdfPages('{}/all_seq_SW.pdf'.format(savedir))
    
for selecctr in range(np.shape(day_selec)[0]):
#    if not np.all(day_selec[selecctr,0:3] == [2014,5,25]):
#        continue
#    if not (np.all(day_selec[selecctr,0:3] == [2013,8,25]) |
#            np.all(day_selec[selecctr,0:3] == [2014,3,28]) |
#            np.all(day_selec[selecctr,0:3] == [2014,5,25])):
#        continue
    if day_selec[selecctr,-1] == 1:
        plotflux = True
    else:
        plotflux = False
    centertime = datetime(day_selec[selecctr,0], day_selec[selecctr,1],
                          day_selec[selecctr,2], day_selec[selecctr,3])
    dtstart = centertime - timedelta(hours=int(day_selec[selecctr,4]))
    dtstop = centertime + timedelta(hours=int(day_selec[selecctr,4]))
    etstart = tc.datetime2et(dtstart)
    etstop = tc.datetime2et(dtstop)
    fmt = '%Y-%m-%d %H:%M:%S'
    print('New plot: {}'.format(datetime.strftime(dtstart, fmt)))
    
    print('Selecting time window...')
    selec_UVIS = imglist[(imglist['ET'] >= etstart) & 
                    (imglist['ET'] <= etstop)]
#    if np.all(day_selec[selecctr,0:3] == [2017,3,20]):
#        selec_UVIS = selec_UVIS[selec_UVIS['ET_START']<tc.datetime2et(datetime(2017,3,20,20,0,0))]
    if np.all(day_selec[selecctr,0:3] == [2014,3,28]):
        selec_UVIS = selec_UVIS.drop(890)

    # combine images
    print('Combining images...')
    join = np.array([
            ['2008_201T04_38_43', '2008_201T04_45_39'],
            ['2008_201T09_14_35', '2008_201T09_21_39'],
            ['2014_150T20_19_17', '2014_150T20_22_21'],
            ['2014_151T20_45_16', '2014_151T20_48_52'],
            ['2014_154T19_14_56', '2014_154T19_19_04'],
            ['2014_158T16_15_59', '2014_158T16_19_11'],
                     ])
    selec_UVIS['COMBINE_FILEPATH'] = 'NONE'
    for iii in range(np.shape(join)[0]):
        et1 = tc.datetime2et(datetime.strptime(join[iii,0], '%Y_%jT%H_%M_%S'))
        et2 = tc.datetime2et(datetime.strptime(join[iii,1], '%Y_%jT%H_%M_%S'))
        if not (np.all(np.array([et1,et2])>=etstart) and np.all(np.array([et1,et2])<=etstop)):
            continue
        # reorganize dataframe a bit
        ind1 = selec_UVIS[(selec_UVIS['FILEPATH'].str.endswith('{}.fits'.format(join[iii,0])))].index[0]
        ind2 = selec_UVIS[(selec_UVIS['FILEPATH'].str.endswith('{}.fits'.format(join[iii,1])))].index[0]
        selec_UVIS.at[ind1,'ET_STOP'] = selec_UVIS.loc[ind2,'ET_STOP']
        selec_UVIS.at[ind1,'ET'] = selec_UVIS.loc[ind1,['ET_START','ET_STOP']].mean()
        selec_UVIS.at[ind1,'EXP'] += selec_UVIS.loc[ind2,'EXP']
        selec_UVIS.at[ind1,'COMBINE_FILEPATH'] = selec_UVIS.loc[ind2,'FILEPATH']
        selec_UVIS.drop(ind2, inplace=True)
        # load and combine images to get true UV power
        image1,angles1,_ = get_image.getRedUVIS('{}/{}'.format(uvispath,selec_UVIS.at[ind1,'FILEPATH']),
                                          minangle=0)
        image2,angles2,_ = get_image.getRedUVIS('{}/{}'.format(uvispath,selec_UVIS.at[ind1,'COMBINE_FILEPATH']),
                                          minangle=0)
        image = np.nanmean([image1,image2], axis=0)
        angles = np.nanmean([angles1,angles2], axis=0)
        selec_UVIS.at[ind1,'UV_POWER'] = udb.integrateUV(data=image, angles=angles, sections=False)
        selec_UVIS.at[ind1,'UV_POWER_SECTIONS'] = udb.integrateUV(data=image, angles=angles, sections=True)
            
    hem = selec_UVIS['HEMISPHERE'].iloc[0]
    pol = 'RH' if hem == 'North' else 'LH'
        
    def set_grid(ax, color='k'):
        ax.grid(which='major', color=color, linestyle='-', linewidth=0.5, alpha=0.5) 
        ax.grid(which='minor', color=color, linestyle='--', linewidth=0.5, alpha=0.6)
        
    def expticks(x, pos):
        if np.isnan(x):
            return 'NaN'
        elif x==0:
            return '0'
        elif np.isinf(x):
            return 'inf'
        tmp = int(np.floor(np.log10(np.abs(x))))
        return r'$%1.2f\times 10^{%d}$' % (x/10**tmp, tmp)

    formatter = mticker.FuncFormatter(expticks)
    
    #==============
    #==============
    # set up figure
    fig = plt.figure()
    fig.set_size_inches(16,24)
    NNN = 6
    NUM_subplots = NNN
    sp_iter = iter(range(NUM_subplots))
    gs = gridspec.GridSpec(NUM_subplots, 2,
                           width_ratios=(50,1), bottom=0.40)
    gs.update(wspace=0.01)
    gs2 = gridspec.GridSpec(1, 3,
                           width_ratios=(25,25,0.4), top=0.35)
    gs2.update(wspace=0.05)
    
    # for naming panels
    panels = 'abcdefghijk'
    pan_iter = iter(range(len(panels)))
    pan_fs = 20
    pan_left = 0.02
    pan_top = 0.85
    
    #=================
    # UV power keogram
    thissp = next(sp_iter)
    ax_keo = plt.subplot(gs[thissp, 0])
    ax_keo.set_facecolor('0.3')
    ybins = np.linspace(0,24,num=37)
    data = np.zeros((0,36))
    xvalues = np.array([], dtype=np.float)
    for ind in selec_UVIS.index:
        xvalues = np.append(xvalues, selec_UVIS.loc[ind,'ET_START'])
        if np.all(np.isnan(selec_UVIS.loc[ind,'UV_POWER_SECTIONS'])):
            selec_UVIS.at[ind,'UV_POWER_SECTIONS'] = np.full((36), np.nan)
        data = np.append(data, [selec_UVIS.loc[ind,'UV_POWER_SECTIONS']], axis=0)
        xvalues = np.append(xvalues, selec_UVIS.loc[ind,'ET_STOP'])
        if ind != selec_UVIS.index[-1]:
            data = np.append(data, [np.full((36), np.nan)], axis=0)
    quad = ax_keo.pcolormesh(tc.et2datetime(xvalues),ybins,data.T,
                             norm=mcolors.LogNorm(vmin=np.nanmin(data),
                                                  vmax=np.nanmax(data)),
                             cmap=cmap_UV)
                             
    magminet = np.min(xvalues)-12*3600
    magmaxet = np.max(xvalues)+12*3600
    pporange = np.linspace(magminet, magmaxet, num=10000)
    ppodt = tc.et2datetime(pporange)
    ppo_n = np.degrees((pmp.getMagPhase(pporange, 'N')+np.pi) % (2*np.pi))
    ppo_s = np.degrees((pmp.getMagPhase(pporange, 'S')+np.pi) % (2*np.pi))
    
    max_n = (ppo_n-90-180) % 360 if hem=='North' else (ppo_n+90-180) % 360
    tmp = np.where(np.diff(max_n)<-180)[0]+1
    ins_times = ppodt[tmp-1]+(ppodt[1]-ppodt[0])/2
    xdata = np.insert(ppodt,tmp,ins_times)
    ydata = np.insert(max_n,tmp,[np.nan])
    tmp = np.where(np.isnan(ydata))[0]
    xdata = np.insert(xdata, tmp, xdata[tmp]-timedelta(seconds=1))
    ydata = np.insert(ydata, tmp, [360])
    tmp = np.where(np.isnan(ydata))[0]
    xdata = np.insert(xdata, tmp+1, xdata[tmp]+timedelta(seconds=1))
    ydata = np.insert(ydata, tmp+1, [0])
    ydata = ydata/360*24
    ax_keo.plot(xdata, ydata, 'w-' if pol=='RH' else 'w--',
                lw=2 if pol=='RH' else 0.8,
                label='PPO N max upward FAC')
    
    if not np.all(np.isnan(ppo_s)):
        max_s = (ppo_s-90-180) % 360 if hem=='North' else (ppo_s+90-180) % 360
        tmp = np.where(np.diff(max_s)<-180)[0]+1
        ins_times = ppodt[tmp-1]+(ppodt[1]-ppodt[0])/2
        xdata = np.insert(ppodt,tmp,ins_times)
        ydata = np.insert(max_s,tmp,[np.nan])
        tmp = np.where(np.isnan(ydata))[0]
        xdata = np.insert(xdata, tmp, xdata[tmp]-timedelta(seconds=1))
        ydata = np.insert(ydata, tmp, [360])
        tmp = np.where(np.isnan(ydata))[0]
        xdata = np.insert(xdata, tmp+1, xdata[tmp]+timedelta(seconds=1))
        ydata = np.insert(ydata, tmp+1, [0])
        ydata = ydata/360*24
        ax_keo.plot(xdata, ydata, 'w-' if pol=='LH' else 'w--',
                    lw=2 if pol=='LH' else 0.8,
                    label='PPO S max upward FAC')
            
    leg = ax_keo.legend(loc=4)
    leg.get_frame().set_facecolor('0.6')
    ax_keo.set_ylabel('LT (hrs)')
    ax_keo.set_ylim([0,24])
    ax_keo.set_yticks([6,12,18,24])
    set_grid(ax_keo)
    ax_keo.text(pan_left, pan_top, '({})'.format(panels[next(pan_iter)]),
                transform=ax_keo.transAxes, ha='center', va='center',
                color='w', fontweight='bold', fontsize=pan_fs)
            
    subax = plt.subplot(gs[thissp, 1])
    cbar = plt.colorbar(quad, cax=subax)
    cbar.set_label('UV power (GW)', rotation=270, labelpad=10)
    
    short_x = (xvalues[0::2]+xvalues[1::2])/2
    short_y = ybins[:-1]+np.diff(ybins)/2
    short_data = data[::2,:]
    
    fmt = '%Y-%m-%d (DOY %j)'
    ax_keo.set_title('{0} ({1}, $T_\mathrm{{median}} = {2:.1f}\,\mathrm{{min}}$)'.format(
                        datetime.strftime(centertime, fmt),
                        hem,
                        np.median(np.diff(short_x[np.isfinite(np.sum(short_data,
                                                    axis=1))])/60)))
    
    #===============
    # total UV power
    # background
    thissp = next(sp_iter)
    ax_uvtot = plt.subplot(gs[thissp, 0], sharex=ax_keo)
    xdata = tc.et2datetime(selec_UVIS['ET'])
    diffsecs = np.array([iii.total_seconds() for iii in np.diff(xdata)])
    tmp = np.where(diffsecs>4000)[0]+1
    ins = np.array([xdata[iii] - timedelta(seconds=1800) for iii in tmp])
    ydata = np.array(selec_UVIS['UV_POWER'])*1e-9
    xdata = np.insert(xdata, tmp, ins)
    ydata = np.insert(ydata, tmp, [np.nan])
    ax_uvtot.plot(xdata, ydata, 'k-x', lw=1)
   
    ax_uvtot.set_ylabel('UV power (GW)',
                        labelpad=15 if not thissp%2 else 5,
                        rotation=270 if not thissp%2 else 90)
    set_grid(ax_uvtot)
    if not thissp%2:
        ax_uvtot.yaxis.set_label_position('right')
        ax_uvtot.yaxis.tick_right()
    ax_uvtot.text(pan_left, pan_top, '({})'.format(panels[next(pan_iter)]),
                  transform=ax_uvtot.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=pan_fs)
    
    #==========
    # MAG field
    cd.set_timeframe([magminet, magmaxet])
    magdata = cd.get_MAG(res='1S')
    thissp = next(sp_iter)
    ax_mag = plt.subplot(gs[thissp, 0], sharex = ax_keo)
    
#    fig, ax_mag = plt.subplots()
    ax_mag.set_color_cycle(['k', myred, myblue])
    coords = 'XYZ'
    for ctr in range(3):
        ax_mag.plot(tc.et2datetime(magdata.ettimes), magdata.data[:,ctr], lw=1,
                    label='$B_\mathrm{{{}}}$'.format(coords[ctr]))
    ax_mag.legend(loc=1)
    ax_mag.set_ylabel('B (nT)',
                      labelpad=30 if not thissp%2 else 5,
                      rotation=270 if not thissp%2 else 90)
    set_grid(ax_mag)
    if not thissp%2:
        ax_mag.yaxis.set_label_position('right')
        ax_mag.yaxis.tick_right()
    ax_mag.text(pan_left, pan_top, '({})'.format(panels[next(pan_iter)]),
                transform=ax_mag.transAxes, ha='center', va='center',
                color='k', fontweight='bold', fontsize=pan_fs)
        
    #================
    # IMF clock angle
    thissp = next(sp_iter)
    ax_imf = plt.subplot(gs[thissp, 0], sharex = ax_keo)
    
#    fig, ax_imf = plt.subplots()
    ax_imf.plot(tc.et2datetime(magdata.ettimes),
                np.degrees(np.arctan2(magdata.data[:,1],magdata.data[:,2]))%360,
                lw=1, c='k')
    ax_imf.set_ylabel('IMF clock angle (deg)',
                      labelpad=15 if not thissp%2 else 5,
                      rotation=270 if not thissp%2 else 90)
    ax_imf.set_ylim([0,360])
    ax_imf.set_yticks(np.arange(0,361,90))
    set_grid(ax_imf)
    if not thissp%2:
        ax_imf.yaxis.set_label_position('right')
        ax_imf.yaxis.tick_right()
    ax_imf.text(pan_left, pan_top, '({})'.format(panels[next(pan_iter)]),
                transform=ax_imf.transAxes, ha='center', va='center',
                color='k', fontweight='bold', fontsize=pan_fs)
        
    #=================
    # Electron density
    thissp = next(sp_iter)
    ax_el = plt.subplot(gs[thissp, 0], sharex = ax_keo)
    
#    fig, ax_el = plt.subplots()
    edensdata = cd.crpws
    edensdata.setETlim([magminet,magmaxet])
    edensdata.readDensities()
    ax_el.plot(tc.et2datetime(edensdata.dens_e_et),
                edensdata.dens_e_val,
                lw=1, c=myred)
    ax_el.scatter(tc.et2datetime(edensdata.dens_e_et),
                   edensdata.dens_e_val,
                   marker='o', c=myred, s=10)
    ax_el.set_yscale('log')
    ax_el.set_ylabel('e$^-$ density (cm$^{-3}$)',
                      labelpad=20 if not thissp%2 else 5,
                      rotation=270 if not thissp%2 else 90)
    set_grid(ax_el)
    if not thissp%2:
        ax_el.yaxis.set_label_position('right')
        ax_el.yaxis.tick_right()
    ax_el.text(pan_left, pan_top, '({})'.format(panels[next(pan_iter)]),
                transform=ax_el.transAxes, ha='center', va='center',
                color='k', fontweight='bold', fontsize=pan_fs)
        
    #==========
    # SKR power
    skr_et = np.linspace(magminet, magmaxet, num=10000)
    skr_powers_3m = skrpw.get3mSKR(skr_et, pol)
    skr_powers_1h = skrpw.get1hSKR(skr_et, pol)
    
    thissp = next(sp_iter)
    ax_skr = plt.subplot(gs[thissp, 0], sharex = ax_keo)
    ax_skr.plot(tc.et2datetime(skr_et), skr_powers_3m, 'k-', lw=0.5, label='3 min average')
    ax_skr.plot(tc.et2datetime(skr_et), skr_powers_1h, 'r-', lw=1, label='1 hr average')
    ax_skr.set_ylabel('{} SKR power,\n 1hr avg (W/m2)'.format(pol),
                      labelpad=30 if not thissp%2 else 5,
                      rotation=270 if not thissp%2 else 90)
    ax_skr.legend(loc=1)
    ax_skr.set_ylim([1e-18, 1e-13])
    ax_skr.set_yscale('log')
    set_grid(ax_skr)
    if not thissp%2:
        ax_skr.yaxis.set_label_position('right')
        ax_skr.yaxis.tick_right()
    ax_skr.text(pan_left, pan_top, '({})'.format(panels[next(pan_iter)]),
                transform=ax_skr.transAxes, ha='center', va='center',
                color='k', fontweight='bold', fontsize=pan_fs)
 
    #========
    # GENERAL
    ax = plt.gca()
    if etstop-etstart>3600*24*2:
        ax.xaxis.set_major_locator(mdates.DayLocator(interval=1))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
        ax.xaxis.set_minor_locator(mdates.HourLocator(interval=3))
    else:
        ax.xaxis.set_major_locator(mdates.HourLocator(interval=3))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d\n%H:%M'))
        ax.xaxis.set_minor_locator(mdates.HourLocator(interval=1))
#    ax.set_xlim(dtstart, dtstop)
    ax.set_xlim([np.min(tc.et2datetime(xvalues)), np.max(tc.et2datetime(xvalues))])
    ax.set_xlim([tc.et2datetime(magminet), tc.et2datetime(magmaxet)])
    plt.setp(ax.get_xticklabels(), visible=True)
    fig.autofmt_xdate()
    gs.update(hspace=0)    
    
    #=======
    # Saving
    fmt = '%Y_%j_%H'
    plt.savefig('{}/{}.png'.format(savedir,datetime.strftime(centertime, fmt)),
                bbox_inches='tight', dpi=300)
    plt.savefig('{}/{}.pdf'.format(savedir,datetime.strftime(centertime, fmt)),
                bbox_inches='tight', dpi=300)
    pdf.savefig()
    plt.show()
    plt.close()
        
pdf.close()
